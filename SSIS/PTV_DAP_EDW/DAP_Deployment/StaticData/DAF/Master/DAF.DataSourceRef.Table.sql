DELETE FROM [DAF].[DataSourceRef]
GO
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'DIVA', N'Diva GTFS', N'DIVAGTFS')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'DW', N'DW', N'DW')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'GTFS', N'GTFS', N'GTFS')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'MYKI', N'MYKI', N'MYKI')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'mykiNTSMirrorDataWarehouse', N'Myki NTS Data Warehouse Mirror', N'myki')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'SMARTRAK', N'SMARTRAK', N'SMARTRAK')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'TORS', N'TORS', N'TORS')
INSERT [DAF].[DataSourceRef] ([DataSourceRefCode], [Description], [ShortName]) VALUES (N'TransPROD', N'TRANSPROD', N'TRANSPROD')
