DELETE FROM [DAF].[EventClassRef]
GO
INSERT [DAF].[EventClassRef] ([EventClassRefCode], [Description], [ShortName]) VALUES (N'DEBUG', N'Debug Description', N'debug')
INSERT [DAF].[EventClassRef] ([EventClassRefCode], [Description], [ShortName]) VALUES (N'EXCEPTION', N'Exception Description', N'exception')
INSERT [DAF].[EventClassRef] ([EventClassRefCode], [Description], [ShortName]) VALUES (N'MESSAGE', N'Message Description', N'message')
