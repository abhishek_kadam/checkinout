DELETE FROM [DAF].[QualityIssueRef]
GO
INSERT [DAF].[QualityIssueRef] ([QualityIssueRefCode], [Description], [ShortName]) VALUES (N'ISSUE', N'Issue Description', N'issue')
INSERT [DAF].[QualityIssueRef] ([QualityIssueRefCode], [Description], [ShortName]) VALUES (N'NONISSUE', N'Non Issue Description', N'nonissue')
