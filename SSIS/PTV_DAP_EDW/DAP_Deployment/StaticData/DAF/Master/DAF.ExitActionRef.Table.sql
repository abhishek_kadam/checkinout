DELETE FROM [DAF].[ExitActionRef]
GO
INSERT [DAF].[ExitActionRef] ([ExitActionRefCode], [Description], [ShortName]) VALUES (N'ABORT', N'Abort action', N'aborting')
INSERT [DAF].[ExitActionRef] ([ExitActionRefCode], [Description], [ShortName]) VALUES (N'CONTINUE', N'Continue action', N'continuing')
