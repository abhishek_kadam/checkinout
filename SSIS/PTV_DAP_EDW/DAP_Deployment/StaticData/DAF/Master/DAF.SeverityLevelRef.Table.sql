DELETE FROM [DAF].[SeverityLevelRef]
GO
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'ERROR', N'Error Description', N'ERROR', 6, N'Y')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'FATAL', N'Fatal Description', N'FATAL', 7, N'N')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'HOLD', N'Item has failed a rule and requires manual user intervention for item to be loaded into warehouse', N'HOLD', 5, N'Y')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'HOLDPASS', N'HOLD record that has been approved', N'HOLDPASS', 4, N'N')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'INFO', N'Information Description', N'INFO', 2, N'Y')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'PASS', N'The data item passed validation', N'PASS', 1, N'N')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'UNVALIDATED', N'The data item has not been validated', N'UNVALIDATED', 0, N'N')
INSERT [DAF].[SeverityLevelRef] ([SeverityLevelRefCode], [Description], [ShortName], [SeverityLevel], [DisplayInDQIMFlag]) VALUES (N'WARN', N'Warning Description', N'WARN', 3, N'Y')
