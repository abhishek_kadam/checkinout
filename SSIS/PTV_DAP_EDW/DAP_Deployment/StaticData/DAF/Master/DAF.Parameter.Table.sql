DELETE FROM [DAF].[Parameter]
GO
SET IDENTITY_INSERT [DAF].[Parameter] ON 

INSERT [DAF].[Parameter] ([ParameterID], [RunGroupID], [Name], [ParameterTypeRefCode], [Description], [StringValue], [IntegerValue], [DateTimeValue]) VALUES (4, 66, N'STARTDATE', N'DATETIME', N'Date Range :START DATE', NULL, NULL, CAST(N'1997-01-01 00:00:00.000' AS DateTime))
INSERT [DAF].[Parameter] ([ParameterID], [RunGroupID], [Name], [ParameterTypeRefCode], [Description], [StringValue], [IntegerValue], [DateTimeValue]) VALUES (5, 66, N'ENDDATE', N'DATETIME', N'Date Range :END DATE', NULL, NULL, CAST(N'2036-12-31 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [DAF].[Parameter] OFF
