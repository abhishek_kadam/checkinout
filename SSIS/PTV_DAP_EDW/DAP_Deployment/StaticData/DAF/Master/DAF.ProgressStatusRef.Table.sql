DELETE FROM [DAF].[ProgressStatusRef]
GO
INSERT [DAF].[ProgressStatusRef] ([ProgressStatusRefCode], [Description], [ShortName]) VALUES (N'ABORTED', N'Aborted Description', N'aborted')
INSERT [DAF].[ProgressStatusRef] ([ProgressStatusRefCode], [Description], [ShortName]) VALUES (N'CANCELLED', N'Cancelled Description', N'cancelled')
INSERT [DAF].[ProgressStatusRef] ([ProgressStatusRefCode], [Description], [ShortName]) VALUES (N'CANCELLING', N'Cancelling Description', N'cancelling')
INSERT [DAF].[ProgressStatusRef] ([ProgressStatusRefCode], [Description], [ShortName]) VALUES (N'COMPLETED', N'Completed Description', N'completed')
INSERT [DAF].[ProgressStatusRef] ([ProgressStatusRefCode], [Description], [ShortName]) VALUES (N'INPROGRESS', N'In Progress Description', N'in progress')
