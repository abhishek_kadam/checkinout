DELETE FROM [DAF].[ImplementationRef]
GO
INSERT [DAF].[ImplementationRef] ([ImplementationRefCode], [Description], [ShortName]) VALUES (N'SSISPACKAGE', N'Implemented by a SSIS package', N'SSIS')
INSERT [DAF].[ImplementationRef] ([ImplementationRefCode], [Description], [ShortName]) VALUES (N'STOREDPROCEDURE', N'Implemented by a stored procedure', N'StoredProc')
INSERT [DAF].[ImplementationRef] ([ImplementationRefCode], [Description], [ShortName]) VALUES (N'TRANSFORM', N'Implemented by the DAF Transform process', N'Transform')
