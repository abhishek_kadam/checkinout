DELETE FROM [DAF].[ParameterTypeRef]
GO
INSERT [DAF].[ParameterTypeRef] ([ParameterTypeRefCode], [Description], [ShortName]) VALUES (N'DATETIME', N'DateTime type', N'datetime')
INSERT [DAF].[ParameterTypeRef] ([ParameterTypeRefCode], [Description], [ShortName]) VALUES (N'INTEGER', N'Integer type', N'integer')
INSERT [DAF].[ParameterTypeRef] ([ParameterTypeRefCode], [Description], [ShortName]) VALUES (N'STRING', N'String type', N'string')
