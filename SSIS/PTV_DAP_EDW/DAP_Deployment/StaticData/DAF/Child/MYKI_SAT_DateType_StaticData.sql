﻿
/********************************
Author Name   : Abhishek Kadam
Created Date  : 02/25/2016
Modified Date : 02/25/2016
Scripts       : DAF Configuration
*********************************/

/*NOTE : First Delete the Records in reverse order because of primary key contrain*/
IF EXISTS (SELECT * FROM DAF.PackageRunGroup WHERE RunGroupID =76)
	DELETE FROM DAF.PackageRunGroup WHERE RunGroupID =76	

IF EXISTS (SELECT * FROM DAF.RunGroup WHERE RunGroupID =76)
	DELETE FROM DAF.RunGroup WHERE RunGroupID =76	

IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Extract.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Extract.dtsx'	
	
IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Transform.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Transform.dtsx'	
	
IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Load.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_DateType.MYKI.Load.dtsx'			
	
IF EXISTS (SELECT * FROM DAF.DataEntityRef WHERE DataEntityRefCode = 'SAT_DATETYPE')
	DELETE FROM DAF.DataEntityRef WHERE DataEntityRefCode = 'SAT_DATETYPE'


INSERT INTO DAF.DataEntityRef (DataEntityRefCode,
		Description,
		ShortName,
		KeyAttribute1,
		KeyAttribute2,
		KeyAttribute3,
		KeyAttribute4,
		KeyAttribute5,
		KeyAttribute6,
		KeyAttribute7,
		KeyAttribute8,
		KeyAttribute9,
		DataEntityType)
		VALUES ('SAT_DATETYPE',
				'Sat : Date Type',
				'Sat_DateType',
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				'Sat')
				


insert into  daf.package
				(PackageID,
				PackageName,
				Description,
				CreatedBy,
				CreatedDate,
				LastModifiedBy,
				LastModifiedDate,
				CleanupStoredProcedureName
				)
				values (184,'Sat_DateType.MYKI.Extract.dtsx','Data Extract',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null),
					   (185,'Sat_DateType.MYKI.Transform.dtsx','Transform',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null),
					   (186,'Sat_DateType.MYKI.Load.dtsx','Raw Data Vault Load',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null)
			   
				

insert into daf.RunGroup
	  (	RunGroupID,
		Description,
		EnabledFlag,
		DataSourceRefCode,
		CleanupStoredProcedureName,
		LastExtractDateTime,
		LastExtractLoadedDateTime,
		CreatedBy,
		CreatedDate,
		LastModifiedBy,
		LastModifiedDate
		)
		values (76,'Sat: Date Type Sat  Data from Myki','Y','mykiNTSMirrorDataWarehouse','usp_cleanup_Generic',NULL,NULL,SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE())

		
		
		

		insert into  daf.PackageRunGroup
		(PackageRunGroupID,
			PackageID,
			RunGroupID,
			DataEntityRefCode,
			ExecutionOrderNum,
			EnabledFlag,
			ExitActionRefCode,
			CreatedDate,
			CreatedBy,
			LastModifiedDate,
			LastModifiedBy

		)
		values  (184,184,76,'SAT_DATETYPE',1,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER),
				(185,185,76,'SAT_DATETYPE',2,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER),
				(186,186,76,'SAT_DATETYPE',3,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER)

		  Go 
		  
		  
