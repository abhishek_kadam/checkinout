﻿/********************************
Author Name   : Abhishek Kadam
Created Date  : 02/16/2016
Modified Date : 02/23/2016
Scripts       : DAF Configuration
*********************************/

/*NOTE : First Delete the Records in reverse order because of primary key contrain*/
IF EXISTS (SELECT * FROM DAF.PackageRunGroup WHERE RunGroupID =64)
	DELETE FROM DAF.PackageRunGroup WHERE RunGroupID =64	

IF EXISTS (SELECT * FROM DAF.RunGroup WHERE RunGroupID =64)
	DELETE FROM DAF.RunGroup WHERE RunGroupID =64	

IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Extract.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Extract.dtsx'	
	
IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Transform.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Transform.dtsx'	
	
IF EXISTS (SELECT * FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Load.dtsx')
	DELETE FROM DAF.package WHERE PackageName = 'Sat_FareProductStatus.MYKI.Load.dtsx'			
	
IF EXISTS (SELECT * FROM DAF.DataEntityRef WHERE DataEntityRefCode = 'SAT_FAREPRODUCTSTATUS')
	DELETE FROM DAF.DataEntityRef WHERE DataEntityRefCode = 'SAT_FAREPRODUCTSTATUS'


INSERT INTO DAF.DataEntityRef (DataEntityRefCode,
		Description,
		ShortName,
		KeyAttribute1,
		KeyAttribute2,
		KeyAttribute3,
		KeyAttribute4,
		KeyAttribute5,
		KeyAttribute6,
		KeyAttribute7,
		KeyAttribute8,
		KeyAttribute9,
		DataEntityType)
		VALUES ('SAT_FAREPRODUCTSTATUS',
				'SAT : FARE PRODUCT STATUS',
				'Sat_FareProductStatus',
				'Fare Product Status Id',
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				'Sat')
				


Insert into  daf.package
				(PackageID,
				PackageName,
				Description,
				CreatedBy,
				CreatedDate,
				LastModifiedBy,
				LastModifiedDate,
				CleanupStoredProcedureName
				)
				values (152,'Sat_FareProductStatus.MYKI.Extract.dtsx','Data Extract',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null),
					   (153,'Sat_FareProductStatus.MYKI.Transform.dtsx','Transform',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null),
					   (154,'Sat_FareProductStatus.MYKI.Load.dtsx','Raw Data Vault Load',SYSTEM_USER,getdate(),SYSTEM_USER,getdate(),null)
			   
				
		
Insert into daf.RunGroup
	  (	RunGroupID,
		Description,
		EnabledFlag,
		DataSourceRefCode,
		CleanupStoredProcedureName,
		LastExtractDateTime,
		LastExtractLoadedDateTime,
		CreatedBy,
		CreatedDate,
		LastModifiedBy,
		LastModifiedDate
		)
		values (64,'SAT : Fare Product Status Data from Myki','Y','mykiNTSMirrorDataWarehouse','usp_cleanup_Generic',NULL,NULL,SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE())

		

Insert into  daf.PackageRunGroup
		(PackageRunGroupID,
			PackageID,
			RunGroupID,
			DataEntityRefCode,
			ExecutionOrderNum,
			EnabledFlag,
			ExitActionRefCode,
			CreatedDate,
			CreatedBy,
			LastModifiedDate,
			LastModifiedBy

		)
		values  (153,152,64,'SAT_FAREPRODUCTSTATUS',1,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER),
				(154,153,64,'SAT_FAREPRODUCTSTATUS',2,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER),
				(155,154,64,'SAT_FAREPRODUCTSTATUS',3,'Y','ABORT',GETDATE(),SYSTEM_USER,GETDATE(),SYSTEM_USER)

		  
		  
