﻿
/* *****************************************************************************
 * Name : STG.[usp_Load_Sat_TransactionType]
 *
 * Created :Feb 8th 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Loads data from staging to raw data vault for Sat_TransactionType
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			08/02/2016	--		Initial version ( Need to add logica to handle processstatus LOADFAILED )
 * NAVEEN SUBRAMANIAM	2.0			15/02/2016	--		Included the transaction
 *
 *
  */

IF OBJECT_ID('[STG].[usp_Load_Sat_TransactionType]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_TransactionType]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_TransactionType] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_TRANSACTIONTYPE where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_TRANSACTIONTYPE 
		SET DV.SAT_TRANSACTIONTYPE.LoadEndDate =GETDATE(),
			DV.SAT_TRANSACTIONTYPE.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_TRANSACTIONTYPE STG 
			 WHERE  DV.SAT_TRANSACTIONTYPE.HubTransactiontypeSid=STG.HubTransactionTypeSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_TRANSACTIONTYPE
		(HubTransactionTypeSid,
		TransactionTypeDesc,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubTransactionTypeSid,
		TransactionTypeDesc,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_TRANSACTIONTYPE
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
