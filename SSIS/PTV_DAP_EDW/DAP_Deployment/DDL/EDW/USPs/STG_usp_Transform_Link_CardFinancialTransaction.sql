/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/02/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Link_CardFinancialTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Transform_Link_CardFinancialTransaction]

CREATE PROC [STG].[usp_Transform_Link_CardFinancialTransaction] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 
/* *****************************************************************************
 * Name : STG.usp_Transform_Link_CardFinancialTransaction
 *
 * Created :Mar 2nd 2016
 * Author : Naveen Subramaniam
 *
 * Abstract: Data Transform for Link_CardFinancialTransaction
 *			 
 *
 * Input : STG.[MYKI_Link_CardFinancialTransaction]
 *		   
 * 
 * Output :STG.[RDV_Link_CardFinancialTransaction]
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			02/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1) 
	from STG.[MYKI_Link_TouchOnTransaction] 
	where Rungroupid =@Rungroupid) 





		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Link_TouchOnTransaction]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO STG.RDV_Link_CardFinancialTransaction
		(
			[LinkCardFinancialTransactionSid],
			[TransactionDate],
			[TransactionTime],
			[HubSmartCardSid],
			[HubFareProductSid],
			[HubTerminalSid],
			[LoadDate],
			[RecordSource],
			[ProcessStatus],
			[UpdateStatus],
			[RunGroupID]
		)

		SELECT
		
		SourceStaging.LinkCardFinancialTransactionSid,
		HD.Date,
		CAST(REFTIME.TIMEFULL AS TIME(0))[TransactionTime],
		HSC.HubSmartCardSid, 
		HFP.HubFareProductSid, 
		HTer.HubTerminalSid,
		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.HubSmartCardSid is null then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		FROM 
		(
	
		select  *			
		FROM
		(
		select *,ROW_NUMBER() over (Partition by LinkCardFinancialTransactionSid Order by ExtractDateTime desc) RN
		from STG.[MYKI_Link_CardFinancialTransaction] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1
		) SourceStaging 

		INNER join DV.Hub_Date HD 
				on HD.Date = SourceStaging.TXN_DATE_KEY 

		INNER JOIN STG.REF_DIM_TIME  REFTIME
						on REFTIME.TIME_KEY = SourceStaging.TXN_TIME_KEY 


		INNER join STG.REF_DIM_TERMINAL EXTTER
			on EXTTER.TERMINAL_KEY = SourceStaging.TERMINAL_KEY
			INNER join DV.Hub_Terminal HTer
				on HTer.TerminalID = EXTTER.TERMINAL_ID

		-- Smart Card 
		INNER JOIN   
		(select 0 DSCCARDKEY ,DSC_CARD_KEY LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_DSC_CARD
		UNION 
		select 1 DSCCARDKEY , LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_LLSC_CARD ) REF_SMARTCARD  
						on REF_SMARTCARD.LLSC_CARD_KEY = SourceStaging.LLSC_CARD_KEY AND 
							REF_SMARTCARD.DSCCARDKEY = SourceStaging.DSC_CARD_KEY
		
		INNER join DV.Hub_Smartcard HSC
								on HSC.CardSurfaceId=REF_SMARTCARD.CARD_SURFACE_ID
		
		--FAREPRODUCT 
		Left Outer join DV.HUB_FAREPRODUCT HFP 
						on HFP.ProductSerialNumber=SourceStaging.PRODUCT_SERIAL_NO
							AND HFP.CardSurfaceId=HSC.CardSurfaceId


		

		LEFT OUTER JOIN DV.[Link_CardFinancialTransaction] DVTarget /*take target reference to identify if incoming rows are new or existing*/
				on DVTarget.[LinkCardFinancialTransactionSid] = SourceStaging.LinkCardFinancialTransactionSid


 IF OBJECT_ID('[#tmpFailed_LinkCardFinancialTransaction]', 'U') IS NOT NULL DROP TABLE [#tmpFailed_LinkCardFinancialTransaction]
		---create tmp table reference for failed records 
		create table #tmpFailed_LinkCardFinancialTransaction
		WITH 
		  ( 
			CLUSTERED COLUMNSTORE INDEX,
			DISTRIBUTION = ROUND_ROBIN 
		  )
		AS
		select SourceStaging.LinkCardFinancialTransactionSid,
		HD.Date TransactionDate  ,
		REFTIME.TIMEFULL TransactionTime ,
		HSC.[HubSmartCardSid] ,
		HSC.CardSurfaceId,
		--HFP.[HubFareProductSid] ,
		--HFP.ProductSerialNumber,
		HTer.[HubTerminalSid],
		HTer.TerminalId,
		SourceStaging.PRODUCT_SERIAL_NO,
		SourceStaging.TERMINAL_KEY
		From
		(
	
			select * FROM STG.[MYKI_Link_CardFinancialTransaction] WHERE Rungroupid = @RunGroupId 
		) SourceStaging 

			LEFT OUTER JOIN DV.Hub_Date HD 
					on HD.Date = SourceStaging.TXN_DATE_KEY 

			LEFT OUTER JOIN STG.REF_DIM_TIME  REFTIME
							on REFTIME.TIME_KEY = SourceStaging.TXN_TIME_KEY 


			LEFT OUTER JOIN STG.REF_DIM_TERMINAL EXTTER
				on EXTTER.TERMINAL_KEY = SourceStaging.TERMINAL_KEY
				LEFT OUTER JOIN DV.Hub_Terminal HTer
					on HTer.TerminalID = EXTTER.TERMINAL_ID

		
		-- Smart Card 
		LEFT OUTER JOIN   
		(select 0 DSCCARDKEY ,DSC_CARD_KEY LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_DSC_CARD
		UNION 
		select 1 DSCCARDKEY , LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_LLSC_CARD ) REF_SMARTCARD  
						on REF_SMARTCARD.LLSC_CARD_KEY = SourceStaging.LLSC_CARD_KEY AND 
							REF_SMARTCARD.DSCCARDKEY = SourceStaging.DSC_CARD_KEY
		
		LEFT OUTER JOIN DV.Hub_Smartcard HSC
								on HSC.CardSurfaceId=REF_SMARTCARD.CARD_SURFACE_ID
		/*
		--FAREPRODUCT 
		LEFT OUTER JOIN DV.HUB_FAREPRODUCT HFP 
						on HFP.ProductSerialNumber=SourceStaging.PRODUCT_SERIAL_NO
							AND HFP.CardSurfaceId=HSC.CardSurfaceId
		*/
		
			WHERE HSC.CardSurfaceID IS NULL   OR HTer.TerminalID IS NULL 
			OR HD.Date IS NULL OR REFTIME.TIMEFULL IS NULL
			--OR HFP.CardSurfaceID IS NULL

	/* insert data into LOG.DATAQUALITYLOG for Logging*/	
		INSERT INTO LOG.DATAQUALITYLOG (
				[DataQualityRuleID],
				[RuleVersionNum],
				[ServicePackageRunID],
				[DataEntityRefCode],
				[SeverityLevelRefCode],
				[DataSourceRefCode],
				[QualityIssueRefCode],
				[AttributeName],
				[OriginalValue],
				[TransformedValue],
				[MessageText],
				[ReferenceID],
				[CreatedDateTime],
				[CreatedBy],
				[KeyAttributeValue1],
				[KeyAttributeValue2],
				[KeyAttributeValue3],
				[KeyAttributeValue4],
				[KeyAttributeValue5]
			)

		SELECT
			0 [DataQualityRuleID],
			1 [RuleVersionNum],
			@ServicePackageRunID [ServicePackageRunID],
			@DataEntityRefCode [DataEntityRefCode],
			'ERROR' [SeverityLevelRefCode],
			@RecordSource [DataSourceRefCode],
			'ISSUE' [QualityIssueRefCode],
			CASE 
				WHEN HubSmartCardSID IS NULL THEN 'HubSmartCardSID'  
				--WHEN HubFareProductSID IS NULL THEN 'HubFareProductSID'  
				ELSE 'HubTerminalSid' END [AttributeName],
			NULL [OriginalValue],
			NULL [TransformedValue],
			'Unable to find '+ 
			CASE 
				WHEN HubSmartCardSID IS NULL THEN 'HubSmartCardSID'  
				--WHEN HubFareProductSID IS NULL THEN 'HubFareProductSID'  
				ELSE 'HubTerminalSid' END  +' when transforming LINK_CardFinanceTransaction from '+ @RecordSource [MessageText],
			'LinkCardFinanceTransaciontSid ' [ReferenceID],
			GETDATE() [CreatedDateTime],
			SYSTEM_USER [CreatedBy],
			CardSurfaceID [KeyAttributeValue1],
			PRODUCT_SERIAL_NO [KeyAttributeValue2],
			TerminalId [KeyAttributeValue3],
			TERMINAL_KEY  [KeyAttributeValue4],
			NULL [KeyAttributeValue5]
			
			FROM #tmpFailed_LinkCardFinancialTransaction
	


		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Update  STG.[MYKI_Link_CardFinancialTransaction] set ProcessStatus='EXTRACTED' WHERE ProcessStatus='EXTRACTPENDING'


		update STG.[MYKI_Link_CardFinancialTransaction]
		set STG.[MYKI_Link_CardFinancialTransaction].ProcessStatus='EXTRACTPENDING'
		FROM #tmpFailed_LinkCardFinancialTransaction FAILED_RECORDS 
		WHERE STG.[MYKI_Link_CardFinancialTransaction].LinkCardFinancialTransactionSid= 
		      FAILED_RECORDS.LinkCardFinancialTransactionSid



		Set @RowsFailed=(SELECT COUNT(1) FROM  #tmpFailed_LinkCardFinancialTransaction )
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)


	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

	truncate table #tmpFailed_LinkCardFinancialTransaction
	drop table #tmpFailed_LinkCardFinancialTransaction
			   



 END CATCH	
truncate table #tmpFailed_LinkCardFinancialTransaction
drop table #tmpFailed_LinkCardFinancialTransaction
End 


