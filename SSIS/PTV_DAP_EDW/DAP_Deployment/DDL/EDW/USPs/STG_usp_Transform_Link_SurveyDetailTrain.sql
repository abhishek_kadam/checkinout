/* *****************************************************************************
 * Name : [STG].[usp_Transform_Link_SurveyDetailTrain]
 *
 * Created :Mar 1st 2016
 * Author : KIRAN KUMAR
 *
 * Abstract: Data Transform for Link_SurveyDetailTrain
 *			 
 *
 * Input : 
 *		   
 * 
 * Output :
 *
 * 
 * ****************************************************************************
 */




IF OBJECT_ID('[STG].[usp_Transform_Link_SurveyDetailTrain]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Transform_Link_SurveyDetailTrain]

 

CREATE PROC [STG].[usp_Transform_Link_SurveyDetailTrain] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 




	/*count candidate rows to be processed */
	SET @RowsProcessed=
	/*(select count(1)
	
	from [STG].[TORS_Link_SurveyDetailTrain]
	where Rungroupid =@Rungroupid) */

(select count(1)
	
	from (
		select	*,ROW_NUMBER() over (Partition by Link_SurveyDetailTrainSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetailTrain] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1 )  

--	BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Link_SurveyDetailTrain]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO [STG].[RDV_Link_SurveyDetailTrain]
		([Link_SurveyDetailTrainSid],
		[Hub_SurveySid],
		[Hub_EntranceSid],
		[Hub_SmartCardSid],
		[LoadDate],
		[RecordSource],
		[ProcessStatus],
		[UpdateStatus],
		[RunGroupID]
		)
		select 
		SourceStaging.Link_SurveyDetailTrainSid,
		HS.HubSurveySid,
		HE.HubEntranceSid,  --Hub_StationEntrance data
		HSC.HubSmartCardSid,
		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when  DVTarget.[LinkSurveyDetailTrainSid] is null  then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		from 
		(
		select * from (
		select Link_SurveyDetailTrainSid,RawBatchID,EntranceID,SurveyorMykiNo1,
		ROW_NUMBER() over (Partition by Link_SurveyDetailTrainSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetailTrain] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1) SourceStaging
		INNER join DV.Hub_Survey HS  
						on HS.Survey_RawBatchID=SourceStaging.RawBatchID
		INNER join DV.Hub_StationEntrance HE  
						on HE.EntranceID=SourceStaging.EntranceID
		INNER join DV.Hub_SmartCard HSC   
						on HSC.CardSurfaceid = SourceStaging.SurveyorMykiNo1
		LEFT OUTER JOIN DV.[Link_SurveyDetailTrain] DVTarget  /*take target reference to identify if incoming rows are new or existing*/
						on DVTarget.LinkSurveyDetailTrainSid=SourceStaging.Link_SurveyDetailTrainSid

			------Extract Pending data collection -----
       
	   IF OBJECT_ID('[ #tmpFailed_Link_SurveyDetailTrain]', 'U') IS NOT NULL DROP TABLE [ #tmpFailed_Link_SurveyDetailTrain]
		---create tmp table reference for failed records 
		create table #tmpFailed_Link_SurveyDetailTrain
		WITH 
		  ( 
			CLUSTERED COLUMNSTORE INDEX,
			DISTRIBUTION = ROUND_ROBIN 
		  )
		AS

		
		select SourceStaging.Link_SurveyDetailTrainSid,
		HS.[HubSurveySid] ,
		HS.Survey_RawBatchID,
		HE.[HubEntranceSid] ,
		HE.EntranceId,
		HSC.[HubSmartCardSid] ,
		HSC.CardSurfaceid,
		SourceStaging.RawBatchID,
		--SourceStaging.EntranceId,
		SourceStaging.SurveyorMykiNo1,
		SourceStaging.SurveyDate,
		SourceStaging.SurveyorName1,
		SourceStaging.ActualSurveyStartTime,
		SourceStaging.ActualSurveyFinishTime,
		SourceStaging.CountOn,
		SourceStaging.Countoff,
		SourceStaging.ShiftID,
		SourceStaging.JobNumber,
		SourceStaging.Comment,
		SourceStaging.Mode,
		SourceStaging.ServiceProvider,
		SourceStaging.SurveyorName2,
		SourceStaging.SurveyorMykiNo2
		From
		(
	select * from (
		select *,ROW_NUMBER() over (Partition by Link_SurveyDetailTrainSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetailTrain] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1) SourceStaging

			LEFT OUTER JOIN DV.Hub_Survey HS  
						on HS.Survey_RawBatchID=SourceStaging.RawBatchID

			LEFT OUTER JOIN DV.Hub_StationEntrance HE  
						on HE.EntranceID=SourceStaging.EntranceID

			LEFT OUTER JOIN  DV.Hub_SmartCard HSC  
						on HSC.CardSurfaceid = SourceStaging.SurveyorMykiNo1
		WHERE   HSC.CardSurfaceID IS NULL OR
				HE.EntranceID IS NULL  OR 
				HS.Survey_RawBatchID IS NULL




				---Extract Pending data collection end-------
		
		
		--Data Quality table inserts
		INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2],
		[KeyAttributeValue3]
		
		)
		SELECT
		0 [DataQualityRuleID],
		1 [RuleVersionNum],
		@ServicePackageRunID [ServicePackageRunID],
		@DataEntityRefCode [DataEntityRefCode],
		'ERROR' [SeverityLevelRefCode],
		@RecordSource [DataSourceRefCode],   
		'ISSUE' [QualityIssueRefCode],
		CASE 
			WHEN HubSurveySid IS NULL THEN 'HubSurveySid' 
			WHEN HubEntranceSid IS NULL THEN 'HubSurveySid'
		ELSE
			 'HubSmartCardSid' 
			 END [AttributeName],
		NULL [OriginalValue],
		NULL [TransformedValue],
		'Unable to find ' + CASE 
								WHEN HubSurveySid IS  NULL THEN 'HubSurveySid' 
								WHEN HubEntranceSid IS  NULL THEN 'HubSurveySid'
								ELSE 'HubSmartCardSid' END + ' when transforming LINK_SurveyDetailTrain from '+ @RecordSource [MessageText],
		'Link_SurveyDetailTrainSid :' + 'SurveyDetailTrainSid'   [ReferenceID],
			--'Link_SurveyDetailTrainSid :'++isnull(cast(Link_SurveyDetailTrainSid as varchar(20)),'NULL')   [ReferenceID],
		GETDATE() [CreatedDateTime],
		SYSTEM_USER [CreatedBy],
		RawBatchID [KeyAttributeValue1],
		EntranceID [KeyAttributeValue2],
		CardSurfaceId [KeyAttributeValue3]
		FROM  #tmpFailed_Link_SurveyDetailTrain
		
		

		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Update   STG.[TORS_Link_SurveyDetailTrain] set ProcessStatus='EXTRACTED' WHERE ProcessStatus='EXTRACTPENDING'


		update STG.[TORS_Link_SurveyDetailTrain]
		set STG.[TORS_Link_SurveyDetailTrain].Processstatus='EXTRACTPENDING'
		FROM #tmpFailed_Link_SurveyDetailTrain FAILED_RECORDS 
		WHERE STG.[TORS_Link_SurveyDetailTrain].Link_SurveyDetailTrainSid= 
		      FAILED_RECORDS.Link_SurveyDetailTrainSid


		Set @RowsFailed=(SELECT COUNT(1) FROM LOG.DATAQUALITYLOG WHERE [ServicePackageRunID]=@ServicePackageRunID)
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   --COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		--ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

	truncate table #tmpFailed_Link_SurveyDetailTrain
	drop table #tmpFailed_Link_SurveyDetailTrain

 END CATCH	

 truncate table #tmpFailed_Link_SurveyDetailTrain
 drop table #tmpFailed_Link_SurveyDetailTrain

End 









