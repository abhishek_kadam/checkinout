/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_Sat_StationEntrance]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Sat_StationEntrance]

CREATE PROC [STG].[usp_Load_Sat_StationEntrance] @RunGroupId [int] AS
  
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_StationEntrance where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )
	/* Update records */
	
		UPDATE 	  DV.SAT_StationEntrance
		SET DV.SAT_StationEntrance.LoadEndDate =GETDATE(),
			DV.SAT_StationEntrance.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_StationEntrance STG 
			 WHERE  DV.SAT_StationEntrance.HubEntranceSid=STG.HubEntranceSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId	
	/* Insert NEW records */	
		INSERT INTO DV.SAT_StationEntrance
		(HubEntranceSid,
		EntranceName,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubEntranceSid,
		EntranceName,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_StationEntrance
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 
	COMMIT TRANSACTION		
End 

IF OBJECT_ID('[STG].[usp_Load_Sat_StationEntrance]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Sat_StationEntrance]