﻿IF OBJECT_ID('[STG].[usp_Load_Sat_ServiceDetail]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_ServiceDetail]
 END;
Go


CREATE PROC [STG].[usp_Load_Sat_ServiceDetail] @RunGroupId [int] AS

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_ServiceDetail
 *
 * Created :Feb 25th 2016
 * Author : Ramakrishna
 * Abstract: Loads data from staging to raw data vault for SAT_ServiceCalendar
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	SET  @RowsProcessed =
	(SELECT   COUNT(1) 
	FROM STG.RDV_SAT_ServiceDetail WHERE RunGroupid=@RunGroupId AND processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_ServiceDetail
		SET DV.SAT_ServiceDetail.LoadEndDate =GETDATE(),
			DV.SAT_ServiceDetail.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_ServiceDetail STG 
			 WHERE  DV.SAT_ServiceDetail.HubServiceSid=STG.HubServiceSid 
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' 
		AND STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
	INSERT INTO DV.SAT_ServiceDetail
	([HubServiceSid],	
	[ServiceNbr],
	[VehicleType],
	[DMon],
	[DTue],
	[DWed],
	[DThur],
	[DFri],
	[DSat],
	[DSun],
	[LoadDate],
	[LoadEndDate],
	[IsCurrentFlag],
	[RecordSource] 
		)
	SELECT 
	[HubServiceSid] , 
    [ServiceNbr],
	[VehicleType],
	[DMon],
	[DTue],
	[DWed],
	[DThur],
	[DFri],
	[DSat],
	[DSun],
	getdate() LoadDate,
	LoadEndDate,
	IsCurrentFlag,
	RecordSource
	FROM STG.RDV_SAT_ServiceDetail
	WHERE Updatestatus ='NEW' 
	AND ProcessStatus='TRANSFORMED' AND RunGroupid=@RunGroupId

	SET @RowsFailed=0  

	SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
