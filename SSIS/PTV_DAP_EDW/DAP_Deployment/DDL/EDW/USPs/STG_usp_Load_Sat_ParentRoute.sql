﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_ParentRoute
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 * Abstract: Loads data from staging to raw data vault for SAT_ParentRoute
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
IF OBJECT_ID('[STG].[usp_Load_Sat_ParentRoute]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_ParentRoute]
 END;
 Go

CREATE PROC [STG].[usp_Load_Sat_ParentRoute] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_ParentRoute where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_ParentRoute
		SET DV.SAT_ParentRoute.LoadEndDate =GETDATE(),
			DV.SAT_ParentRoute.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_ParentRoute STG 
			 WHERE  DV.SAT_ParentRoute.HubParentRouteSid=STG.HubParentRouteSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_ParentRoute 
	( [HubParentRouteSid], 
    [LineDescShort], 
    [LineDescLong], 
    [EffectiveDate], 
    [TermDate] ,
    [LineNotes] , 
    [PublicDescShort] , 
    [ReducedHolidayService], 
    [StopTemplate] , 
    [WheelChairAccess] , 
    [WheelChairAccessSat] , 
    [WheelChairAccessSun] , 
    [LoadDate] ,
    [LoadEndDate] ,
    [IsCurrentFlag], 
    [RecordSource] 
		)
		select   
	[HubParentRouteSid], 
    [LineDescShort], 
    [LineDescLong], 
    [EffectiveDate], 
    [TermDate] ,
    [LineNotes] , 
    [PublicDescShort] , 
    [ReducedHolidayService], 
    [StopTemplate] , 
    [WheelChairAccess] , 
    [WheelChairAccessSat] , 
    [WheelChairAccessSun] , 
    getdate() [LoadDate] ,
    [LoadEndDate] ,
    [IsCurrentFlag], 
    [RecordSource] 
		from STG.RDV_SAT_ParentRoute
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 