CREATE PROC [STG].[usp_Load_Sat_FareProduct] @RunGroupId [int] AS

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_TERMINALGROUPLOCATION
 *
 * Created :Feb 8th 2016
 * Author :Naveen Subramaniam
 *
 * Abstract: Loads data from staging to raw data vault for SAT_FareProduct
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * NAVEEN SUBRAMANIAM	1.0			08/02/2016	--		Initial version ( Need to add logica to handle processstatus LOADFAILED )
 *
 *
  */
BEGIN 

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int
	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_FAREPRODUCT where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )
	BEGIN TRANSACTION

	 BEGIN TRY
	
		/* Update records */
	
			UPDATE 	  DV.SAT_FAREPRODUCT 
			SET DV.SAT_FAREPRODUCT.LoadEndDate =GETDATE(),
				DV.SAT_FAREPRODUCT.IsCurrentFlag='N'

			FROM  STG.RDV_SAT_FAREPRODUCT STG 
				 WHERE  DV.SAT_FAREPRODUCT.HubFareProductSID=STG.HubFareProductSID
			AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
		/* Insert NEW records */	
			INSERT INTO DV.SAT_FAREPRODUCT 
			(
				[HubFareProductSid], 
				[ActionListSeqNo], 
				[Duration], 
				[FareProductStatId], 
				[FareProductTypeId], 
				[FirstUtilisationDate], 
				[LastUtilisationDate], 
				[NewProductSerialNo], 
				[Premium], 
				[ProcessingDate], 
				[ProductSerialNo], 
				[RefAmt], 
				[RefDate], 
				[ShiftNumber], 
				[ServiceProviderId], 
				[TransactionAmt], 
				[TransactionTimeStamp], 
				[Zone_Max], 
				[ZoneMin], 
				[LoadDate], 
				[LoadEndDate], 
				[IsCurrentFlag], 
				[RecordSource]			
			)
			select 
				[HubFareProductSid], 
    			[ActionListSeqNo], 
    			[Duration], 
    			[FareProductStatId], 
    			[FareProductTypeId], 
    			[FirstUtilisationDate], 
    			[LastUtilisationDate], 
    			[NewProductSerialNo], 
    			[Premium], 
    			[ProcessingDate], 
    			[ProductSerialNo], 
    			[RefAmt], 
    			[RefDate], 
    			[ShiftNumber], 
    			[ServiceProviderId], 
    			[TransactionAmt], 
    			[TransactionTimeStamp], 
    			[Zone_Max], 
    			[ZoneMin], 
				getdate() LoadDate,
				LoadEndDate,
				IsCurrentFlag,
				RecordSource
			from STG.RDV_SAT_FAREPRODUCT
			WHERE Updatestatus ='NEW' 
			and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

			set @RowsFailed=0  

			Select @RowsProcessed, @RowsFailed 

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH

		 IF(@@trancount>0)

		 BEGIN
 
		 set  @RowsFailed = @RowsProcessed
		 ROLLBACK TRANSACTION
		 Select @RowsProcessed, @RowsFailed
		 END
		 ;
		 THROW 50000,'Unable to load SAT_FAREPRODUCT from MYKI into data warehouse.',1;

	 END CATCH
END 
