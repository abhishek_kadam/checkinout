﻿/******************************************************************************
 * Name : STG.usp_Load_Sat_DateType
 *
 * Created :Feb 23th 2016
 * Author : Abhishek Kadam
 * Abstract: Loads data from staging to raw data vault for SAT_DateType
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * *****************************************************************************
 */

IF OBJECT_ID('[STG].[usp_Load_Sat_DateType]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_DateType]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_DateType] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_DATETYPE where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_DATETYPE 
		SET DV.SAT_DATETYPE.LoadEndDate =GETDATE(),
			DV.SAT_DATETYPE.IsCurrentFlag='N',
			DV.SAT_DATETYPE.LoadDate=GETDATE()

		FROM  STG.RDV_SAT_DATETYPE STG 
			 WHERE  DV.SAT_DATETYPE.[Date]=STG.[Date]
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId

	/* Insert NEW records */	
		INSERT INTO DV.SAT_DATETYPE 
		([Date],
		[DayTypeCategory],
		[DayType],
		[ABSWeek],
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select [Date],
			[DayTypeCategory],
			[DayType],
			[ABSWeek],
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_DATETYPE
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
