﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_ServiceCalendar
 *
 * Created :Feb 25th 2016
 * Author : Manoj Mayandi
 * Abstract: Loads data from staging to raw data vault for SAT_ServiceCalendar
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */

IF OBJECT_ID('[STG].[usp_Load_Sat_ServiceCalendar]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_ServiceCalendar]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_ServiceCalendar] @RunGroupId [int] AS

Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	SET  @RowsProcessed =
	(SELECT   COUNT(1) 
	FROM STG.RDV_SAT_ServiceCalendar WHERE RunGroupid=@RunGroupId AND processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_ServiceCalendar
		SET DV.SAT_ServiceCalendar.LoadEndDate =GETDATE(),
			DV.SAT_ServiceCalendar.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_ServiceCalendar STG 
			 WHERE  DV.SAT_ServiceCalendar.HubServiceSid=STG.HubServiceSid 
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' 
		AND STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
	INSERT INTO DV.SAT_ServiceCalendar
	([HubServiceSid] , 
    [Monday] , 
    [Tuesday] , 
    [Wednesday] , 
    [Thursday] , 
    [Friday] , 
    [Saturday] , 
    [Sunday] , 
    [StartDate] , 
    [EndDate] , 
    [LoadDate] , 
    [LoadEndDate] , 
    [IsCurrentFlag] , 
    [RecordSource] 
		)
	SELECT 
	[HubServiceSid] , 
    [Monday] , 
    [Tuesday] , 
    [Wednesday] , 
    [Thursday] , 
    [Friday] , 
    [Saturday] , 
    [Sunday] , 
    [StartDate] , 
    [EndDate] , 
	getdate() LoadDate,
	LoadEndDate,
	IsCurrentFlag,
	RecordSource
	FROM STG.RDV_SAT_ServiceCalendar
	WHERE Updatestatus ='NEW' 
	AND ProcessStatus='TRANSFORMED' AND RunGroupid=@RunGroupId

	SET @RowsFailed=0  

	SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
