/*****************************
Author :  Swathi
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_Link_CardFinanceTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Load_Link_CardFinanceTransaction]

CREATE PROC [STG].[usp_Load_Link_CardFinanceTransaction] @RunGroupId [int] AS
 
/* *****************************************************************************
 * Name : STG.usp_Load_Link_CardFinanceTransaction
 *
 * Created :March 9th 2016
 * Author : Swathi
 *
 * Abstract: Data Load for Link_TouchOffTransaction
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Swathi	1.0			09/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[RDV_Link_CardFinancialTransaction] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' and updatestatus='NEW') /*EXISTING are not processed */
			
	


		/* insert data into DW staging table */

		INSERT INTO DV.[Link_CardFinancialTransaction]
		([LinkCardFinancialTransactionSid],
		[TransactionDate] , 
		[TransactionTime] , 
		[HubSmartCardSid] , 
		[HubFareproductSid] , 
		[HubTerminalSid] , 
		[LoadDate] , 
		[RecordSource]
		)
		select 
		[LinkCardFinancialTransactionSid],
		[TransactionDate] , 
		[TransactionTime] , 
		[HubSmartCardSid] , 
		[HubFareproductSid] , 
		[HubTerminalSid] , 
		[LoadDate] , 
		[RecordSource]
		from STG.[RDV_Link_CardFinancialTransaction]
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Link_CardFinancialTransaction]
		set ProcessStatus='LOADED'
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId



		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   

 END TRY

 BEGIN CATCH

	
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








