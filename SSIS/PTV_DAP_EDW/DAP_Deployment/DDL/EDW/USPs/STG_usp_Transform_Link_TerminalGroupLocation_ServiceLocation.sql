﻿

IF OBJECT_ID('[STG].[usp_Transform_Link_TerminalGroupLocation_ServiceLocation]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Transform_Link_TerminalGroupLocation_ServiceLocation]
 END;
 Go

  

CREATE PROC [STG].[usp_Transform_Link_TerminalGroupLocation_ServiceLocation] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 
/* *****************************************************************************
 * Name : STG.usp_Transform_Link_TerminalGroupLocation_ServiceLocation
 *
 * Created :Feb 23rd 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Data Transform for Link_TerminalGroupLocation_ServiceLocation
 *			 
 *
 * Input : 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			23/02/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 







	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[MYKI_Link_TerminalGroupLocation_ServiceLocation] 
	where Rungroupid =@Rungroupid) 

	BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Link_TerminalGroupLocation_ServiceLocation]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 


		/* insert data into DW staging table */

		INSERT INTO STG.RDV_Link_TerminalGroupLocation_ServiceLocation
		([LinkTerminalGroupLocationServiceLocationSid],
		[HubServiceLocationSid],
		[HubTerminalGroupLocationSid],
		[LoadDate],
		[RecordSource],
		[ProcessStatus],
		[UpdateStatus],
		[RunGroupID]
		)
		select 
		--STG.GENERATEHASHKEY(ISNULL(SourceStaging.TERMINAL_ID,'')+'$'+ISNULL(SourceStaging.TERMINAL_GROUP_LOC_ID,''),'$') [LinkTerminalTerminalGroupLocationSid],
		cast(HASHBYTES('SHA1',ISNULL(SourceStaging.SERVICE_LOCATION_ID,'')+'$'+ISNULL(SourceStaging.TERMINAL_GROUP_LOC_ID,'')) as binary(20)), /*udf dont work in these scenerios hence direct call to hashkeys function ..need correction later*/ 
		HSL.HubServiceLocationSid,
		HTG.HubTerminalGroupLocationSid,
		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.HubServiceLocationSid is null then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		from 
		(
		select  SERVICE_LOCATION_ID,TERMINAL_GROUP_LOC_ID 
		from STG.[MYKI_Link_TerminalGroupLocation_ServiceLocation] 
		where Rungroupid =@Rungroupid ) SourceStaging 
		INNER join DV.Hub_ServiceLocation HSL 
						on HSL.ServiceLocationId=SourceStaging.SERVICE_LOCATION_ID
		INNER join DV.Hub_TerminalGroupLocation HTG 
						on HTG.TerminalGroupLocId=SourceStaging.TERMINAL_GROUP_LOC_ID
		LEFT OUTER JOIN DV.[Link_TerminalGroupLocation_ServiceLocation] DVTarget /*take target reference to identify if incoming rows are new or existing*/
						on DVTarget.HubServiceLocationSid=HSL.HubServiceLocationSid
						and DVTarget.[HubTerminalGroupLocationSid]=HTG.[HubTerminalGroupLocationSid]


		
		INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2]
		)
		SELECT
		0 [DataQualityRuleID],
		1 [RuleVersionNum],
		@ServicePackageRunID [ServicePackageRunID],
		@DataEntityRefCode [DataEntityRefCode],
		'ERROR' [SeverityLevelRefCode],
		@RecordSource [DataSourceRefCode],
		'ISSUE' [QualityIssueRefCode],
		CASE WHEN HSL.ServiceLocationId IS NULL THEN 'HubServiceLocationSid' ELSE 'HubTerminalGroupLocationSid' END [AttributeName],
		NULL [OriginalValue],
		NULL [TransformedValue],
		'Unable to find '+CASE WHEN HSL.ServiceLocationId IS NULL THEN 'HubServiceLocationSid' ELSE 'HubTerminalGroupLocationSid' END+' when transforming LINK_TERMINALGROUPLOCATION_SERVICELOCATION from '+ @RecordSource [MessageText],
		case when HSL.ServiceLocationId IS NULL  then 'SERVICE_LOCATION_ID :'+isnull(SourceStaging.SERVICE_LOCATION_ID,'NULL')  else  'TERMINAL_GROUP_LOC_ID :'+ISNULL(SourceStaging.TERMINAL_GROUP_LOC_ID,'NULL') end [ReferenceID],
		GETDATE() [CreatedDateTime],
		SYSTEM_USER [CreatedBy],
		SourceStaging.SERVICE_LOCATION_ID [KeyAttributeValue1],
		SourceStaging.TERMINAL_GROUP_LOC_ID [KeyAttributeValue2]
		FROM 
		(
		select  SERVICE_LOCATION_ID,TERMINAL_GROUP_LOC_ID
		from STG.[MYKI_Link_TerminalGroupLocation_ServiceLocation]  
		where Rungroupid =@Rungroupid ) SourceStaging 
		LEFT OUTER join DV.Hub_ServiceLocation HSL 
						on HSL.ServiceLocationId=SourceStaging.SERVICE_LOCATION_ID
		LEFT OUTER join DV.Hub_TerminalGroupLocation HTG 
						on HTG.TerminalGroupLocId=SourceStaging.TERMINAL_GROUP_LOC_ID
		WHERE HSL.ServiceLocationId IS NULL OR HTG.TerminalGroupLocId IS NULL
		

		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Set @RowsFailed=(SELECT COUNT(1) FROM LOG.DATAQUALITYLOG WHERE [ServicePackageRunID]=@ServicePackageRunID)
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








