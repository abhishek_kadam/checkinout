/*****************************
Author :  CHARAN KUMAR
Modified Date : 03/08/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Sat_CardFinanceTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Transform_Sat_CardFinanceTransaction]

CREATE PROC [STG].[usp_Transform_Sat_CardFinanceTransaction] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 
/* *****************************************************************************
 * Name : STG.usp_Transform_Sat_CardFinanceTransaction
 *
 * Created :March 8th 2016
 * Author : CHARAN KUMAR
 *
 * Abstract: Data Transform for Sat_CardFinanceTransaction
 *			 
 *
 * Input : 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * CHARAN KUMAR	 	 1.0		08/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 



	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	
	from (
		select	*,ROW_NUMBER() over (Partition by LinkCardFinancialTransactionSid Order by ExtractDateTime desc) RN
		from STG.[MYKI_Link_CardFinancialTransaction] 
		where Rungroupid =@Rungroupid and processstatus='EXTRACTED') TotalResultSet Where RN=1) 

	
	
		/* clear DW staging */
		
		

		delete 
		from   STG.[RDV_Sat_CardFinanceTransaction]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

		PRINT 'DW Staging Records Cleared'


		/* insert data into DW staging table */

		INSERT INTO STG.[RDV_Sat_CardFinanceTransaction]
		(   LinkCardFinancialTransactionSid ,
			[ActionListId] ,
			[StopSequenceNumber] ,
			[EntryPointId] ,
			[GstAmt] ,
			[GstRate],
			[PaymentCardTransactionSequenceNum] ,
			[PaymentTypeId] ,
			[ProductSerialNo] ,
			[RefundFeeAmt] ,
			[SettlementDate] ,
			[ServiceProviderId] ,
			[TerminalId] ,
			[PaymentCardId] ,
			[TpurseAmt] ,
			[TransactionAmt] ,
			[TransactionTimeStamp] ,
			[ZoneNo] ,
			[LoadDate] ,
			[TicketingBusinessDate] ,
			[LoadEndDate],
			[IsCurrentFlag],
			[RecordSource] ,
			[ProcessStatus] , 
			[UpdateStatus],
			[RunGroupID] 
		)
		select 
		LinkDVTarget.LinkTouchOffTransactionSid,
		cast(ACTIONLIST_ID as varchar(50)),
		cast(BUS_STOP as int),
		cast(ENTRY_POINT_ID as int),
		cast(GST_AMT as money),
		cast(GST_RATE as money),
		cast(LINE_NO as int),
		cast(REFPAYMENTTYPE.PAYMENT_TYPE_ID as int),
		cast(PRODUCT_SERIAL_NO as int),
		cast(REFUND_FEE_AMT as money),
		null as settlementdatekey,
		cast(REFSP.SP_ID as int),
		cast(REFTERMINAL.TERMINAL_ID as int),
		cast(LLSC_TPURSE_LOAD_CSN as bigint),
		cast(TPURSE_AMT as money),
		null as TransactionAmt,
		cast(TXN_TIMESTAMP as datetime),
		cast(ZONE_NO as int),
		GETDATE() [LoadDate],
		null as TicketingBusinessDate,
		null as LoadEndDate,
		'Y' as IsCurrentFlag,
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.LinkCardFinancialTransactionSid is null then 'NEW' else 'UPDATE' end  [UpdateStatus], 
		@RunGroupId [RunGroupID]

		from 
		(
		select * from (
		select *,ROW_NUMBER() over (Partition by LinkCardFinancialTransactionSid Order by ExtractDateTime desc) RN
		from STG.[MYKI_Link_CardFinancialTransaction] 
		where Rungroupid =@Rungroupid and ProcessStatus='EXTRACTED') TotalResultSet Where RN=1) SourceStaging

		--Link_CardFinancialTransaction
		INNER JOIN DV.Link_CardFinancialTransaction LinkDVTarget on LinkDVTarget.LinkCardFinancialTransactionSid=SourceStaging.LinkCardFinancialTransactionSid

		---TERMINAL	
		INNER JOIN STG.REF_DIM_TERMINAL  REFTERMINAL 
						on REFTERMINAL.TERMINAL_KEY = SourceStaging.TERMINAL_KEY 
		
		
		---Trx Time	
		INNER JOIN STG.REF_DIM_TIME  REFTIME
						on REFTIME.TIME_KEY = SourceStaging.TXN_TIME_KEY 

		--Payment Type
		inner join  [STG].[REF_DIM_PAYMENT_TYPE] REFPAYMENTTYPE ON REFPAYMENTTYPE.PAYMENT_TYPE_KEY=SourceStaging.PAYMENT_TYPE_KEY

		--Txn Type
		inner join [STG].[REF_DIM_TXN_TYPE] REFTXNTYPE on REFTXNTYPE.TXN_TYPE_KEY=SourceStaging.TXN_TYPE_KEY
	
		
		-- Smart Card 
		INNER JOIN   
		(select 0 DSCCARDKEY ,DSC_CARD_KEY LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_DSC_CARD
		UNION 
		select 1 DSCCARDKEY , LLSC_CARD_KEY,	CARD_SURFACE_ID from STG.REF_DIM_LLSC_CARD ) REF_SMARTCARD  
						on REF_SMARTCARD.LLSC_CARD_KEY = SourceStaging.LLSC_CARD_KEY AND 
							REF_SMARTCARD.DSCCARDKEY = SourceStaging.DSC_CARD_KEY
		
		INNER join DV.Hub_Smartcard HSC
								on HSC.CardSurfaceId=REF_SMARTCARD.CARD_SURFACE_ID
		
		--FAREPRODUCT 
		inner join DV.HUB_FAREPRODUCT HFP 
						on HFP.ProductSerialNumber=SourceStaging.PRODUCT_SERIAL_NO
							AND HFP.CardSurfaceId=HSC.CardSurfaceId

		-- SERVICEPROVIDER
		INNER JOIN STG.REF_DIM_SP REFSP ON REFSP.SP_KEY=SourceStaging.SP_KEY

		--Check records exists in RDV table
		left outer join DV.Sat_CardFinanceTransaction DVTarget on SourceStaging.LinkCardFinancialTransactionSid=DVTarget.LinkCardFinancialTransactionSid

		
		PRINT 'DW Staging Records Inserted'
		
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed



 END TRY

 BEGIN CATCH

	
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		
	END;
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed ; --send resultset back 

	THROW 
	


 END CATCH	


End 









		