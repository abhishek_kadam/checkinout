﻿

IF OBJECT_ID('[STG].[usp_Load_Link_TerminalGroupLocation_ServiceLocation]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Link_TerminalGroupLocation_ServiceLocation]
 END;
 Go

  

CREATE PROC [STG].[usp_Load_Link_TerminalGroupLocation_ServiceLocation] @RunGroupId [int] AS
 
/* *****************************************************************************
 * Name : STG.usp_Load_Link_TerminalGroupLocation_ServiceLocation
 *
 * Created :Feb 22nd 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Data Load for Link_TerminalGroupLocation_ServiceLocation
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			22/02/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[RDV_Link_TerminalGroupLocation_ServiceLocation] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' and updatestatus='NEW') /*EXISTING are not processed */
			
	BEGIN TRANSACTION

	

		/* insert data into DW staging table */

		INSERT INTO DV.[Link_TerminalGroupLocation_ServiceLocation]
		([LinkTerminalGroupLocationServiceLocationSid],
		[HubServiceLocationSid],
		[HubTerminalGroupLocationSid],
		[LoadDate],
		[RecordSource]
		)
		select 
		[LinkTerminalGroupLocationServiceLocationSid],
		[HubServiceLocationSid],
		[HubTerminalGroupLocationSid],
		[LoadDate],
		[RecordSource]
		from STG.[RDV_Link_TerminalGroupLocation_ServiceLocation]
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Link_TerminalGroupLocation_ServiceLocation]
		set ProcessStatus='LOADED'
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId



		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 












