﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_SHAPE
 *
 * Created :Feb 16th 2016
 * Author : Kiran Kumar
 * Abstract: Loads data from staging to raw data vault for SAT_SHAPE
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */



IF OBJECT_ID('[STG].[usp_Load_Sat_Shape]', 'U') IS NOT NULL
   DROP PROCEDURE [STG].[usp_Load_Sat_Shape]
   go

CREATE PROC [STG].[usp_Load_Sat_Shape] @RunGroupId [int] AS


Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_SHAPE where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_SHAPE
		SET DV.SAT_SHAPE.LoadEndDate =GETDATE(),
			DV.SAT_SHAPE.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_SHAPE STG 
			 WHERE  DV.SAT_SHAPE.HubShapeSid=STG.HubShapeSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_SHAPE 
		(HubShapeSid,
		ShapePointLatitude,
		ShapePointLongitude,
		ShapeDistanceTravelled,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubShapeSid,
			ShapePointLatitude,
			ShapePointLongitude,
			ShapeDistanceTravelled,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_SHAPE
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 

