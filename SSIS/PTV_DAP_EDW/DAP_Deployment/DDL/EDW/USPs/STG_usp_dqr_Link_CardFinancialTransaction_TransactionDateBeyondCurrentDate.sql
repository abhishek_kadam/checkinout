/*****************************
Author :  Naveen
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_dqr_Link_CardFinancialTransaction_TransactionDateBeyondCurrentDate]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_dqr_Link_CardFinancialTransaction_TransactionDateBeyondCurrentDate]


CREATE PROC [STG].[usp_dqr_Link_CardFinancialTransaction_TransactionDateBeyondCurrentDate] @DataQualityEntityID [int],@ServicePackageRunID [int],@RunGroupID [int] AS


/* *****************************************************************************
 * Name    : usp_dqr_Link_CardFinancialTransaction_TransactionDateBeyondCurrentDate
 *
 * Abstract: Data validation to check if Transaction Date greater than Current Date
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
  
    declare @errorMsg varchar(400)

    declare @dataQualityRuleID int=(select dqe.DataQualityRuleID from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataEntityRefCode varchar(50)=(select dqe.DataEntityRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataSourceRefCode varchar(50)=(select dqe.DataSourceRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @ruleVersionNum smallint=(select dqr.RuleVersionNum from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @severityLevelRefCode varchar(50)=(select dqr.SeverityLevelRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @description varchar(500)=(select dqr.[Description] from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    
    	

   
    --inserting record into log for failed records
     INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2],
		[KeyAttributeValue3],
		[KeyAttributeValue4],
		[KeyAttributeValue5],
		[KeyAttributeValue6],
		[KeyAttributeValue7],
		[KeyAttributeValue8],
		[KeyAttributeValue9]
		)
    select  @dataQualityRuleID
            ,@ruleVersionNum
            ,@ServicePackageRunID
            ,@dataEntityRefCode
            ,@severityLevelRefCode
            ,@dataSourceRefCode
            ,'ISSUE'
            ,'Transaction Date'
			,null
			,null
			,@description
			,cast(TransactionDate as varchar(50))
			,getdate()
            ,system_user
			,CardSurfaceId
			,TransactionDate
			,TransactionTime
            ,TerminalId
			,FareProductType
            ,ProductSerialNumber
			,NULL
			,NULL
			,NULL
	from   (select SC.CardSurfaceId,a.TransactionDate,a.TransactionTime,T.TerminalId,null as FareProductType,FP.ProductSerialNumber
			from 
			STG.[RDV_Link_CardFinancialTransaction] a 
			inner join DV.Hub_Smartcard SC on SC.HubSmartCardSid=a.HubSmartCardSid
			inner join DV.Hub_Terminal T on T.HubTerminalSid=a.HubTerminalSid
			inner join DV.HUB_FAREPRODUCT FP on FP.HubFareProductSid=a.HubFareProductSid

			where   RunGroupID = @runGroupID
			and   ProcessStatus = 'TRANSFORMED' and UpdateStatus='NEW' and a.TransactionDate>getdate()) main

	

end




