﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_HEADSIGN
 *
 * Created :Feb 22nd 2016
 * Author : Prakash Shanmugam
 * Abstract: Loads data from staging to raw data vault for SAT_HEADSIGN
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */

 IF OBJECT_ID('[STG].[usp_Load_Sat_Headsign]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Headsign]
 END;
 Go

CREATE PROC [STG].[usp_Load_Sat_Headsign] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_HEADSIGN where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_HEADSIGN
		SET DV.SAT_HEADSIGN.LoadEndDate =GETDATE(),
			DV.SAT_HEADSIGN.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_HEADSIGN STG 
			 WHERE  DV.SAT_HEADSIGN.HubTripSid=STG.HubTripSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId

	
	/* Insert NEW records */	
		INSERT INTO DV.SAT_HEADSIGN
		(	[HubTripSid],
			[Headsign], 
			[LoadDate],
			[LoadEndDate],
			[IsCurrentFlag],
			[RecordSource]
		)
		select	[HubTripSid],
				[Headsign], 
				getdate() LoadDate,
				LoadEndDate,
				IsCurrentFlag,
				RecordSource
		from STG.RDV_SAT_HEADSIGN
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
