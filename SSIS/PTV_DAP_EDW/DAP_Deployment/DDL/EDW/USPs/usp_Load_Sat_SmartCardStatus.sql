﻿
/* *****************************************************************************
 * Name : STG.usp_Load_Sat_SmartCardStatus
 *
 * Created :Feb 24th 2016
 * Author : Usha Rana
 *
 * Abstract: Loads data from staging to raw data vault for Sat_SmartCardStatus
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************

  */

  IF OBJECT_ID('[STG].[usp_Load_Sat_SmartCardStatus]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_SmartCardStatus]
 END;
Go


CREATE PROC [STG].[usp_Load_Sat_SmartCardStatus] @RunGroupId [int] AS
Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_SmartCardStatus where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_SmartCardStatus
		SET		  DV.Sat_SmartCardStatus.LoadEndDate =GETDATE(),
			      DV.Sat_SmartCardStatus.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_SmartCardStatus STG 
	    WHERE	  DV.Sat_SmartCardStatus.HubSmartCardSid=STG.HubSmartCardSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_SmartCardStatus 
		( HubSmartCardSid,
         ATPYEnableBit,
		 CardStatusDesc,
		 CardStatuslId,
		 CardRegInd,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	 HubSmartCardSid,
ATPYEnableBit,
CardStatusDesc,
CardStatuslId,
CardRegInd,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_SmartCardStatus

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 
