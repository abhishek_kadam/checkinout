/*****************************
Author :  Charan
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[usp_dqr_Sat_CardFinanceTransaction_SettlementDate]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_dqr_Sat_CardFinanceTransaction_SettlementDate]


CREATE PROC [STG].[usp_dqr_Sat_CardFinanceTransaction_SettlementDate] @DataQualityEntityID [int],@ServicePackageRunID [int],@RunGroupID [int] AS


/* *****************************************************************************
 * Name    : usp_dqr_Sat_CardFinanceTransaction_SettlementDate
 *
 * Abstract: Data validation to check if Settlement Date changes  and all other attributes remains same
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
  
    declare @errorMsg varchar(400)

    declare @dataQualityRuleID int=(select dqe.DataQualityRuleID from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataEntityRefCode varchar(50)=(select dqe.DataEntityRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataSourceRefCode varchar(50)=(select dqe.DataSourceRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @ruleVersionNum smallint=(select dqr.RuleVersionNum from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @severityLevelRefCode varchar(50)=(select dqr.SeverityLevelRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @description varchar(500)=(select dqr.[Description] from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    
    	

   
    --inserting record into log for failed records
     INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2],
		[KeyAttributeValue3],
		[KeyAttributeValue4],
		[KeyAttributeValue5],
		[KeyAttributeValue6],
		[KeyAttributeValue7],
		[KeyAttributeValue8],
		[KeyAttributeValue9]
		)
    select  @dataQualityRuleID
            ,@ruleVersionNum
            ,@ServicePackageRunID
            ,@dataEntityRefCode
            ,@severityLevelRefCode
            ,@dataSourceRefCode
            ,'ISSUE'
            ,'Settlement Date'
			,null
			,null
			,@description
			,cast(SettlementDate as varchar(50))
			,getdate()
            ,system_user
			,null 
			,TerminalId
			,null
			,TransactionDate
			,TransactionTime
            ,SettlementDate
			,PaymentTypeId
            ,null
			,null
	from   (SELECT sourceStaging.* FROM 
	
			(select LinkCardFinancialTransactionSid ,
			[ActionListId] ,
			[StopSequenceNumber] ,
			[EntryPointId] ,
			[GstAmt] ,
			[GstRate],
			[PaymentCardTransactionSequenceNum] ,
			[PaymentTypeId] ,
			[ProductSerialNo] ,
			[RefundFeeAmt] ,
			[SettlementDate] ,
			[ServiceProviderId] ,
			[TerminalId] ,
			[PaymentCardId] ,
			[TpurseAmt] ,
			[TransactionAmt] ,
			[TransactionTimeStamp] ,
			[ZoneNo] ,
			[TicketingBusinessDate] from STG.RDV_Sat_CardFinanceTransaction
			where   RunGroupID = @runGroupID
			and   ProcessStatus = 'TRANSFORMED' and UpdateStatus='NEW')sourceStaging

			INNER JOIN DV.Sat_CardFinanceTransaction DVTarget on    DVTarget.LinkCardFinancialTransactionSid=sourceStaging.LinkCardFinancialTransactionSid	 and 		
																	DVTarget.[ActionListId]=sourceStaging.[ActionListId] and
																	DVTarget.[StopSequenceNumber]=sourceStaging.[StopSequenceNumber] and
																	DVTarget.[EntryPointId]=sourceStaging.[EntryPointId] and
																	DVTarget.[GstAmt] =sourceStaging.[GstAmt] and
																	DVTarget.[GstRate]=sourceStaging.[GstRate] and
																	DVTarget.[PaymentCardTransactionSequenceNum]=sourceStaging.[PaymentCardTransactionSequenceNum] and
																	DVTarget.[PaymentTypeId]=sourceStaging.[PaymentTypeId] and
																	DVTarget.[ProductSerialNo] =sourceStaging.[ProductSerialNo] and
																	DVTarget.[RefundFeeAmt] =sourceStaging.[RefundFeeAmt] and
																	DVTarget.[ServiceProviderId]=sourceStaging.[ServiceProviderId] and
																	DVTarget.[TerminalId]=sourceStaging.[TerminalId] and
																	DVTarget.[PaymentCardId]=sourceStaging.[PaymentCardId] and
																	DVTarget.[TpurseAmt] =sourceStaging.[TpurseAmt]and
																	DVTarget.[TransactionAmt]=sourceStaging.[TransactionAmt] and
																	DVTarget.[TransactionTimeStamp]=sourceStaging.[TransactionTimeStamp] and
																	DVTarget.[ZoneNo]=sourceStaging.[ZoneNo] and
																	DVTarget.[TicketingBusinessDate]=sourceStaging.[TicketingBusinessDate]
			WHERE 
			sourceStaging.[SettlementDate]<>DVTarget.[SettlementDate]) main



end







