/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/02/2016
****************************/

IF OBJECT_ID('[STG].[usp_Extract_Link_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Extract_Link_TouchOnTransaction]

CREATE PROC [STG].[usp_Extract_Link_TouchOnTransaction] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS
 
/* *****************************************************************************
 * Name : STG.usp_Extract_Link_TouchOnTransaction
 *
 * Created :March 2nd 2016
 * Author : Naveen Subramaniam
 *
 * Abstract: Data Extract for Link_TouchOnTransaction
 *			 
 *
 * Input : @RunGroupId Rungroupid from DAF.Rungroup
 *		   @LastExtractLoadedDateTime last extract for the rungroup
 *		   
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			02/03/2016	--		Initial version  
 *
 */
Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

		/* clear source staging */
		delete from  STG.[MYKI_Link_TouchOnTransaction] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'

	
		/* insert data into source staging table */


		INSERT INTO STG.[MYKI_Link_TouchOnTransaction]
		(	
			LinkTouchOnTransactionSid,
			CARD_KEY, 
			TXN_DATE_KEY,
			TXN_TIME_KEY, 
			TERMINAL_KEY, 
			ROUTE_STOP_KEY,
			PRODUCT_SERIAL_NO, 
			FAREPRODUCT_TYPE_KEY, 
			TRANSCTION_TYPE, 
			REC_UPDT_DT, 
			STOP_LOCATION_KEY, 
			DSC_CARD_KEY, 
			LLSC_CARD_KEY, 
			TXN_TIMESTAMP,
			LLSC_CARD_SURFACE_ID,
			DSC_CARD_SURFACE_ID,
			[PROCESSING_DATE],
			[BUS_STOP],
			[ENTRY_LOC_ID],
			[SP_ID],
			[ZONE_MAX],
			[ZONE_MIN],
			[BUSINESS_DATE],
			RunGroupID,
			ProcessStatus,
			ExtractDateTime
		)

		SELECT 
			Cast(HASHBYTES('SHA1',case when TXN_DATE_KEY ='NULL' then '' else TXN_DATE_KEY end +
								  case when TXN_TIME_KEY ='NULL' then '' else TXN_TIME_KEY end + 
								  case when SourceStaging.LLSC_CARD_KEY ='NULL' then '' else SourceStaging.LLSC_CARD_KEY end +
								  case when SourceStaging.DSC_CARD_KEY ='NULL' then '' else SourceStaging.DSC_CARD_KEY end +
								  case when PRODUCT_SERIAL_NO ='NULL' then '' else PRODUCT_SERIAL_NO end +
								  case when TERMINAL_KEY ='NULL' then '' else TERMINAL_KEY end +
								  case when ROUTE_STOP_KEY ='NULL' then '' else ROUTE_STOP_KEY end +
								  case when STOP_LOCATION_KEY ='NULL' then '' else STOP_LOCATION_KEY end
								  ) as binary(20)) as LinkTouchOnTransactionSid, /*udf dont work in these scenerios hence direct call to hashkeys function ..need correction later*/ 
		    case when CARD_KEY ='NULL' then NULL else CARD_KEY end CARD_KEY,
			case when TXN_DATE_KEY ='NULL' then NULL else TXN_DATE_KEY end TXN_DATE_KEY,
			case when TXN_TIME_KEY ='NULL' then NULL else TXN_TIME_KEY end TXN_TIME_KEY,
			case when TERMINAL_KEY ='NULL' then NULL else TERMINAL_KEY end TERMINAL_KEY,
			case when ROUTE_STOP_KEY ='NULL' then NULL else ROUTE_STOP_KEY end ROUTE_STOP_KEY,
			case when PRODUCT_SERIAL_NO ='NULL' then NULL else PRODUCT_SERIAL_NO end PRODUCT_SERIAL_NO,
			case when FAREPRODUCT_TYPE_KEY ='NULL' then NULL else FAREPRODUCT_TYPE_KEY end FAREPRODUCT_TYPE_KEY,
			case when TRANSCTION_TYPE ='NULL' then NULL else TRANSCTION_TYPE end TRANSCTION_TYPE,
			case when SourceStaging.REC_UPDT_DT ='NULL' then NULL else SourceStaging.REC_UPDT_DT end REC_UPDT_DT,
			case when STOP_LOCATION_KEY='NULL' then NULL else STOP_LOCATION_KEY end STOP_LOCATION_KEY,
			case when SourceStaging.DSC_CARD_KEY ='NULL' then NULL else SourceStaging.DSC_CARD_KEY end DSC_CARD_KEY,
			case when SourceStaging.LLSC_CARD_KEY ='NULL' then NULL else SourceStaging.LLSC_CARD_KEY end LLSC_CARD_KEY,
			case when TXN_TIMESTAMP ='NULL' then NULL else TXN_TIMESTAMP end TXN_TIMESTAMP,
			LLSC.CARD_SURFACE_ID  LLSC_CARD_SURFACE_ID,
			DSC.CARD_SURFACE_ID  DSC_CARD_SURFACE_ID,
			case when SourceStaging.[PROCESSING_DATE] ='NULL' then NULL else SourceStaging.[PROCESSING_DATE] end [PROCESSING_DATE],
			case when SourceStaging.[BUS_STOP] ='NULL' then NULL else SourceStaging.[BUS_STOP] end [BUS_STOP],
			case when SourceStaging.[ENTRY_LOC_ID] ='NULL' then NULL else SourceStaging.[ENTRY_LOC_ID] end [ENTRY_LOC_ID],
			case when SourceStaging.[SP_KEY] ='NULL' then NULL else SourceStaging.[SP_KEY] end [SP_KEY],
			case when SourceStaging.[ZONE_MAX] ='NULL' then NULL else SourceStaging.[ZONE_MAX] end [ZONE_MAX],
			case when SourceStaging.[ZONE_MIN] ='NULL' then NULL else SourceStaging.[ZONE_MIN] end [ZONE_MIN],
			case when SourceStaging.[BUSINESS_DATE] ='NULL' then NULL else SourceStaging.[BUSINESS_DATE] end [BUSINESS_DATE],			
			@RunGroupId RunGroupID,'EXTRACTED' [ProcessStatus] ,getdate() [ExtractDateTime]
			FROM [EXT].[MYKI_FACT_SCAN_ON_TRANSACTION] SourceStaging
			

					LEFT OUTER join STG.REF_DIM_LLSC_CARD LLSC
						On LLSC.LLSC_CARD_KEY = 
								CASE WHEN SourceStaging.DSC_CARD_KEY = 1 THEN
									SourceStaging.LLSC_CARD_KEY
								END
					
					LEFT OUTER join STG.REF_DIM_DSC_CARD DSC
						On DSC.DSC_CARD_KEY = 
								CASE WHEN SourceStaging.DSC_CARD_KEY <> 1 THEN
									SourceStaging.DSC_CARD_KEY
								END
				
			 WHERE  CARD_KEY <>'CARD_KEY' 
		

	    set   @RowsProcessed=(select count(1) from STG.[MYKI_Link_TouchOnTransaction] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED' )

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed




 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)


		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 
