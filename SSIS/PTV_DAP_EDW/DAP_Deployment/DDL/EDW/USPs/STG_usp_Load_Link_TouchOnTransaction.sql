/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/05/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_Link_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Load_Link_TouchOnTransaction]


CREATE PROC [STG].[usp_Load_Link_TouchOnTransaction] @RunGroupId [int] AS
 
/* *****************************************************************************
 * Name : STG.usp_Load_Link_FareProductUsage
 *
 * Created :March 5th 2016
 * Author :Naveen Subramaniam
 *
 * Abstract: Data Load for Link_TouchOnTransaction
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			05/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[RDV_Link_TouchOnTransaction] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' and updatestatus='NEW') /*EXISTING are not processed */
			
	BEGIN TRANSACTION

	

		/* insert data into DW staging table */

		INSERT INTO DV.[Link_TouchOnTransaction]
		(
			[LinkTouchOnTransactionSid],
			[TransactionDate],
			[TransactionTime],
			[HubStopSid],
			[HubSmartCardSid],
			[HubFareProductSid], 
			[HubRouteSid],
			[HubTerminalSid], 
			[LoadDate], 
			[RecordSource]		 
		)
		select 
			[LinkTouchOnTransactionSid],
			[TransactionDate],
			[TransactionTime],
			[HubStopSid],
			[HubSmartCardSid],
			[HubFareProductSid], 
			[HubRouteSid],
			[HubTerminalSid], 
			[LoadDate], 
			[RecordSource]	 
		from STG.[RDV_Link_TouchOnTransaction]
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Link_TouchOnTransaction]
		set ProcessStatus='LOADED'
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/* Quality Data Update  */

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








