﻿/* *****************************************************************************
 * Name : STG.usp_Load_SAT_Entrance
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 *
 * Abstract: Loads data from staging to raw data vault for SAT_Entrance
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
  */

IF OBJECT_ID('[STG].[usp_Load_Sat_Entrance]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Entrance]
 END;
 Go


CREATE PROC [STG].[usp_Load_Sat_Entrance] @RunGroupId [int] AS

Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_Entrance where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_Entrance 
		SET DV.SAT_Entrance.LoadEndDate =GETDATE(),
			DV.SAT_Entrance.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_Entrance STG 
			 WHERE  DV.SAT_Entrance.HubLocationEntranceSid=STG.HubLocationEntranceSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_Entrance
		([HubLocationEntranceSid], 
    [POIEntranceID], 
    [Latitude], 
    [Longitude], 
    [StreetNumber], 
    [StreetName], 
    [Suburb], 
    [LoadDate], 
    [LoadEndDate], 
    [IsCurrentFlag], 
    [RecordSource]
		)
		select [HubLocationEntranceSid], 
    [POIEntranceID], 
    [Latitude], 
    [Longitude], 
    [StreetNumber], 
    [StreetName], 
    [Suburb], 
	getdate() LoadDate,
	[LoadEndDate], 
    [IsCurrentFlag], 
    [RecordSource]
		from STG.RDV_SAT_Entrance
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
