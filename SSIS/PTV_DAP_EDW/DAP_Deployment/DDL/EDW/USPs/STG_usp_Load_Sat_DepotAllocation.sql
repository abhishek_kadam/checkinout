﻿/******************************************************************************
 * Name : STG.usp_Load_Sat_DepotAllocation
 *
 * Created :Feb 16th 2016
 * Author : Abhishek Kadam
 * Abstract: Loads data from staging to raw data vault for SAT_DepotAllocation
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * *****************************************************************************
 */

IF OBJECT_ID('[STG].[usp_Load_Sat_DepotAllocation]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_DepotAllocation]
 END;
Go

  

CREATE PROC [STG].[usp_Load_Sat_DepotAllocation] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_DepotAllocation where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_DepotAllocation 
		SET DV.SAT_DepotAllocation.LoadEndDate =GETDATE(),
			DV.SAT_DepotAllocation.IsCurrentFlag='N',
			DV.SAT_DepotAllocation.LoadDate=GETDATE()

		FROM  STG.RDV_SAT_DepotAllocation STG 
			 WHERE  DV.SAT_DepotAllocation.HubDepotAllocationSid=STG.HubDepotAllocationSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId

	/* Insert NEW records */	
		INSERT INTO DV.SAT_DepotAllocation
		(
		    [HubDepotAllocationSid], 
			[DepotAllocationDate], 
			[ServicesAllocatedCount], 
			[ServicesLoadedCount], 
			[LoadDate], 
			[LoadEndDate], 
			[IsCurrentFlag], 
			[RecordSource]
		)
		select 
		    [HubDepotAllocationSid], 
			[DepotAllocationDate], 
			[ServicesAllocatedCount], 
			[ServicesLoadedCount], 
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_DepotAllocation
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
