﻿

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_VEHICLE
 *
 * Created :Feb 22nd 2016
 * Author :Prakash Shanmugam
 * Abstract: Loads data from staging to raw data vault for SAT_VEHICLE
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */

 IF OBJECT_ID('[STG].[usp_Load_Sat_Vehicle]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Vehicle]
 END;
 Go

CREATE PROC [STG].[usp_Load_Sat_Vehicle] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_VEHICLE where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )
		
	/* Update records */
	
		UPDATE 	  [DV].[SAT_VEHICLE]
		SET [DV].[SAT_VEHICLE].LoadEndDate = GETDATE(),
			[DV].[SAT_VEHICLE].IsCurrentFlag='N'

		FROM  [STG].[RDV_SAT_VEHICLE] STG 
			 WHERE  [DV].[SAT_VEHICLE].HubVehicleSid = STG.HubVehicleSid
		AND   STG.Updatestatus ='UPDATED' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid = @RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO [DV].[SAT_VEHICLE] 
		(HubVehicleSid,
		Name,
		CompanyName,		
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select 
			HubVehicleSid,
			Name,
			CompanyName,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from [STG].[RDV_SAT_VEHICLE] 
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
