﻿

IF OBJECT_ID('[STG].[usp_Extract_Link_ServiceLocation_ServiceProvider]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Extract_Link_ServiceLocation_ServiceProvider]
 END;
 Go

  

CREATE PROC [STG].[usp_Extract_Link_ServiceLocation_ServiceProvider] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS
 
/* *****************************************************************************
 * Name : STG.usp_Extract_Link_ServiceLocation_ServiceProvider
 *
 * Created :Feb 23rd 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Data Extract for Link_ServiceLocation_ServiceProvider
 *			 
 *
 * Input : @RunGroupId Rungroup id of data acquisition 
 *		   @LastExtractLoadedDateTime last extract date time of source file
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			23/02/2016	--		Initial version  
 *
 */
Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1) from 
	(SELECT distinct [SVCE_LOC_ID],SERVICEPROVIDERID
	FROM EXT.MYKI_DIM_SERVICE_LOCATION  WHERE   [SVCE_LOC_ID] <>'SVCE_LOC_ID' ) a) /* checks sanity of external table as well */
	


	BEGIN TRANSACTION


		/* clear source staging */
		delete from  STG.[MYKI_Link_ServiceLocation_ServiceProvider] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'

	
		/* insert data into source staging table */

		INSERT INTO STG.[MYKI_Link_ServiceLocation_ServiceProvider]
		([SVCE_LOC_ID] , 
		 SERVICEPROVIDERID ,
		 [RunGroupID] ,
		 [ProcessStatus],
		 [ExtractDateTime])
		SELECT distinct case when [SVCE_LOC_ID] ='NULL' then NULL else [SVCE_LOC_ID] end [SVCE_LOC_ID],
		case when SERVICEPROVIDERID ='NULL'  then null else SERVICEPROVIDERID end  SERVICEPROVIDERID,
		@RunGroupId,'EXTRACTED' [ProcessStatus] ,getdate() [ExtractDateTime]
		FROM EXT.MYKI_DIM_SERVICE_LOCATION   WHERE [SVCE_LOC_ID] <>'SVCE_LOC_ID'  
		
		
		/*Purge extract Pending*/
		---Delete from STG.Extractpending where rungroupid =@RunGroupId--NOT REQUIRED 

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION

		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








