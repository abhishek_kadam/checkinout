﻿
/* *****************************************************************************
 * Name : STG.[usp_Load_Sat_Point]
 *
 * Created :3rd March 2016
 * Author : Prakash
 * Abstract: Loads data from staging to raw data vault for SAT_Point
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
IF OBJECT_ID('[STG].[usp_Load_Sat_Point]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Point]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_Point] @RunGroupId [int] AS


Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_POINT where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_POINT
		SET DV.SAT_POINT.LoadEndDate =GETDATE(),
			DV.SAT_POINT.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_POINT STG 
			 WHERE  DV.SAT_POINT.[HubLocationPointSid] = STG.[HubLocationPointSid]
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId

	/* Insert NEW records */	
		INSERT INTO DV.SAT_POINT
	(	[HubLocationPointSid], 
		[LoadDate] ,
		[PointName] ,
		[Latitude] ,
		[Longitude] , 
		[LoadEndDate],   
		[IsCurrentFlag], 
		[RecordSource] 
		)
		select   
		[HubLocationPointSid], 
		getdate() [LoadDate] ,
		[PointName] ,
		[Latitude] ,
		[Longitude] , 
		[LoadEndDate],   
		[IsCurrentFlag], 
		[RecordSource]
		from STG.RDV_SAT_POINT
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
