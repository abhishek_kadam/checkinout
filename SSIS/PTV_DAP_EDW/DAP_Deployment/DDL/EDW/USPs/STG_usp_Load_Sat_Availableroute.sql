﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_ParentRoute
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 * Abstract: Loads data from staging to raw data vault for SAT_AvailableRoute
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
 IF OBJECT_ID('[STG].[usp_Load_Sat_AvailableRoute]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_AvailableRoute]
 END;
 Go
 
CREATE PROC [STG].[usp_Load_Sat_AvailableRoute] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_AvailableRoute where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_AvailableRoute
		SET DV.SAT_AvailableRoute.LoadEndDate =GETDATE(),
			DV.SAT_AvailableRoute.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_AvailableRoute STG 
			 WHERE  DV.SAT_AvailableRoute.Hub_AvailableRouteSid=STG.Hub_AvailableRouteSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_AvailableRoute
	( [Hub_AvailableRouteSid], 
    [RouteDesc] ,
    [RouteFrom] ,
    [RouteTo] ,
    [RouteVia] , 
    [WheelChairAccess], 
    [Myki], 
    [LoadDate] ,
    [LoadEndDate] ,
    [IsCurrentFlag], 
    [RecordSource] 
		)
		select   
	 [Hub_AvailableRouteSid], 
    [RouteDesc] ,
    [RouteFrom] ,
    [RouteTo] ,
    [RouteVia] , 
    [WheelChairAccess], 
    [Myki], 
    getdate() [LoadDate] ,
    [LoadEndDate] ,
    [IsCurrentFlag], 
    [RecordSource] 
		from STG.RDV_SAT_AvailableRoute
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 