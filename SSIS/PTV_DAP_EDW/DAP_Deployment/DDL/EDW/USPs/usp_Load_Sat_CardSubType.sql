﻿

/* *****************************************************************************
 * Name : STG.usp_Load_Sat_CardSubType
 *
 * Created :Feb 22nd 2016
 * Author : Kiran Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_CardSubType
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************
 
  */



IF OBJECT_ID('[STG].[usp_Load_Sat_CardSubType]', 'U') IS NOT NULL
   DROP PROCEDURE [STG].[usp_Load_Sat_CardSubType] 

go




CREATE PROC [STG].[usp_Load_Sat_CardSubType] @RunGroupId [int] AS



Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_CardSubType where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_CardSubType 
		SET		  DV.Sat_CardSubType.LoadEndDate =GETDATE(),
			      DV.Sat_CardSubType.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_CardSubType STG 
	    WHERE	  DV.Sat_CardSubType.HubCardSubTypeSid=STG.HubCardSubTypeSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_CardSubType
		(HubCardSubTypeSid,
		CardSubtypeDesc,
		FareType,
		FareSubType,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	HubCardSubTypeSid,
		CardSubtypeDesc,
		FareType,
		FareSubType,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_CardSubType

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 