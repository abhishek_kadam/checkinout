﻿
IF OBJECT_ID('[STG].[usp_Load_Sat_ServiceException]', 'P') IS NOT NULL
DROP PROCEDURE [STG].[usp_Load_Sat_ServiceException]
GO


CREATE PROC [STG].[usp_Load_Sat_ServiceException] @RunGroupId [int] AS

/* *****************************************************************************
 * SP Name : STG.usp_Load_SAT_ServiceException
 *
 * Created :Mar 2nd 2016
 * Author : Manoj Mayandi
 * Abstract: Loads data from staging to raw data vault for SAT_ServiceException
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	SET  @RowsProcessed =
	(SELECT   COUNT(1) 
	FROM STG.RDV_SAT_ServiceException WHERE RunGroupid=@RunGroupId AND processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_ServiceException
		SET DV.SAT_ServiceException.LoadEndDate =GETDATE(),
			DV.SAT_ServiceException.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_ServiceException STG 
		WHERE  DV.SAT_ServiceException.HubServiceSid=STG.HubServiceSid 
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' 
		AND STG.RunGroupid=@RunGroupId

	
		
	/* Insert NEW records */	
	INSERT INTO DV.SAT_ServiceException
	(
	[HubServiceSid] , 
    [date] , 
	[exception_type], 
    [LoadDate] , 
    [LoadEndDate] , 
    [IsCurrentFlag] , 
    [RecordSource] 
	)
	SELECT 
	[HubServiceSid] , 
    [date] , 
	[exception_type],
	getdate() LoadDate,
	LoadEndDate,
	IsCurrentFlag,
	RecordSource
	FROM STG.RDV_SAT_ServiceException
	WHERE Updatestatus ='NEW' 
	AND ProcessStatus='TRANSFORMED' AND RunGroupid=@RunGroupId

	SET @RowsFailed=0  

	SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
