
/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_Link_SurveyDetail]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Link_SurveyDetail]

CREATE PROC [STG].[usp_Load_Link_SurveyDetail] @RunGroupId [int] AS

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[RDV_Link_SurveyDetail] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' and updatestatus='NEW') /*EXISTING are not processed */
			
	BEGIN TRANSACTION
	

		/* insert data into DW staging table */

		INSERT INTO DV.[Link_SurveyDetail]
		([LinkSurveyDetailSid],
		[HubSurveySid],
		[HubSmartCardSid],
		[HubStopSid],
		[HubVehicleSid],
		[LoadDate],
		[RecordSource]
		)
		select 
		[Link_SurveyDetailSid],
		[Hub_SurveySid],
		[Hub_SmartCardSid],
		[Hub_StopSid],
		[Hub_VehicleSid],
		[LoadDate],
		[RecordSource]
		from STG.[RDV_Link_SurveyDetail]
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Link_SurveyDetail]
		set ProcessStatus='LOADED'
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId



		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 
