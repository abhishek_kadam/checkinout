﻿/* *****************************************************************************
 * Name : [usp_Load_Sat_StopDetail]
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 *
 * Abstract: Loads data from staging to raw data vault for SAT_StopDetail
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
  */

IF OBJECT_ID('[STG].[usp_Load_Sat_StopDetail]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_StopDetail]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_StopDetail] @RunGroupId [int] AS

Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_StopDetail where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_StopDetail 
		SET DV.SAT_StopDetail.LoadEndDate =GETDATE(),
			DV.SAT_StopDetail.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_StopDetail STG 
			 WHERE  DV.SAT_StopDetail.HubStopSid=STG.HubStopSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_StopDetail
		(HubStopSid,
		 [PrimaryStopName],
		 [SecondaryStopName] ,
		 [StreetAddress] ,
		 [Suburb], 
		 [StopLatitude] ,
		 [StopLongitude] , 
		 [StopType], 
		 [BayNbr] , 
		 [PlatformNumber], 
		 [StopNumber], 
		 [StopSpecName] ,  
		 [LowFloorAccess] , 
		 [TimetableCode] , 
		 [StopLandmark] ,  
		 [TrainNbrPlatforms] , 
		 [StationName] , 
		 [TrainTimeTable] , 
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubStopSid,
		 [PrimaryStopName],
		 [SecondaryStopName] ,
		 [StreetAddress] ,
		 [Suburb], 
		 [StopLatitude] ,
		 [StopLongitude] , 
		 [StopType], 
		 [BayNbr] , 
		 [PlatformNumber], 
		 [StopNumber], 
		 [StopSpecName] ,  
		 [LowFloorAccess] , 
		 [TimetableCode] , 
		 [StopLandmark] ,  
		 [TrainNbrPlatforms] , 
		 [StationName] , 
		 [TrainTimeTable] , 
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_STOPDetail
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
