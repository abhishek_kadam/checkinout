﻿

/* *****************************************************************************
 * Name : STG.usp_Load_Sat_PaymentType
 *
 * Created :Feb 16th 2016
 * Author : Kiran Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_PaymentType
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************
  
 */

IF OBJECT_ID('[STG].[usp_Load_Sat_PaymentType]', 'U') IS NOT NULL
   DROP PROCEDURE [STG].[usp_Load_Sat_PaymentType]

go


CREATE PROC [STG].[usp_Load_Sat_PaymentType] @RunGroupId [int] AS




Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_PaymentType where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_PaymentType 
		SET		  DV.Sat_PaymentType.LoadEndDate =GETDATE(),
			      DV.Sat_PaymentType.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_PaymentType  STG 
	    WHERE	  DV.Sat_PaymentType.HubPaymentTypeSid=STG.HubPaymentTypeSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_PaymentType 
		(HubPaymentTypeSid,
		PaymentTypeDescription,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	HubPaymentTypeSid,
				PaymentTypeDescription,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_PaymentType

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 