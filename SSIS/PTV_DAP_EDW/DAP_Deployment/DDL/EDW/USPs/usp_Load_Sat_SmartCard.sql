﻿


/* *****************************************************************************
 * Name : STG.usp_Load_Sat_SmartCard
 *
 * Created :Feb 23rd 2016
 * Author : Kiran Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_SmartCard
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************

  */



IF OBJECT_ID('[STG].[usp_Load_Sat_SmartCard]', 'U') IS NOT NULL
   DROP PROCEDURE [STG].[usp_Load_Sat_SmartCard] 

go




CREATE PROC [STG].[usp_Load_Sat_SmartCard] @RunGroupId [int] AS



Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_SmartCard where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_SmartCard 
		SET		  DV.Sat_SmartCard.LoadEndDate =GETDATE(),
			      DV.Sat_SmartCard.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_SmartCard STG 
	    WHERE	  DV.Sat_SmartCard.HubSmartCardSid=STG.HubSmartCardSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_SmartCard 
		( HubSmartCardSid,
CardExpiryDate,
CardPhysicalId,
CardPAN,
CardSaleLoadAgentId,
CardSubTypeDesc,
CardSubTypeId,
CardTypeDesc,
CardTypeId,
InitDate,
OrderRefLineNo,
OrderReferenceNo,
CardDDAInd,
CardPersonlisedInd,
DSCFareProductTypeId,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	 HubSmartCardSid,
CardExpiryDate,
CardPhysicalId,
CardPAN,
CardSaleLoadAgentId,
CardSubTypeDesc,
CardSubTypeId,
CardTypeDesc,
CardTypeId,
InitDate,
OrderRefLineNo,
OrderReferenceNo,
CardDDAInd,
CardPersonlisedInd,
DSCFareProductTypeId,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_SmartCard

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 