﻿

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_Route
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 * Abstract: Loads data from staging to raw data vault for SAT_ParentRoute
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */
IF OBJECT_ID('[STG].[usp_Load_Sat_Route]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Route]
 END;
 Go

CREATE PROC [STG].[usp_Load_Sat_Route] @RunGroupId [int] AS

Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_Route where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_Route
		SET DV.SAT_Route.LoadEndDate =GETDATE(),
			DV.SAT_Route.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_Route STG 
			 WHERE  DV.SAT_Route.HubRouteSid=STG.HubRouteSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_Route 
	( [HubRouteSid], 
    [DirectionCode], 
    [DescShort],
    [DirectionDesc], 
    [LineFrom],  
    [LineTo] , 
    [LineVia], 
    [LoadDate], 
    [LoadEndDate], 
    [IsCurrentFlag], 
    [RecordSource] 
		)
		select   
	[HubRouteSid], 
    [DirectionCode], 
    [DescShort],
    [DirectionDesc], 
    [LineFrom],  
    [LineTo] , 
    [LineVia], 
    getdate() [LoadDate] ,
    [LoadEndDate] ,
    [IsCurrentFlag], 
    [RecordSource] 
		from STG.RDV_SAT_Route
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
