﻿/******************************************************************************
 * Name : STG.usp_Load_Sat_Fareproductstatus
 *
 * Created :Feb 16th 2016
 * Author : Abhishek Kadam
 * Abstract: Loads data from staging to raw data vault for SAT_FAREPRODUCTSTATUS
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * *****************************************************************************
 */

IF OBJECT_ID('[STG].[usp_Load_Sat_FareProductStatus]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_FareProductStatus]
 END;
Go

  

CREATE PROC [STG].[usp_Load_Sat_FareProductStatus] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_FAREPRODUCTSTATUS where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_FAREPRODUCTSTATUS 
		SET DV.SAT_FAREPRODUCTSTATUS.LoadEndDate =GETDATE(),
			DV.SAT_FAREPRODUCTSTATUS.IsCurrentFlag='N',
			DV.SAT_FAREPRODUCTSTATUS.LoadDate=GETDATE()

		FROM  STG.RDV_SAT_FAREPRODUCTSTATUS STG 
			 WHERE  DV.SAT_FAREPRODUCTSTATUS.HubFareproductStatusSid=STG.HubFareproductStatusSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId

	/* Insert NEW records */	
		INSERT INTO DV.SAT_FAREPRODUCTSTATUS 
		([HubFareProductStatusSid],
		FareProductStatusDesc,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubFareProductStatusSid,
			FareProductStatusDesc,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_FAREPRODUCTSTATUS
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
