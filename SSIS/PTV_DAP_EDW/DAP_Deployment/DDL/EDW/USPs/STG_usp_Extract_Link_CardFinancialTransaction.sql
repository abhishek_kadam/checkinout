/*****************************
Author :  Vijender
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[usp_Extract_Link_CardFinancialTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Extract_Link_CardFinancialTransaction]

CREATE PROC [STG].[usp_Extract_Link_CardFinancialTransaction] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS
 
/* *****************************************************************************
 * Name : STG.usp_Extract_Link_CardFinancialTransaction
 *
 * Created :March 3rd 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Data Extract for Link_CardFinancialTransaction
 *			 
 *
 * Input : @RunGroupId Rungroupid from DAF.Rungroup
 *		   @LastExtractLoadedDateTime last extract for the rungroup
 *		   
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			01/03/2016	--		Initial version  
 *
 */
Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	---SET @RowsProcessed=
	---(select count(1)  
	---FROM [EXT].[MYKI_FACT_CARD_FIN_TXN] WHERE SETTLEMENT_DATE_KEY <>'SETTLEMENT_DATE_KEY' ) --a) /* checks sanity of external table as well */

	BEGIN TRANSACTION


		/* clear source staging */
		delete from  STG.[MYKI_Link_CardFinancialTransaction] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'

	
		/* insert data into source staging table */

		INSERT INTO STG.[MYKI_Link_CardFinancialTransaction]
		(	LinkCardFinancialTransactionSid,
			SETTLEMENT_DATE_KEY,
			TXN_TYPE_KEY,
			TERMINAL_KEY,
			TXN_DATE_KEY,
			TXN_TIME_KEY,
			CARD_TXN_SEQ_NO,
			PRODUCT_SERIAL_NO,
			CARD_PROD_SEQ_NO,
			SP_KEY,
			PAYMENT_TYPE_KEY,
			ENTRY_POINT_ID,
			GST_AMT,
			GST_RATE,
			TPURSE_AMT,
			REFUND_FEE_AMT,
			ZONE_NO,
			LINE_NO,
			ACTIONLIST_ID,
			BUS_STOP,
			DSC_CARD_KEY,
			LLSC_CARD_KEY,
			LLSC_TPURSE_LOAD_CSN,
			TXN_TIMESTAMP,
			RunGroupID,
			ProcessStatus,
			ExtractDateTime
			)

		SELECT 
			cast(HASHBYTES('SHA1',case when SETTLEMENT_DATE_KEY ='NULL' then '' else SETTLEMENT_DATE_KEY end+
								  case when TXN_TIME_KEY ='NULL' then '' else TXN_TIME_KEY end+
								  case when DSC_CARD_KEY ='NULL' then '' else DSC_CARD_KEY end+
								  case when LLSC_CARD_KEY ='NULL' then '' else LLSC_CARD_KEY end+
								  case when PRODUCT_SERIAL_NO ='NULL' then '' else PRODUCT_SERIAL_NO end+
								  case when TERMINAL_KEY ='NULL' then '' else TERMINAL_KEY end+
								  case when TXN_TYPE_KEY ='NULL' then '' else TXN_TYPE_KEY end+
								  case when CARD_PROD_SEQ_NO ='NULL' then '' else CARD_PROD_SEQ_NO end+
								  case when CARD_TXN_SEQ_NO ='NULL' then '' else CARD_TXN_SEQ_NO end

								  ) as binary(20)), /*udf dont work in these scenerios hence direct call to hashkeys function ..need correction later*/ 
		    case when SETTLEMENT_DATE_KEY ='NULL' then NULL else SETTLEMENT_DATE_KEY end SETTLEMENT_DATE_KEY,
			case when TXN_TYPE_KEY ='NULL' then NULL else TXN_TYPE_KEY end TXN_TYPE_KEY,
			case when TERMINAL_KEY ='NULL' then NULL else TERMINAL_KEY end TERMINAL_KEY,
			case when TXN_DATE_KEY ='NULL' then NULL else TXN_DATE_KEY end TXN_DATE_KEY,
			case when TXN_TIME_KEY ='NULL' then NULL else TXN_TIME_KEY end TXN_TIME_KEY,
			case when CARD_TXN_SEQ_NO ='NULL' then NULL else CARD_TXN_SEQ_NO end CARD_TXN_SEQ_NO,
			case when PRODUCT_SERIAL_NO ='NULL' then NULL else PRODUCT_SERIAL_NO end PRODUCT_SERIAL_NO,
			case when CARD_PROD_SEQ_NO ='NULL' then NULL else CARD_PROD_SEQ_NO end CARD_PROD_SEQ_NO,
			case when SP_KEY ='NULL' then NULL else SP_KEY end SP_KEY,
			case when PAYMENT_TYPE_KEY ='NULL' then NULL else PAYMENT_TYPE_KEY end PAYMENT_TYPE_KEY,
			case when ENTRY_POINT_ID ='NULL' then NULL else ENTRY_POINT_ID end ENTRY_POINT_ID,
			case when GST_AMT ='NULL' then NULL else GST_AMT end GST_AMT,
			case when GST_RATE ='NULL' then NULL else GST_RATE end GST_RATE,
			case when TPURSE_AMT ='NULL' then NULL else TPURSE_AMT end TPURSE_AMT,
			case when REFUND_FEE_AMT ='NULL' then NULL else REFUND_FEE_AMT end REFUND_FEE_AMT,
			case when ZONE_NO ='NULL' then NULL else ZONE_NO end ZONE_NO,
			case when LINE_NO ='NULL' then NULL else LINE_NO end LINE_NO,
			case when ACTIONLIST_ID ='NULL' then NULL else ACTIONLIST_ID end ACTIONLIST_ID,
			case when BUS_STOP ='NULL' then NULL else BUS_STOP end BUS_STOP,
			case when DSC_CARD_KEY ='NULL' then NULL else DSC_CARD_KEY end DSC_CARD_KEY,
			case when LLSC_CARD_KEY ='NULL' then NULL else LLSC_CARD_KEY end LLSC_CARD_KEY,
			case when LLSC_TPURSE_LOAD_CSN ='NULL' then NULL else LLSC_TPURSE_LOAD_CSN end LLSC_TPURSE_LOAD_CSN,
			case when TXN_TIMESTAMP ='NULL' then NULL else TXN_TIMESTAMP end TXN_TIMESTAMP,
			@RunGroupId,'EXTRACTED' [ProcessStatus] ,getdate() [ExtractDateTime]
			FROM [EXT].[MYKI_FACT_CARD_FIN_TXN] WHERE  SETTLEMENT_DATE_KEY <>'SETTLEMENT_DATE_KEY' 
		

	    set   @RowsProcessed=(select count(1) from STG.[MYKI_Link_CardFinancialTransaction] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED' )

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION

		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 









