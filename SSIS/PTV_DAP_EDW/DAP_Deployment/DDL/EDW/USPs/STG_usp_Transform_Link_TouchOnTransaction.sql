/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/08/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Link_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Transform_Link_TouchOnTransaction]

CREATE PROC [STG].[usp_Transform_Link_TouchOnTransaction] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 
/* *****************************************************************************
 * Name : STG.usp_Transform_Link_TouchOnTransaction
 *
 * Created :Mar 2nd 2016
 * Author : Naveen Subramaniam
 *
 * Abstract: Data Transform for Link_TouchOnTransaction
 *			 
 *
 * Input : STG.[MYKI_Link_TouchOnTransaction]
 *		   
 * 
 * Output :STG.[RDV_Link_TouchOnTransaction]
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			02/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1) 
	from 		(
				select *,ROW_NUMBER() over (Partition by LinkTouchOnTransactionSid Order by ExtractDateTime desc) RN
				from STG.[MYKI_Link_TouchOnTransaction] 
				where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1
				) 

		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Link_TouchOnTransaction]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO STG.RDV_Link_TouchOnTransaction
		(
			[LinkTouchOnTransactionSid],
			[TransactionDate],
			[TransactionTime],
			[HubStopSid],
			[HubSmartCardSid],
			[HubFareProductSid],
			[HubRouteSid],
			[HubTerminalSid],
			[LoadDate],
			[RecordSource],
			[ProcessStatus],
			[UpdateStatus],
			[RunGroupID]
		)

		SELECT
		
		SourceStaging.LinkTouchOnTransactionSid,
		HD.Date,
		CAST(REFTIME.TIMEFULL AS TIME(0))[TransactionTime],
		HS.HubStopSID,
		HSC.HubSmartCardSid, 
		HFP.HubFareProductSid, 
		HR.HubRouteSid,
		HTer.HubTerminalSid,
		GETDATE() [LoadDate],
		'EDW' [RecordSource],  -- Hardcoded the RecordSource since we have different recordSource value for LINK  & SAT
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.HubSmartCardSid is null then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		FROM 
		(
	
		select 
			Convert(Date, TXN_DATE_KEY) TXN_DATE_KEY, 
			TXN_TIME_KEY,
			TERMINAL_KEY, ROUTE_STOP_KEY, PRODUCT_SERIAL_NO,  STOP_LOCATION_KEY,  DSC_CARD_KEY, LLSC_CARD_KEY, FAREPRODUCT_TYPE_KEY, 						LinkTouchOnTransactionSid,  LLSC_CARD_SURFACE_ID, DSC_CARD_SURFACE_ID  	
		FROM
		(
		select *,ROW_NUMBER() over (Partition by LinkTouchOnTransactionSid Order by ExtractDateTime desc) RN
		from STG.[MYKI_Link_TouchOnTransaction] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1
		) SourceStaging 

		INNER join DV.Hub_Date HD 
				on HD.Date = SourceStaging.TXN_DATE_KEY 

		INNER JOIN STG.REF_DIM_TIME  REFTIME
						on REFTIME.TIME_KEY = SourceStaging.TXN_TIME_KEY 


		INNER join STG.REF_DIM_TERMINAL EXTTER
			on EXTTER.TERMINAL_KEY = SourceStaging.TERMINAL_KEY
			INNER join DV.Hub_Terminal HTer
				on HTer.TerminalID = EXTTER.TERMINAL_ID

		INNER JOIN  STG.REF_DIM_ROUTE_STOP EXTRS
			ON EXTRS.ROUTE_STOP_KEY = SourceStaging.ROUTE_STOP_KEY
			INNER join DV.Hub_Route HR
				on HR.RouteID = EXTRS.Route_ID

		INNER join STG.REF_DIM_STOP_LOCATION EXTSL
			on EXTSL.STOP_LOCATION_KEY = SourceStaging.STOP_LOCATION_KEY
			INNER JOIN DV.HUB_STOP HS
				ON HS.STOPID = EXTSL.STOP_LOCATION_ID
		
		
		INNER JOIN DV.HUB_SMARTCARD HSC
			On  HSC.CardSurfaceID =
				CASE WHEN DSC_CARD_KEY = 1 THEN
					SourceStaging.LLSC_CARD_SURFACE_ID
				Else
					SourceStaging.DSC_CARD_SURFACE_ID
				END
		
		LEFT OUTER JOIN DV.HUB_FareProduct HFP
			On  HFP.CardSurfaceID =
				CASE WHEN DSC_CARD_KEY = 1 THEN
					SourceStaging.LLSC_CARD_SURFACE_ID
				Else
					SourceStaging.DSC_CARD_SURFACE_ID
				END
			AND HFP.ProductSerialNumber = SourceStaging.PRODUCT_SERIAL_NO

		

		LEFT OUTER JOIN DV.[Link_TouchOnTransaction] DVTarget /*take target reference to identify if incoming rows are new or existing*/
				on DVTarget.[LinkTouchOnTransactionSid] = SourceStaging.LinkTouchOnTransactionSid


 IF OBJECT_ID('[ #tmpFailed_Link_TouchOnTransaction]', 'U') IS NOT NULL DROP TABLE [ #tmpFailed_Link_TouchOnTransaction]
		---create tmp table reference for failed records 
		create table #tmpFailed_Link_TouchOnTransaction
		WITH 
		  ( 
			CLUSTERED COLUMNSTORE INDEX,
			DISTRIBUTION = ROUND_ROBIN 
		  )
		AS
		select SourceStaging.LinkTouchOnTransactionSid,
		HD.Date TransactionDate  ,
		REFTIME.TIMEFULL TransactionTime ,
		HS.[HubStopSid] ,
		HS.StopID,
		HSC.[HubSmartCardSid] ,
		HSC.CardSurfaceId,
		--HFP.[HubFareProductSid] ,
		--HFP.ProductSerialNumber,
		HR.HubRouteSid ,
		HR.RouteId, 
		HTer.[HubTerminalSid],
		HTer.TerminalId,
		SourceStaging.LLSC_CARD_SURFACE_ID,
		SourceStaging.PRODUCT_SERIAL_NO,
		SourceStaging.STOP_LOCATION_KEY,
		SourceStaging.ROUTE_STOP_KEY,
		SourceStaging.TERMINAL_KEY
		From
		(
	
			select * FROM  --WHERE Rungroupid = @RunGroupId 
				(
				select *,ROW_NUMBER() over (Partition by LinkTouchOnTransactionSid Order by ExtractDateTime desc) RN
				from STG.[MYKI_Link_TouchOnTransaction] 
				where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1

		) SourceStaging 

			LEFT OUTER JOIN DV.Hub_Date HD 
					on HD.Date = SourceStaging.TXN_DATE_KEY 

			LEFT OUTER JOIN STG.REF_DIM_TIME  REFTIME
							on REFTIME.TIME_KEY = SourceStaging.TXN_TIME_KEY 


			LEFT OUTER JOIN STG.REF_DIM_TERMINAL EXTTER
				on EXTTER.TERMINAL_KEY = SourceStaging.TERMINAL_KEY
				LEFT OUTER JOIN DV.Hub_Terminal HTer
					on HTer.TerminalID = EXTTER.TERMINAL_ID

			LEFT OUTER JOIN STG.REF_DIM_ROUTE_STOP EXTRS
				ON EXTRS.ROUTE_STOP_KEY = SourceStaging.ROUTE_STOP_KEY
				LEFT OUTER JOIN  DV.Hub_Route HR
					on HR.RouteID = EXTRS.Route_ID

			LEFT OUTER JOIN STG.REF_DIM_STOP_LOCATION EXTSL
				on EXTSL.STOP_LOCATION_KEY = SourceStaging.STOP_LOCATION_KEY
				LEFT OUTER JOIN  DV.HUB_STOP HS
					ON HS.STOPID = EXTSL.STOP_LOCATION_ID
		
		
			LEFT OUTER JOIN DV.HUB_SMARTCARD HSC
				On  HSC.CardSurfaceID =
					CASE WHEN DSC_CARD_KEY = 1 THEN
						SourceStaging.LLSC_CARD_SURFACE_ID
					Else
						SourceStaging.DSC_CARD_SURFACE_ID
					END
		
		/*
			LEFT OUTER JOIN DV.HUB_FareProduct HFP
				On  HFP.CardSurfaceID =
					CASE WHEN DSC_CARD_KEY = 1 THEN
						SourceStaging.LLSC_CARD_SURFACE_ID
					Else
						SourceStaging.DSC_CARD_SURFACE_ID
					END
				AND HFP.ProductSerialNumber = SourceStaging.PRODUCT_SERIAL_NO
		*/
			WHERE HSC.CardSurfaceID IS NULL   OR HS.STOPID IS NULL OR HR.RouteID IS NULL 
				OR HTer.TerminalID IS NULL OR HD.Date IS NULL OR REFTIME.TIMEFULL IS NULL
		--OR HFP.CardSurfaceID IS NULL

	/* insert data into LOG.DATAQUALITYLOG for Logging*/	
		INSERT INTO LOG.DATAQUALITYLOG (
				[DataQualityRuleID],
				[RuleVersionNum],
				[ServicePackageRunID],
				[DataEntityRefCode],
				[SeverityLevelRefCode],
				[DataSourceRefCode],
				[QualityIssueRefCode],
				[AttributeName],
				[OriginalValue],
				[TransformedValue],
				[MessageText],
				[ReferenceID],
				[CreatedDateTime],
				[CreatedBy],
				[KeyAttributeValue1],
				[KeyAttributeValue2],
				[KeyAttributeValue3],
				[KeyAttributeValue4],
				[KeyAttributeValue5]
			)

		SELECT
			0 [DataQualityRuleID],
			1 [RuleVersionNum],
			@ServicePackageRunID [ServicePackageRunID],
			@DataEntityRefCode [DataEntityRefCode],
			'ERROR' [SeverityLevelRefCode],
			@RecordSource [DataSourceRefCode],
			'ISSUE' [QualityIssueRefCode],
			CASE 
				WHEN HubSmartCardSID IS NULL THEN 'HubSmartCardSID'  
				--WHEN HubFareProductSID IS NULL THEN 'HubFareProductSID'  
				WHEN HubStopSID IS NULL THEN 'HubStopSID' 
				WHEN HubRouteSid IS NULL THEN 'HubRouteSid' 
				ELSE 'HubTerminalSid' END [AttributeName],
			NULL [OriginalValue],
			NULL [TransformedValue],
			'Unable to find '+ 
			CASE 
				WHEN HubSmartCardSID IS NULL THEN 'HubSmartCardSID'  
				--WHEN HubFareProductSID IS NULL THEN 'HubFareProductSID'  
				WHEN HubStopSID IS NULL THEN 'HubStopSID' 
				WHEN HubRouteSid IS NULL THEN 'HubRouteSid' 
				ELSE 'HubTerminalSid' END  +' when transforming LINK_TouchOnTransaction from '+ @RecordSource [MessageText],
			'LinkTouchOnTransactionSid '   [ReferenceID],
			GETDATE() [CreatedDateTime],
			SYSTEM_USER [CreatedBy],
			LLSC_CARD_SURFACE_ID [KeyAttributeValue1],
			PRODUCT_SERIAL_NO [KeyAttributeValue2],
			STOP_LOCATION_KEY [KeyAttributeValue3],
			ROUTE_STOP_KEY [KeyAttributeValue4],
			TERMINAL_KEY [KeyAttributeValue5]
			
			FROM #tmpFailed_Link_TouchOnTransaction


	


		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Update   STG.[MYKI_Link_TouchOnTransaction] set ProcessStatus='EXTRACTED' WHERE ProcessStatus='EXTRACTPENDING'


		update STG.[MYKI_Link_TouchOnTransaction]
		set STG.[MYKI_Link_TouchOnTransaction].ProcessStatus='EXTRACTPENDING'
		FROM #tmpFailed_Link_TouchOnTransaction FAILED_RECORDS 
		WHERE STG.[MYKI_Link_TouchOnTransaction].LinkTouchOnTransactionSid= 
		      FAILED_RECORDS.LinkTouchOnTransactionSid



		Set @RowsFailed=(SELECT COUNT(1) FROM  #tmpFailed_Link_TouchOnTransaction )
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)


	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

	truncate table #tmpFailed_Link_TouchOnTransaction
	drop table #tmpFailed_Link_TouchOnTransaction



 END CATCH	
truncate table #tmpFailed_Link_TouchOnTransaction
drop table #tmpFailed_Link_TouchOnTransaction
End 


