/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Extract_Link_SurveyDetail]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Extract_Link_SurveyDetail]

CREATE PROC [STG].[usp_Extract_Link_SurveyDetail] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS



Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1) from 
	(SELECT distinct LinkSurveyDetailSid
	FROM [EXT].[vw_TORS_LinkSurveyDetail]) a) /* checks sanity of external table as well */

	BEGIN TRANSACTION


		/* clear source staging */
		delete from  [STG].[TORS_Link_SurveyDetail] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'

	
		/* insert data into source staging table */

		INSERT INTO [STG].[TORS_Link_SurveyDetail]
			(   [LinkSurveyDetailSid] , 
				[RawBatchID] , 
				[CardSurfaceId] , 
				[StopId] , 
				[VehicleId] , 
				[SurveyDate], 
				[SurveyorName] , 
				[RouteNumber] , 
				[CountStartTime] , 
				[CountEndTime] , 
				[Time], 
				[CountOn] , 
				[CountOff], 
				[AlreadyOn] , 
				[RemainedOn], 
				[Sequence] , 
				[Shift] , 
				[JobNumber], 
				[Door], 
				[Comment] , 
				ServiceProvider,
				[RunGroupID] , 
				[ProcessStatus] , 
				[ExtractDateTime])
		SELECT  LinkSurveyDetailSid ,
		        RawbatchId , 
				SurveyorMykiNo1 ,
				MetlinkStopID ,
				Vehicle_BusRego_TramNo , 
				 Cast((RIGHT(Surveydate,4) + '' + SUBSTRING(Surveydate,4,2) + '' + LEFT(Surveydate,2)) as date) as Surveydate ,
				SurveyorName1,
				[Route],
				cast(ActualSurveyStartTime as time(7)) ActualSurveyStartTime,
				cast(ActualSurveyFinishTime as time(7)) ActualSurveyFinishTime,
				cast([Time] as time(7)) [Time],
				CountOn,
				CountOff,
				AlreadyOn,
				RemainingOn,
				[Sequence],
				ShiftID,
				JobNumber,
				Door,
				Comment,
				ServiceProvider,
				@RunGroupId,
				'EXTRACTED' [ProcessStatus] ,
				 getdate() [ExtractDateTime]

		FROM [EXT].[vw_TORS_LinkSurveyDetail] where Surveydate<>'Surveydate' --and CAST(REC_UPDT_DT AS DATETIME) > @LastExtractLoadedDateTime
		

		/*Purge extract Pending*/
		---Delete from STG.Extractpending where rungroupid =@RunGroupId--NOT REQUIRED 

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION

		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 