/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Sat_SurveyDetail]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Transform_Sat_SurveyDetail]

CREATE PROC [STG].[usp_Transform_Sat_SurveyDetail] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	
	from [STG].[TORS_Link_SurveyDetail]
	where Rungroupid =@Rungroupid) 

	BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Sat_SurveyDetail]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO [STG].[RDV_Sat_SurveyDetail]
		([LinkSurveyDetailSid] , 
    [SurveyDate] , 
    [SurveyorName] , 
    [RouteNumber] , 
    [CountStartTime] , 
    [CountEndTime] , 
    [Time] , 
    [CountOn] , 
    [CountOff] , 
    [AlreadyOn] , 
    [RemainedOn] , 
    [Sequence] , 
    [Shift] , 
    [JobNumber], 
    [Door] , 
    [Comment] , 
    [ServiceProvider] , 
    [LoadDate] , 
     [RecordSource] , 
    [ProcessStatus] , 
    [UpdateStatus] ,
	[RungroupID]
		)
		
SELECT
		SourceStaging.LinkSurveyDetailSid, 		
		SourceStaging.[SurveyDate],
		SourceStaging.[SurveyorName], 
		SourceStaging.[RouteNumber],
		SourceStaging.[CountStartTime],
		SourceStaging.[CountEndTime],
		SourceStaging.[Time],
		SourceStaging.[CountOn],
	    SourceStaging.[CountOff],
		SourceStaging.[AlreadyOn],
		SourceStaging.[RemainedOn],
		SourceStaging.[Sequence],
		SourceStaging.[Shift],
		SourceStaging.[JobNumber],
		SourceStaging.[Door],
		SourceStaging.[Comment],
		SourceStaging.[ServiceProvider],
		

		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.LinkSurveyDetailSid is null then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		FROM 
		(
		select [LinkSurveyDetailSid] , 
    [SurveyDate] , 
    [SurveyorName] , 
    [RouteNumber] , 
    Cast( [CountStartTime] as Time(7) ) [CountStartTime], 
    Cast( [CountEndTime] as Time(7) ) [CountEndTime] , 
    Cast( [Time] as Time(7) ) [Time] , 
    CountOn,
    CountOff,
    [AlreadyOn] , 
    [RemainedOn] , 
    [Sequence] , 
    [Shift] , 
    [JobNumber], 
    [Door] , 
    [Comment] , 
    [ServiceProvider] 
		FROM STG.[TORS_LINK_SurveyDetail] 
		WHERE Rungroupid = @RunGroupId AND PROCESSSTATUS = 'EXTRACTED'
		) SourceStaging 

		
			

		LEFT OUTER JOIN DV.[SAT_SurveyDetail] DVTarget /*take target reference to identify if incoming rows are new or existing*/
				on DVTarget.LinkSurveyDetailSid = SourceStaging.LinkSurveyDetailSid 
				AND DVTarget.Surveydate =SourceStaging.Surveydate
						AND DVTarget.SurveyorName =SourceStaging.SurveyorName
						AND DVTarget.[RouteNumber] =SourceStaging.[RouteNumber]
						AND DVTarget.CountOn =SourceStaging.CountOn
						AND DVTarget.CountOff =SourceStaging.CountOff
						--AND DVTarget.[Time] =SourceStaging.[Time]
						--AND DVTarget.CountStartTime =SourceStaging.[CountOn]
						--AND DVTarget.CountEndTime =SourceStaging.[CountOff]
						AND DVTarget.[AlreadyOn] =SourceStaging.[AlreadyOn]
						AND DVTarget.[RemainedOn] =SourceStaging.[RemainedOn]
						AND DVTarget.[Shift] =SourceStaging.[Shift]
						AND DVTarget.JobNumber =SourceStaging.JobNumber
						AND DVTarget.Comment =SourceStaging.Comment
						AND DVTarget.ServiceProvider =SourceStaging.ServiceProvider
						
						



		/*Extract pending not valid here as source data will always be available in full through Data Lake*/
		/*
			UPDATE 	  [STG].[MYKI_LINK_TouchOnTransaction]
			SET PROCESSSTATUS ='EXTRACTPENDING'
			WHERE LinkTouchOnTransactionSID NOT in ( Select DISTINCT LinkTouchOnTransactionSID  FROM  [STG].[RDV_LINK_TouchOnTransaction] WHERE RunGroupid=@RunGroupId )
				 AND RunGroupid=@RunGroupId
		*/



		--Set @RowsFailed=(SELECT COUNT(1) FROM LOG.DATAQUALITYLOG WHERE [ServicePackageRunID]=@ServicePackageRunID)
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 