﻿

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_StopMode
 *
 * Created :Feb 16th 2016
 * Author : Ramakrishna
 *
 * Abstract: Loads data from staging to raw data vault for SAT_StopMode
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
  */

  IF OBJECT_ID('[STG].[usp_Load_Sat_StopMode]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_StopMode]
 END;
 Go

CREATE PROC [STG].[usp_Load_Sat_StopMode] @RunGroupId [int] AS
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_StopMode where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_StopMode 
		SET DV.SAT_StopMode.LoadEndDate =GETDATE(),
			DV.SAT_StopMode.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_StopMode STG 
			 WHERE  DV.SAT_StopMode.HubModeSid=STG.HubModeSid
		AND   STG.Updatestatus ='UPDATED' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_StopMode
		(HubModeSid,
		StopModeName,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubModeSid,
			StopModeName,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_STOPMODE
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
