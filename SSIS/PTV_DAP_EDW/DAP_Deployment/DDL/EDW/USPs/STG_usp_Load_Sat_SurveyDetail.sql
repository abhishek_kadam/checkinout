/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_Sat_SurveyDetail]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Sat_SurveyDetail]


CREATE PROC [STG].[usp_Load_Sat_SurveyDetail] @RunGroupId [int] AS
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1)
	from STG.[RDV_SAT_SurveyDetail] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' ) /*EXISTING are not processed */
	--and updatestatus='NEW'

	UPDATE 	  DV.Sat_SurveyDetail SET DV.Sat_SurveyDetail.LoadEndDate =GETDATE()
    FROM  STG.RDV_Sat_SurveyDetail STG 
    WHERE  DV.Sat_SurveyDetail.LinkSurveyDetailSid=STG.LinkSurveyDetailSid
		AND   STG.Updatestatus ='NEW' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


			INSERT INTO DV.[SAT_SurveyDetail]
			(
				[LinkSurveyDetailSid],
				[surveyDate], 
				[SurveyorName], 
				[RouteNumber], 
				[CountStarttime], 
				[CountEndTime], 
				[Time], 
				[CountOn], 
				[CountOff], 
				[AlreadyOn], 
				[RemainedOn], 
				[Sequence],
				[Shift],
				[JobNumber],
				[Door],
				[Comment],
				[ServiceProvider],
				[LoadDate], 
				[LoadEndDate], 
				--[IsCurrentFlag], 
				[RecordSource]	 
			)
			SELECT
				STAGE.[LinkSurveyDetailSid],
				STAGE.[surveyDate], 
				STAGE.[SurveyorName], 
				STAGE.[RouteNumber], 
				STAGE.[CountStarttime], 
				STAGE.[CountEndTime], 
				STAGE.[Time], 
				STAGE.[CountOn], 
				STAGE.[CountOff], 
				STAGE.[AlreadyOn], 
				STAGE.[RemainedOn], 
				STAGE.[Sequence], 
				STAGE.[Shift], 
				STAGE.[JobNumber], 
				STAGE.[Door], 
				STAGE.[Comment],
				STAGE.[ServiceProvider],
				STAGE.[LoadDate], 
				NULL, 
				--NULL, 
				STAGE.[RecordSource]
					 
			FROM STG.RDV_SAT_SurveyDetail STAGE 
			--LEFT OUTER JOIN  DV.SAT_SurveyDetail DVTarget
			--	ON DVTarget.LinkSurveyDetailSid = STAGE.LinkSurveyDetailSid and 
			--	DVTarget.LinkSurveyDetailSid is NULL

			WHERE 
				 STAGE.UpdateStatus = 'NEW' AND STAGE.ProcessStatus = 'TRANSFORMED'
				AND RunGroupid=@RunGroupId 

	
		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_SAT_SurveyDetail]
		set ProcessStatus='LOADED'
		WHERE  ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId
		-- Updatestatus ='NEW' and
				

		/* Quality Data Update  */

		Set @RowsFailed=0

		
		SELECT @RowsProcessed , @RowsFailed 
		

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 
