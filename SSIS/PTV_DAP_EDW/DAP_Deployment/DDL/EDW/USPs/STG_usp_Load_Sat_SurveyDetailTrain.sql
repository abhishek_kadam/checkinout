/* *****************************************************************************
 * Name : STG.usp_Load_Sat_SurveyDetailTrain
 *
 * Created :March 8th 2016
 * Author :Kiran Kuamr
 *
 * Abstract: Data Load for Sat_SurveyDetailTrain
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************

 */


IF OBJECT_ID('[STG].[usp_Load_Sat_SurveyDetailTrain]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Sat_SurveyDetailTrain]

 
CREATE PROC [STG].[usp_Load_Sat_SurveyDetailTrain] @RunGroupId [int] AS
 

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1)
	from STG.[RDV_Sat_SurveyDetailTrain] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' ) /*EXISTING are not processed */
	--and updatestatus='NEW'

	UPDATE 	  DV.Sat_SurveyDetailTrain SET DV.Sat_SurveyDetailTrain.LoadEndDate =GETDATE()
    FROM  STG.RDV_Sat_SurveyDetailTrain STG 
    WHERE  DV.Sat_SurveyDetailTrain.LinkSurveyDetailTrainSid=STG.LinkSurveyDetailTrainSid
		AND   STG.Updatestatus ='NEW' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
			INSERT INTO DV.[Sat_SurveyDetailTrain]
			(
				[LinkSurveyDetailTrainSid] , 
				[SurveyDate] , 
				[SurveyorName] ,
				[CountStartTime], 
				[CountEndTime] , 
				[Mode] , 
				[CountOn], 
				[CountOff] ,
				[Shift], 
				[JobNumber] , 
				[Comment] , 
				[ServiceProvider] , 
				[SecondSurveyorName] , 
				[SecondSurveyorMyki] , 
				[LoadDate] , 
				[LoadEndDate] ,
				[RecordSource]
			)
			SELECT
				STAGE.[LinkSurveyDetailTrainSid],
				STAGE.[SurveyDate], 
				STAGE.[SurveyorName], 
				STAGE.[CountStartTime], 
				STAGE.[CountEndTime], 
				STAGE.[Mode], 
				STAGE.[CountOn], 
				STAGE.[CountOff], 
				STAGE.[Shift], 
				STAGE.[JobNumber], 
				STAGE.[Comment], 
				STAGE.[ServiceProvider], 
				STAGE.[SecondSurveyorName], 
				STAGE.[SecondSurveyorMyki], 
				STAGE.[LoadDate], 
				NULL , 
				STAGE.[RecordSource]
					 
			FROM STG.RDV_sat_SurveyDetailTrain STAGE 
			LEFT OUTER JOIN  DV.sat_SurveyDetailTrain DVTarget
				ON DVTarget.LinkSurveyDetailTrainSid = STAGE.LinkSurveyDetailTrainSid 
				 AND DVTarget.LinkSurveyDetailTrainSid is NULL    
				
			WHERE STAGE.UpdateStatus = 'NEW' AND STAGE.ProcessStatus = 'TRANSFORMED'
				AND RunGroupid=@RunGroupId 

	
		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Sat_SurveyDetailTrain]
		set ProcessStatus='LOADED'
		WHERE  ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId
		-- Updatestatus ='NEW' and
				

		/* Quality Data Update  */

		Set @RowsFailed=0

		
		SELECT @RowsProcessed , @RowsFailed 
		

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








