/* *****************************************************************************
 * Name : [STG].[usp_Extract_Link_SurveyDetailTrain]
 *
 * Created :Mar 1st 2016
 * Author : Kiran Kmar
 *
 * Abstract: Data Extract for Link_SurveyDetailTrain
 *			 
 *
 * Input : @RunGroupId Rungroupid from DAF.Rungroup
 *		   @LastExtractLoadedDateTime last extract for the rungroup
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 */


IF OBJECT_ID('[STG].[usp_Extract_Link_SurveyDetailTrain]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Extract_Link_SurveyDetailTrain]


  

CREATE  PROC [STG].[usp_Extract_Link_SurveyDetailTrain] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS
 

Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	
	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1) FROM [EXT].[vw_TORS_LinkSurveyDetailTrain])  /* checks sanity of external table as well */ 




		/* clear source staging */
		delete from  [STG].[TORS_Link_SurveyDetailTrain] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'
	BEGIN TRANSACTION
	
		/* insert data into source staging table */

		INSERT INTO [STG].[TORS_Link_SurveyDetailTrain]

		([Link_SurveyDetailTrainSid],
		 [RawbatchId] , 
		 [EntranceID] ,
		 [SurveyorMykiNo1],
		 [RunGroupID] ,
		 [ProcessStatus],
		 [ExtractDateTime],
		 [SurveyDate] , --These columns used for Link_Sat 
		 [SurveyorName1] , 
		 [ActualSurveyStartTime] , 
		 [ActualSurveyFinishTime] , 
		 [CountOn] , 
		 [CountOff], 
		 [ShiftID], 
		 [JobNumber] , 
		 [Comment] , 
		 [Mode],
		 [ServiceProvider], 
		 [SurveyorName2], 
		 [SurveyorMykiNo2])
		SELECT  
		  [LinkSurveyDetailTrainSid] ,
		  [RawBatchID] ,
		  [EntranceID],
		  [SurveyorMykiNo1],
		  @RunGroupId,
		  'EXTRACTED' [ProcessStatus] ,
		  getdate() [ExtractDateTime],
		  [SurveyDate] , --These columns used for Link_Sat 
		 [SurveyorName1] , 
		 [ActualSurveyStartTime] , 
		 [ActualSurveyFinishTime] , 
		 [CountOn] , 
		 [CountOff], 
		 [ShiftID], 
		 [JobNumber] , 
		 [Comment] , 
		 [Mode],
		 [ServiceProvider], 
		 [SurveyorName2], 
		 [SurveyorMykiNo2]
		FROM [EXT].[vw_TORS_LinkSurveyDetailTrain]  --and CAST(REC_UPDT_DT AS DATETIME) > @LastExtractLoadedDateTime
		

		/*Purge extract Pending*/
		---Delete from STG.Extractpending where rungroupid =@RunGroupId--NOT REQUIRED 

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION

		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








