/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/06/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Sat_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_Transform_Sat_TouchOnTransaction]


CREATE PROC [STG].[usp_Transform_Sat_TouchOnTransaction] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 
/* *****************************************************************************
 * Name : STG.usp_Transform_Link_TouchOnTransaction
 *
 * Created :Mar 6nd 2016
 * Author : Naveen Subramaniam
 *
 * Abstract: Data Transform for Link_TouchOnTransaction
 *			 
 *
 * Input : STG.[MYKI_SAT_TouchOnTransaction]
 *		   
 * 
 * Output :STG.[RDV_Sat_TouchOnTransaction]
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			06/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1) 
	from STG.[MYKI_LINK_TouchOnTransaction] 
	where Rungroupid =@Rungroupid AND PROCESSSTATUS = 'EXTRACTED') 


	BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_SAT_TouchOnTransaction]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO STG.RDV_Sat_TouchOnTransaction
		(
			[LinkTouchOnTransactionSid], 
			[RouteStopSequenceNum], 
			[EntryLocationId], 
			[ProcessingDate], 
			[ProductSerialNumber], 
			[ServiceProviderId], 
			[StopLocationId], 
			[ZoneMax], 
			[ZoneMin], 
			[RouteStopId], 
			[TicketingBusinessDate], 
			[LoadDate], 
			[RecordSource],
			[ProcessStatus],
			[UpdateStatus],
			[RunGroupID]
		)

		
SELECT
		SourceStaging.LinkTouchOnTransactionSid, 		
		SourceStaging.BUS_STOP [RouteStopSequenceNo],
		SourceStaging.ENTRY_LOC_ID [EntryLocationID], 
		SourceStaging.PROCESSING_DATE [ProcessingDate],
		SourceStaging.PRODUCT_SERIAL_NO [ProductSerialNumber],
		SourceStaging.SP_ID [ServiceProviderID],
		HS.StopID [StopLocationID],
		SourceStaging.ZONE_MAX [ZoneMax],
		SourceStaging.ZONE_MIN [ZoneMin],
		HR.Routeid [RouteStopID], 
		SourceStaging.BUSINESS_DATE [TicketingBusinessDate],
		GETDATE() [LoadDate],
		'mykiNTSMirrorDataWarehouse' [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when DVTarget.LinkTouchOnTransactionSid is null then 'NEW' else 'UPDATE' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		FROM 
		(
		select 
			LinkTouchOnTransactionSid, TERMINAL_KEY, ROUTE_STOP_KEY, PRODUCT_SERIAL_NO,  STOP_LOCATION_KEY,  DSC_CARD_KEY, LLSC_CARD_KEY,		
			LLSC_CARD_SURFACE_ID, DSC_CARD_SURFACE_ID,  CAST( ENTRY_LOC_ID as INT) ENTRY_LOC_ID, 
			CAST( BUS_STOP as INT) BUS_STOP,  CAST( BUSINESS_DATE as DATE) BUSINESS_DATE, CAST( PROCESSING_DATE as DateTime2) as PROCESSING_DATE, 
			CAST( ZONE_MAX as INT) ZONE_MAX, CAST( ZONE_MIN as INT) ZONE_MIN, CAST( SP_ID as INT) SP_ID 
		FROM STG.[MYKI_LINK_TouchOnTransaction] 
		WHERE Rungroupid = @RunGroupId AND PROCESSSTATUS = 'EXTRACTED'
		) SourceStaging 

		INNER JOIN  STG.REF_DIM_ROUTE_STOP EXTRS
			ON EXTRS.ROUTE_STOP_KEY = SourceStaging.ROUTE_STOP_KEY
			INNER join DV.Hub_Route HR
				on HR.RouteID = EXTRS.Route_ID

		INNER join STG.REF_DIM_STOP_LOCATION EXTSL
			on EXTSL.STOP_LOCATION_KEY = SourceStaging.STOP_LOCATION_KEY
			INNER JOIN DV.HUB_STOP HS
				ON HS.STOPID = EXTSL.STOP_LOCATION_ID

		LEFT OUTER JOIN DV.[SAT_TouchOnTransaction] DVTarget /*take target reference to identify if incoming rows are new or existing*/
				on DVTarget.LinkTouchOnTransactionSid = SourceStaging.LinkTouchOnTransactionSid 

		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 


