﻿IF OBJECT_ID('[STG].[usp_Load_Sat_FareProductType]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_FareProductType]
 END;
 Go


CREATE PROC [STG].[usp_Load_Sat_FareProductType] @RunGroupId [int] AS



/* *****************************************************************************
 * Name : STG.usp_Load_Sat_FareProductType
 *
 * Created :Feb 16th 2016
 * Author : Charan Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_FareProductType
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Charan Kumar   	1.0			16/02/2016	--		Initial version 
 *
 *
  */
Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_FareProductType where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_FareProductType 
		SET		  DV.Sat_FareProductType.LoadEndDate =GETDATE(),
			      DV.Sat_FareProductType.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_FareProductType STG 
	    WHERE	  DV.Sat_FareProductType.HubFareProductSid=STG.HubFareProductSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_FareProductType 
		(HubFareProductSid,
		FareproductEffectivityDate,
		FareProductLongDesccription,
		FareProductTypeDescription,
		ProductSubType,
		ProductType,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	HubFareProductSid,
				FareproductEffectivityDate,
				FareProductLongDesccription,
				FareProductTypeDescription,
				ProductSubType,
				ProductType,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_FareProductType

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 
