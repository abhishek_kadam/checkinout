﻿CREATE PROC [STG].[usp_Load_Sat_Servicelocation] @RunGroupId [int] AS

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_SERVICELOCATION
 *
 * Created :Feb 17th 2016
 * Author : ASHA U S
 *
 * Abstract: Loads data from staging to raw data vault for SAT_SERVICELOCATION
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * ASHA U S			1.0			17/02/2016	--		Initial version ( Need to add logica to handle processstatus LOADFAILED )
 *
 *
  */
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_SERVICELOCATION where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_SERVICELOCATION 
		SET DV.SAT_SERVICELOCATION.LoadEndDate =GETDATE(),
			DV.SAT_SERVICELOCATION.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_SERVICELOCATION STG 
			 WHERE  DV.SAT_SERVICELOCATION.HubServiceLocationSid=STG.HubServiceLocationSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_SERVICELOCATION 
		(HubServiceLocationSid,
		CoverageName,
		InventLocationId,
		LocationTypeDesc,
		LocationTypeId,
		NtsZoneId,
		ServiceLocationDescription,
		ServiceProviderId,
		ServiceLocName,
		ServiceLocShortname,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubServiceLocationSid,
		CoverageName,
		InventLocationId,
		LocationTypeDesc,
		LocationTypeId,
		NtsZoneId,
		ServiceLocationDescription,
		ServiceProviderId,
		ServiceLocName,
		ServiceLocShortname,
	    getdate() LoadDate,
	    LoadEndDate,
		IsCurrentFlag,
		RecordSource
		from STG.RDV_SAT_SERVICELOCATION
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
