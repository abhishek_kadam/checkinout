/*****************************
Author :  Naveen Subramaniam
Modified Date : 03/07/2016
****************************/

IF OBJECT_ID('[STG].[usp_Load_SAT_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [[STG].[usp_Load_SAT_TouchOnTransaction]

CREATE PROC [STG].[usp_Load_SAT_TouchOnTransaction] @RunGroupId [int] AS
 
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_TouchOnTransaction
 *
 * Created :March 7th 2016
 * Author :Naveen Subramaniam
 *
 * Abstract: Data Load for SAT_TouchOnTransaction
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Naveen Subramaniam	1.0			08/03/2016	--		Initial version  
 *
 */
Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1)
	from STG.[RDV_SAT_TouchOnTransaction] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' ) /*EXISTING are not processed */



		UPDATE 	 DV.SAT_TouchOnTransaction 	
		SET 
				DV.SAT_TouchOnTransaction.[RouteStopSequenceNum]=STAGE.[RouteStopSequenceNum], 
				DV.SAT_TouchOnTransaction.[EntryLocationId]=STAGE.[EntryLocationId], 
				DV.SAT_TouchOnTransaction.[ProcessingDate]=STAGE.[ProcessingDate], 
				DV.SAT_TouchOnTransaction.[ProductSerialNumber]=STAGE.[ProductSerialNumber], 
				DV.SAT_TouchOnTransaction.[ServiceProviderId]=STAGE.[ServiceProviderId], 
				DV.SAT_TouchOnTransaction.[StopLocationId]=STAGE.[StopLocationId], 
				DV.SAT_TouchOnTransaction.[ZoneMax]=STAGE.[ZoneMax], 
				DV.SAT_TouchOnTransaction.[ZoneMin]=STAGE.[ZoneMin], 
				DV.SAT_TouchOnTransaction.[RouteStopId]=STAGE.[RouteStopId], 
				DV.SAT_TouchOnTransaction.[TicketingBusinessDate]=STAGE.[TicketingBusinessDate], 
				DV.SAT_TouchOnTransaction.[LoadDate]=STAGE.[LoadDate]

		FROM  STG.RDV_SAT_TouchOnTransaction STAGE 
			 WHERE DV.SAT_TouchOnTransaction.LinkTouchOnTransactionSid=STAGE.LinkTouchOnTransactionSid 
		AND STAGE.ProcessStatus='TRANSFORMED' AND STAGE.UPDATEStatus='UPDATE' AND STAGE.RunGroupid=@RunGroupId

		
			INSERT INTO DV.[SAT_TouchOnTransaction]
			(
				[LinkTouchOnTransactionSid],
				[RouteStopSequenceNum], 
				[EntryLocationId], 
				[ProcessingDate], 
				[ProductSerialNumber], 
				[ServiceProviderId], 
				[StopLocationId], 
				[ZoneMax], 
				[ZoneMin], 
				[RouteStopId], 
				[TicketingBusinessDate], 
				[LoadDate], 
				[LoadEndDate], 
				[IsCurrentFlag], 
				[RecordSource]	 
			)
			SELECT
				STAGE.[LinkTouchOnTransactionSid],
				STAGE.[RouteStopSequenceNum], 
				STAGE.[EntryLocationId], 
				STAGE.[ProcessingDate], 
				STAGE.[ProductSerialNumber], 
				STAGE.[ServiceProviderId], 
				STAGE.[StopLocationId], 
				STAGE.[ZoneMax], 
				STAGE.[ZoneMin], 
				STAGE.[RouteStopId], 
				STAGE.[TicketingBusinessDate], 
				STAGE.[LoadDate], 
				NULL, 
				NULL, 
				STAGE.[RecordSource]
					 
			FROM STG.RDV_SAT_TouchOnTransaction STAGE 
			--LEFT OUTER JOIN  DV.SAT_TouchOnTransaction DVTarget
			--	ON DVTarget.LinkTouchOnTransactionSid = STAGE.LinkTouchOnTransactionSid 

			WHERE 
				--DVTarget.LinkTouchOnTransactionSid is NULL  AND 
			STAGE.UpdateStatus = 'NEW' AND STAGE.ProcessStatus = 'TRANSFORMED'
				AND RunGroupid=@RunGroupId 

	
		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_SAT_TouchOnTransaction]
		set ProcessStatus='LOADED'
		WHERE  ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

				

		/* Quality Data Update  */

		Set @RowsFailed=0

		
		SELECT @RowsProcessed , @RowsFailed 
		

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








