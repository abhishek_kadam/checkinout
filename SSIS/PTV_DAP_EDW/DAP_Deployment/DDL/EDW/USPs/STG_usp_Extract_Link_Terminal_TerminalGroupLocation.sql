﻿

IF OBJECT_ID('[STG].[usp_Extract_Link_Terminal_TerminalGroupLocation]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Extract_Link_Terminal_TerminalGroupLocation]
 END;
 Go

  

CREATE PROC [STG].[usp_Extract_Link_Terminal_TerminalGroupLocation] @RunGroupId [int],@LastExtractLoadedDateTime [datetime] AS
 
/* *****************************************************************************
 * Name : STG.usp_Extract_Link_Terminal_TerminalGroupLocation
 *
 * Created :Feb 18th 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Data Extract for Link_Terminal_TerminalGroupLocation
 *			 
 *
 * Input : 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			18/02/2016	--		Initial version  
 *
 */
Begin  

declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=
	(select count(1) from 
	(SELECT distinct TERMINAL_ID,TERMINAL_GROUP_LOC_ID
	FROM EXT.MYKI_DIM_TERMINAL WHERE TERMINAL_KEY <>'TERMINAL_KEY' ) a) /* checks sanity of external table as well */

	BEGIN TRANSACTION


		/* clear source staging */
		delete from  STG.[MYKI_Link_Terminal_TerminalGroupLocation] where rungroupid =@RunGroupId and [ProcessStatus] ='EXTRACTED'

	
		/* insert data into source staging table */

		INSERT INTO STG.[MYKI_Link_Terminal_TerminalGroupLocation]
		([TERMINAL_ID] , 
		 [TERMINAL_GROUP_LOC_ID] ,
		 [RunGroupID] ,
		 [ProcessStatus],
		 [ExtractDateTime])
		SELECT distinct case when TERMINAL_ID ='NULL' then NULL else TERMINAL_ID end TERMINAL_ID,
		case when TERMINAL_GROUP_LOC_ID ='NULL'  then null else TERMINAL_GROUP_LOC_ID end  TERMINAL_GROUP_LOC_ID,
		@RunGroupId,'EXTRACTED' [ProcessStatus] ,getdate() [ExtractDateTime]
		FROM EXT.MYKI_DIM_TERMINAL WHERE TERMINAL_KEY <>'TERMINAL_KEY' --and CAST(REC_UPDT_DT AS DATETIME) > @LastExtractLoadedDateTime
		

		/*Purge extract Pending*/
		---Delete from STG.Extractpending where rungroupid =@RunGroupId--NOT REQUIRED 

		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION

		SELECT isnull(@RowsProcessed,0) RowsProcessed, @RowsFailed  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








