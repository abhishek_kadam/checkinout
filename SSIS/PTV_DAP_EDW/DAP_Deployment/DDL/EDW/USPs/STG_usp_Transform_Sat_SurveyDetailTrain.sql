/* *****************************************************************************
 * Name : [STG].[usp_Transform_Sat_Link_SurveyDetailTrain]
 *
 * Created :Mar 1st 2016
 * Author : KIRAN KUMAR
 *
 * Abstract: Data Transform for Link_SurveyDetailTrain
 *			 
 *
 * Input : STG.[TORS_LINK_SurveyDetailTrain]
 *		   
 * 
 * Output :STG.[RDV_Sat_SurveyDetailTrain]
 *
 * 
 * ****************************************************************************
 */

 IF OBJECT_ID('[STG].[usp_Transform_Sat_SurveyDetailTrain]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Transform_Sat_SurveyDetailTrain]

 
CREATE PROC [STG].[usp_Transform_Sat_SurveyDetailTrain] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS
 

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	/*(select count(1)
	
	from [STG].[TORS_Link_SurveyDetailTrain]
	where Rungroupid =@Rungroupid) */

	(select count(1)
	
	from (
		select	*,ROW_NUMBER() over (Partition by Link_SurveyDetailTrainSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetailTrain] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1 )  

	BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		 from   [STG].[RDV_Sat_SurveyDetailTrain]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO [STG].[RDV_Sat_SurveyDetailTrain]
		(	[LinkSurveyDetailTrainSid] , 
			[SurveyDate] , 
			[SurveyorName] ,  
			[CountStartTime] , 
			[CountEndTime] ,
			[Mode] , 
			[CountOn] , 
			[CountOff] , 
			[Shift] , 
			[JobNumber], 
			[Comment],  
			[ServiceProvider] , 
			[SecondSurveyorName] , 
			[SecondSurveyorMyki], 
			[LoadDate] , 
			[RecordSource] , 
			--[LoadEndDate], 
			[RunGroupID] , 
			[ProcessStatus] , 
			[UpdateStatus] 
		)
		select 
		SourceStaging.Link_SurveyDetailTrainSid,
		 SourceStaging.Surveydate as Surveydate,
		SourceStaging.SurveyorName1 as SurveyorName1,
		SourceStaging.ActualSurveyStartTime as  CountStartTime,
		SourceStaging.ActualSurveyFinishTime as   CountEndTime,
		SourceStaging.Mode  as Mode,
		SourceStaging.CountOn as CountOn,
		SourceStaging.CountOff as CountOff,
		SourceStaging.ShiftID  as [Shift] ,
		SourceStaging.JobNumber as JobNumber,
		SourceStaging.Comment  as Comment,
		SourceStaging.ServiceProvider as ServiceProvider,
		SourceStaging.SurveyorName2  as SecondSurveyorName,
		SourceStaging.SurveyorMykiNo2  as SecondSurveyorMyki,
		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		--NULL,
		@RunGroupId [RunGroupID],
		'TRANSFORMED' [ProcessStatus],
		case when  DVTarget.LinkSurveyDetailTrainSid is null  then 'NEW' else 'EXISTING' end  [UpdateStatus]  /*check on one of the key is enough*/
		
		 
		from 
		(
		select
		 Link_SurveyDetailTrainSid   ,
		CAST((RIGHT(Surveydate,4) + '' + SUBSTRING(Surveydate,4,2) + '' + LEFT(Surveydate,2)) as date) as Surveydate,
		SurveyorName1,
		CAST(ActualSurveyStartTime as time(7))   ActualSurveyStartTime,
		CAST(ActualSurveyFinishTime as time(7))   ActualSurveyFinishTime,
		CAST(Mode as varchar(20)) as Mode,
		CAST(CountOn as int) as CountOn,
		CAST(CountOff as int) as CountOff,
		CAST(ShiftID as varchar(50)) as ShiftID ,
		CAST(JobNumber as varchar(50)) as JobNumber,
		CAST(Comment as varchar(250)) as Comment,
		CAST(ServiceProvider as varchar(50)) as ServiceProvider,
		CAST(SurveyorName2 as varchar(50)) as SurveyorName2,
		CAST(SurveyorMykiNo2 as varchar(50)) as SurveyorMykiNo2
		from STG.[TORS_Link_SurveyDetailTrain] 
		WHERE Rungroupid = @RunGroupId AND PROCESSSTATUS = 'EXTRACTED'
		 ) SourceStaging
		
		
		LEFT OUTER JOIN  DV.[Sat_SurveyDetailTrain]  DVTarget   /*take target reference to identify if incoming rows are new or existing*/
						on DVTarget.LinkSurveyDetailTrainSid =SourceStaging.Link_SurveyDetailTrainSid
						AND DVTarget.Surveydate =SourceStaging.Surveydate
						AND DVTarget.SurveyorName =SourceStaging.SurveyorName1
						AND DVTarget.CountOn =SourceStaging.CountOn
						AND DVTarget.CountOff =SourceStaging.CountOff
						AND DVTarget.[Shift] =SourceStaging.ShiftID
						AND DVTarget.JobNumber =SourceStaging.JobNumber
						AND DVTarget.Comment =SourceStaging.Comment
						AND DVTarget.ServiceProvider =SourceStaging.ServiceProvider
						AND DVTarget.SecondSurveyorName =SourceStaging.SurveyorName2
						AND DVTarget.SecondSurveyorMyki =SourceStaging.SurveyorMykiNo2
						AND DVTarget.CountStartTime =SourceStaging.ActualSurveyStartTime
						AND DVTarget.CountEndTime =SourceStaging.ActualSurveyFinishTime
						AND DVTarget.Mode =SourceStaging.Mode
						

			

		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Set @RowsFailed=(SELECT COUNT(1) FROM LOG.DATAQUALITYLOG WHERE [ServicePackageRunID]=@ServicePackageRunID)
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 








