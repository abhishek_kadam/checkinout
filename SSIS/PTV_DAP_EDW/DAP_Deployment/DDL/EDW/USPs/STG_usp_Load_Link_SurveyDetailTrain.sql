/* *****************************************************************************
 * Name : STG.usp_Load_Link_SurveyDetailTrain
 *
 * Created :Feb 22nd 2016
 * Author : KIRAN KUMAR
 *
 * Abstract: Data Load for Link_SurveyDetailTrain
 *			 
 *
 * Input : @RunGroupId : RunGroupId of Data Acqusition 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************

 */

IF OBJECT_ID('[STG].[usp_Load_Link_SurveyDetailTrain]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Load_Link_SurveyDetailTrain]

 


CREATE PROC [STG].[usp_Load_Link_SurveyDetailTrain] @RunGroupId [int] AS
 

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	(select count(1)
	from STG.[RDV_Link_SurveyDetailTrain] 
	where Rungroupid =@Rungroupid and ProcessStatus='TRANSFORMED' and updatestatus='NEW') /*EXISTING are not processed */
			
	BEGIN TRANSACTION
	

		/* insert data into DW staging table */

		INSERT INTO DV.[Link_SurveyDetailTrain]
		([LinkSurveyDetailTrainSid],
		[HubSurveySid],
		[HubSmartCardSid],
		[HubEntranceSid],
		[LoadDate],
		[RecordSource]
		)
		select 
		[Link_SurveyDetailTrainSid],
		[Hub_SurveySid],
		[Hub_SmartCardSid],
		[Hub_EntranceSid],
		getdate() as [LoadDate],
		[RecordSource]
		from STG.[RDV_Link_SurveyDetailTrain]
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		/*UPDATE DW STAGING for LOADED Records*/
		update 
		STG.[RDV_Link_SurveyDetailTrain]
		set ProcessStatus='LOADED'
		WHERE Updatestatus ='NEW' 
				and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId



		Set @RowsFailed=0
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


   COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
		
				 
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

 END CATCH	

End 











