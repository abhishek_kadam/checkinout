/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[usp_Transform_Link_SurveyDetail]', 'P') IS NOT NULL
  DROP PROCEDURE [STG].[usp_Transform_Link_SurveyDetail]

CREATE PROC [STG].[usp_Transform_Link_SurveyDetail] @RunGroupId [int],@RecordSource [varchar](100),@ServicePackageRunID [int],@DataEntityRefCode [varchar](50) AS

Begin  


declare @RowsProcessed  as int =0
declare @RowsFailed as int =0

BEGIN TRY 

	/*count candidate rows to be processed */
	SET @RowsProcessed=

	/*(select count(1)
	
	from [STG].[TORS_Link_SurveyDetail]
	where Rungroupid =@Rungroupid) */
	(select count(1)
	
	from (
		select	*,ROW_NUMBER() over (Partition by LinkSurveyDetailSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetail] 
		where Rungroupid =@Rungroupid ) TotalResultSet Where RN=1 )  

	----BEGIN TRANSACTION


		/* clear DW staging */
		
		delete 
		from   [STG].[RDV_Link_SurveyDetail]
		where  RunGroupID = @RunGroupID 
		and    ProcessStatus in ('TRANSFORMED', 'LOADED','EXISTING') 

	
		/* insert data into DW staging table */

		INSERT INTO   [STG].[RDV_Link_SurveyDetail]
		([Link_SurveyDetailSid],
		[Hub_SurveySid],
		[Hub_SmartCardSid],
		[Hub_StopSid],
		[Hub_VehicleSid],
		[LoadDate],
		[RecordSource],
		[ProcessStatus],
		[UpdateStatus],
		[RunGroupID]
		)
		
		select 
		SourceStaging.LinkSurveyDetailSid, 
		HS.HubSurveySid,
		HSC.HubSmartCardSid,
		HP.HubStopSid,
		HV.HubVehicleSid,
		GETDATE() [LoadDate],
		@RecordSource [RecordSource],
		'TRANSFORMED' [ProcessStatus],
		case when  DVTarget.[HubSurveySid] is null  then 'NEW' else 'EXISTING' end  [UpdateStatus],  /*check on one of the key is enough*/
		@RunGroupId [RunGroupID]

		from 
		(
		select * from (
		select LinkSurveyDetailSid,RawBatchID,CardSurfaceid,Stopid,Vehicleid,
		ROW_NUMBER() over (Partition by LinkSurveyDetailSid Order by ExtractDateTime desc) RN
		from STG.[TORS_Link_SurveyDetail] 
		where Rungroupid =@Rungroupid )TotalResultSet Where RN=1) SourceStaging 
		INNER join DV.Hub_Survey HS 
						on HS.Survey_RawBatchID=SourceStaging.RawBatchID
		
		INNER join DV.Hub_SmartCard HSC 
						on HSC.CardSurfaceid = SourceStaging.CardSurfaceid
		INNER join DV.Hub_Stop HP
						on HP.StopID=SourceStaging.StopID
	    INNER join DV.Hub_Vehicle HV
						on HV.VehicleID=SourceStaging.VehicleID
		LEFT OUTER JOIN DV.[Link_SurveyDetail] DVTarget /*take target reference to identify if incoming rows are new or existing*/
						on  DVTarget.LinkSurveyDetailSid=SourceStaging.LinkSurveyDetailSid


						------Extract Pending data collection starts-----
       IF OBJECT_ID('#tmpFailed_Link_SurveyDetail', 'U') IS NOT NULL DROP TABLE #tmpFailed_Link_SurveyDetail
		---create tmp table reference for failed records 
		create table #tmpFailed_Link_SurveyDetail
		WITH 
		  ( 
			CLUSTERED COLUMNSTORE INDEX,
			DISTRIBUTION = ROUND_ROBIN 
		  )
		AS

		
		select 
		SourceStaging.LinkSurveyDetailSid,
		HS.[HubSurveySid] ,
		HS.Survey_RawBatchID,
		HSC.[HubSmartCardSid] ,
		HSC.CardSurfaceid,
		HP.[HubStopSid] ,
		HP.Stopid,
		HV.[HubVehicleSid] ,
		HV.VehicleID,
		SourceStaging.SurveyDate,
		SourceStaging.SurveyorName,
		SourceStaging.RouteNumber,
		SourceStaging.CountStartTime,
		SourceStaging.CountEndTime,
		SourceStaging.[Time],
		SourceStaging.CountOn,
		SourceStaging.Countoff,
		SourceStaging.AlreadyOn,
		SourceStaging.RemainedOn,
		SourceStaging.[Sequence],
		SourceStaging.[Shift],
		SourceStaging.JobNumber,
		SourceStaging.Door,
		SourceStaging.Comment,
		SourceStaging.ServiceProvider
		
		From
		(
	select * from (
			select * ,ROW_NUMBER() over (Partition by LinkSurveyDetailSid Order by ExtractDateTime desc) RN 
			FROM STG.[TORS_Link_SurveyDetail]  WHERE Rungroupid = @RunGroupId )TotalResultSet Where RN=1)
		SourceStaging 

			LEFT OUTER JOIN DV.Hub_Survey HS  
						on HS.Survey_RawBatchID=SourceStaging.RawBatchID

			LEFT OUTER JOIN  DV.Hub_SmartCard HSC  
						on HSC.CardSurfaceid = SourceStaging.CardSurfaceid
			LEFT OUTER JOIN DV.Hub_Stop HP 
						on HP.StopID=SourceStaging.StopID
			LEFT OUTER JOIN DV.Hub_Vehicle HV
						on HV.VehicleID=SourceStaging.VehicleID		
							
		WHERE HSC.CardSurfaceID IS NULL OR HP.StopID IS NULL  OR HS.Survey_RawBatchID IS NULL OR HV.VehicleID IS NULL




				---Extract Pending data collection end-------


		
		INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2],
		[KeyAttributeValue3],
		[KeyAttributeValue4]
		)
		SELECT
		0 [DataQualityRuleID],
		1 [RuleVersionNum],
		@ServicePackageRunID [ServicePackageRunID],
		@DataEntityRefCode [DataEntityRefCode],
		'ERROR' [SeverityLevelRefCode],
		@RecordSource [DataSourceRefCode],
		'ISSUE' [QualityIssueRefCode],

		CASE 
			WHEN HubSurveySid IS NULL THEN 'HubSurveySid' 
			WHEN HubSmartCardSid IS NULL THEN 'HubSmartCardSid'
			WHEN HubStopSid IS NULL THEN 'HubStopSid'
		ELSE
			 'HubVehicleSid' 
			 END [AttributeName],
		NULL [OriginalValue],
		NULL [TransformedValue],

		'Unable to find ' + CASE 
								WHEN HubSurveySid IS  NULL THEN 'HubSurveySid' 
								WHEN HubSmartCardSid IS  NULL THEN 'HubSmartCardSid'
								WHEN HubStopSid IS NULL THEN 'HubStopSid'
								ELSE 'HubVehicleSid' END + ' when transforming LINK_SurveyDetail from '+ @RecordSource [MessageText],
          'LinkSurveyDetailSid :'++isnull(cast(LinkSurveyDetailSid as varchar(20)),'NULL')   [ReferenceID],
		GETDATE() [CreatedDateTime],
		SYSTEM_USER [CreatedBy],
		Survey_RawBatchID [KeyAttributeValue1],
		CardSurfaceid [KeyAttributeValue2],
		Stopid [KeyAttributeValue3],
		Vehicleid [KeyAttributeValue4]
		FROM #tmpFailed_Link_SurveyDetail
				
		

		/*Extract pending not valid here as source data will always be available in full through Data Lake*/

		Update   STG.[TORS_Link_SurveyDetail] set ProcessStatus='EXTRACTED' WHERE ProcessStatus='EXTRACTPENDING'


		update STG.[TORS_Link_SurveyDetail]
		set STG.[TORS_Link_SurveyDetail].Processstatus='EXTRACTPENDING'
		FROM #tmpFailed_Link_SurveyDetail FAILED_RECORDS 
		WHERE STG.[TORS_Link_SurveyDetail].LinkSurveyDetailSid= 
		      FAILED_RECORDS.LinkSurveyDetailSid



		Set @RowsFailed=(SELECT COUNT(1) FROM LOG.DATAQUALITYLOG WHERE [ServicePackageRunID]=@ServicePackageRunID)
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed


  -- COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)
	BEGIN
 
		SET  @RowsFailed = isnull(@RowsProcessed,0)

		ROLLBACK TRANSACTION
	
		SELECT isnull(@RowsProcessed,0) RowsProcessed, ISNULL(@RowsFailed,0)  RowsFailed  --send resultset back 
	END;
		
	THROW 

	truncate table  #tmpFailed_Link_SurveyDetail
	drop table  #tmpFailed_Link_SurveyDetail

 END CATCH	

 truncate table  #tmpFailed_Link_SurveyDetail
 drop table  #tmpFailed_Link_SurveyDetail

End 