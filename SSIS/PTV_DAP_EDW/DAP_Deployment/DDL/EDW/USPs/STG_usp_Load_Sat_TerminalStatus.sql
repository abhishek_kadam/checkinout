IF OBJECT_ID('[STG].[usp_Load_Sat_TerminalStatus]', 'P') IS NOT NULL
 
BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_TerminalStatus]
 
END;
 Go

CREATE  PROC [STG].[usp_Load_Sat_TerminalStatus] @RunGroupId [int] AS

/* *****************************************************************************
 * Name : STG.usp_Load_SAT_TERMINALGROUPLOCATION
 *
 * Created :Feb 8th 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Loads data from staging to raw data vault for SAT_TERMINALGROUPLOCATION
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
 * 
 * *****************************************************************************
 
  */
Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_TERMINALSTATUS where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_TERMINALSTATUS 
		SET DV.SAT_TERMINALSTATUS.LoadEndDate =GETDATE(),
			DV.SAT_TERMINALSTATUS.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_TERMINALSTATUS STG 
			 WHERE  DV.SAT_TERMINALSTATUS.HubTerminalStatusSid=STG.HubTerminalStatusSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_TERMINALSTATUS
		(HubTerminalStatusSid,
		
		TerminalStatusDesc,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubTerminalStatusSid,
			
			TerminalStatusDesc,
			getdate() LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_SAT_TERMINALSTATUS
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
