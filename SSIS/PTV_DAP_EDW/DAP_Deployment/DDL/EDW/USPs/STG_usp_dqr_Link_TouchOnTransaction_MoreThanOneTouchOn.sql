/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[usp_dqr_Link_TouchOnTransaction_MoreThanOneTouchOn]', 'U') IS NOT NULL
                DROP TABLE [STG].[usp_dqr_Link_TouchOnTransaction_MoreThanOneTouchOn]

CREATE PROC [STG].[usp_dqr_Link_TouchOnTransaction_MoreThanOneTouchOn] @DataQualityEntityID [int],@ServicePackageRunID [int],@RunGroupID [int] AS


/* *****************************************************************************
 * Name    : usp_dqr_Link_TouchOnTransaction_MoreThanTwoTouchOns
 *
 * Abstract: Data validation to check if Two or more touch Ons should not occur for the same card, same terminal same date and same time. 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(400)

    declare @dataQualityRuleID int=(select dqe.DataQualityRuleID from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataEntityRefCode varchar(50)=(select dqe.DataEntityRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @dataSourceRefCode varchar(50)=(select dqe.DataSourceRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @ruleVersionNum smallint=(select dqr.RuleVersionNum from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @severityLevelRefCode varchar(50)=(select dqr.SeverityLevelRefCode from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    declare @description varchar(500)=(select dqr.[Description] from DAF.DataQualityEntity dqe join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID  where  dqe.DataQualityEntityID = @DataQualityEntityID)
    
    	
   
    --inserting record into log for failed records
        INSERT INTO LOG.DATAQUALITYLOG ([DataQualityRuleID],
		[RuleVersionNum],
		[ServicePackageRunID],
		[DataEntityRefCode],
		[SeverityLevelRefCode],
		[DataSourceRefCode],
		[QualityIssueRefCode],
		[AttributeName],
		[OriginalValue],
		[TransformedValue],
		[MessageText],
		[ReferenceID],
		[CreatedDateTime],
		[CreatedBy],
		[KeyAttributeValue1],
		[KeyAttributeValue2],
		[KeyAttributeValue3],
		[KeyAttributeValue4],
		[KeyAttributeValue5],
		[KeyAttributeValue6],
		[KeyAttributeValue7],
		[KeyAttributeValue8],
		[KeyAttributeValue9]
		)
    select  @dataQualityRuleID
            ,@ruleVersionNum
            ,@ServicePackageRunID
            ,@dataEntityRefCode
            ,@severityLevelRefCode
            ,@dataSourceRefCode
            ,'ISSUE'
            ,'LinkTouchOnTransactionSid'
			,null
			,null
			,@description
			,cast(LinkTouchOnTransactionSid as varchar(50))
			,getdate()
            ,'dapetlusr'
			,CardSurfaceId
			,TransactionDate
			,TransactionTime
            ,TerminalId
			,FareProductType
            ,ProductSerialNumber
			,StopId
			,RouteId
			,NULL   
		
                    
	from   (select SC.CardSurfaceId,a.TransactionDate,a.TransactionTime,T.TerminalId,null as FareProductType,FP.ProductSerialNumber,S.StopId,R.RouteId,LinkTouchOnTransactionSid from 
STG.[RDV_Link_TouchOnTransaction] a 
inner join 
(select HubSmartCardSid,HubTerminalSid,TransactionDate,TransactionTime,count(1)C from STG.[RDV_Link_TouchOffTransaction]
group by HubSmartCardSid,HubTerminalSid,TransactionDate,TransactionTime
having count(1)>=2) b on a.HubSmartCardSid=b.HubSmartCardSid and a.HubTerminalSid=b.HubTerminalSid and a.TransactionDate=b.TransactionDate and a.TransactionTime=b.TransactionTime
inner join DV.Hub_Smartcard SC on SC.HubSmartCardSid=a.HubSmartCardSid
inner join DV.Hub_Terminal T on T.HubTerminalSid=a.HubTerminalSid
inner join DV.HUB_FAREPRODUCT FP on FP.HubFareProductSid=a.HubFareProductSid
inner join DV.Hub_Stop  S on S.HubStopSid=a.HubStopSid
inner join DV.Hub_Route R on R.HubRouteSid=a.HubRouteSid
where   RunGroupID = @runGroupID
    and   ProcessStatus = 'TRANSFORMED' and UpdateStatus='NEW')Main



end







