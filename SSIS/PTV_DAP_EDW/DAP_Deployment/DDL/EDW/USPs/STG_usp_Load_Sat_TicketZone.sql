﻿
/* *****************************************************************************
 * Name : STG.usp_Load_Sat_TicketZone
 *
 * Created :Feb 23rd 2016
 * Author : Kiran Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_TicketZone
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************
  */

IF OBJECT_ID('[STG].[usp_Load_Sat_TicketZone]', 'P') IS NOT NULL
 
BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_TicketZone]
 
END;
 Go



Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_TicketZone where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_TicketZone 
		SET		  DV.Sat_TicketZone.LoadEndDate =GETDATE(),
			      DV.Sat_TicketZone.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_TicketZone STG 
	    WHERE	  DV.Sat_TicketZone.HubZoneSid=STG.HubZoneSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_TicketZone
		(HubZoneSid,
		TicketZoneID,
		Zone,
		ZoneOrder,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	HubZoneSid,
				TicketZoneID,
				Zone,
				ZoneOrder,
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_TicketZone

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 
