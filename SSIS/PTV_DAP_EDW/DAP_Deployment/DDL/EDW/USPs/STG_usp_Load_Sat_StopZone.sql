﻿/* *****************************************************************************
 * Name : STG.usp_Load_SAT_StopZone
 *
 * Created :Feb 25th 2016
 * Author : Ramakrishna
 *
 * Abstract: Loads data from staging to raw data vault for SAT_StopZone
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
 *
  */

IF OBJECT_ID('[STG].[usp_Load_Sat_StopZone]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_StopZone]
 END;
Go

CREATE PROC [STG].[usp_Load_Sat_StopZone] @RunGroupId [int] AS
BEGIN 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	SET  @RowsProcessed =
	(SELECT   COUNT(1) 
	FROM STG.RDV_SAT_StopZone WHERE RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE DV.SAT_StopZone
		SET DV.SAT_StopZone.LoadEndDate =GETDATE(),
			DV.SAT_StopZone.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_StopZone STG 
		WHERE  DV.SAT_StopZone.HubStopSid=STG.HubStopSid
		AND   STG.Updatestatus ='UPDATE' 
		AND STG.ProcessStatus='TRANSFORMED' 
		AND STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_StopZone
		([HubStopSid] , 
		[ZoneStopId], 
		[Zone], 
		[LoadDate], 
		[LoadEndDate], 
		[IsCurrentFlag], 
		[RecordSource]
		)
		SELECT [HubStopSid] , 
		[ZoneStopId], 
		[Zone], 
		getdate() LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		FROM STG.RDV_SAT_StopZone
		WHERE Updatestatus ='NEW' 
		AND ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
