﻿IF OBJECT_ID('[STG].[usp_Load_Sat_Terminal]', 'P') IS NOT NULL
 BEGIN
  DROP PROCEDURE [STG].[usp_Load_Sat_Terminal]
 END;
 Go


CREATE PROC [STG].[usp_Load_Sat_Terminal] @RunGroupId [int] AS

/* *****************************************************************************
 * Name : STG.usp_Load_Sat_Terminal
 *
 * Created :Feb 16th 2016
 * Author : Swathi Reddy
 *
 * Abstract: Loads data from staging to raw data vault for Sat_Terminal
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * Swathi Reddy   	1.0			16/02/2016	--		Initial version 
 *
 *
  */
Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
 
 Set  @RowsProcessed =(SELECT  COUNT(1) from STG.RDV_SAT_Terminal where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_Terminal 
		SET DV.Sat_Terminal.LoadEndDate =GETDATE(),
			DV.Sat_Terminal.IsCurrentFlag='N'

		FROM  STG.RDV_Sat_Terminal STG 
	    WHERE  DV.Sat_Terminal.HubTerminalSid=STG.HubTerminalSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.Sat_Terminal 
		(HubTerminalSid,
		CreatedDate,
		PositionId,
		TerminalGroupLocationDescription,
		TerminalPosition,
		TerminalStatusDesc,
		TerminalStatusId,
		TerminalSubgroupLocationDescription,
		TerminalSubgroupLocationIdentifier,
		TerminalType,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select HubTerminalSid,
			CreatedDate,
			PositionId,
			TerminalGroupLocationDescription,
			TerminalPosition,
			TerminalStatusDesc,
			TerminalStatusId,
			TerminalSubgroupLocationDescription,
			TerminalSubgroupLocationIdentifier,
			TerminalType,
			getdate() LoadDate,
			Null as LoadEndDate,
			IsCurrentFlag,
			RecordSource
		from STG.RDV_Sat_Terminal
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

 IF(@@trancount>0)

 BEGIN
 
 set  @RowsFailed = @RowsProcessed
 ROLLBACK TRANSACTION
 Select @RowsProcessed, @RowsFailed
 END
 ;
 THROW 50000,'Unable to load SAT_TERMINAL from MYKI into data warehouse.',1;

 END CATCH
		
End 

