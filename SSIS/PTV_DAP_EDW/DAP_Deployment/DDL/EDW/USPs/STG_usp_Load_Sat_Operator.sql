﻿
/* *****************************************************************************
 * Name : STG.usp_Load_SAT_OPERATOR
 *
 * Created :Feb 16th 2016
 * Author : Kiran Kumar
 * Abstract: Loads data from staging to raw data vault for SAT_OPERATOR
 *
 * Input : @RunGroupId : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed : Number of records failed
   
 * *****************************************************************************
 */ 


 IF OBJECT_ID('[STG].[usp_Load_Sat_Operator]', 'U') IS NOT NULL
   DROP Procedure [STG].[usp_Load_Sat_Operator]
go

CREATE PROC [STG].[usp_Load_Sat_Operator] @RunGroupId [int] AS

Begin 
	BEGIN TRANSACTION

	DECLARE @RowsProcessed int 
	DECLARE @RowsFailed int

	--@RowsProcessed = COUNT(1) 
	/*Note down Number of records processed in Load Process*/
	set  @RowsProcessed =
	(SELECT   COUNT(1) 
	from STG.RDV_SAT_Operator where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )


	
	/* Update records */
	
		UPDATE 	  DV.SAT_Operator
		SET DV.SAT_Operator.LoadEndDate =GETDATE(),
			DV.SAT_Operator.IsCurrentFlag='N'

		FROM  STG.RDV_SAT_Operator STG 
			 WHERE  DV.SAT_OPerator.HubOperatorSid=STG.HubOperatorSid
		AND   STG.Updatestatus ='UPDATE' AND STG.ProcessStatus='TRANSFORMED' and STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	
		INSERT INTO DV.SAT_Operator 
		(HubOperatorSid,
		OperatorName,
		BusinessName,
		TradingName,
		GroupName,
		StreetAddress,
		Suburb,
		OperatorMode,
		PostCode,
		DataSystem,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		select  HubOperatorSid,
				OperatorName,
				BusinessName,
				TradingName,
				GroupName,
				StreetAddress,
				Suburb,
				OperatorMode,
				PostCode,
				DataSystem,
				getdate() LoadDate,
				LoadEndDate,
				IsCurrentFlag,
				RecordSource
		from STG.RDV_SAT_Operator
		WHERE Updatestatus ='NEW' 
		and ProcessStatus='TRANSFORMED' and RunGroupid=@RunGroupId

		set @RowsFailed=0  

		Select @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION
		
End 
