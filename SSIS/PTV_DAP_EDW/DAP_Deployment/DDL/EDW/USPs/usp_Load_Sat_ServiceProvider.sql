﻿

/* *****************************************************************************
 * Name : STG.usp_Load_Sat_ServiceProvider
 *
 * Created :Feb 18th 2016
 * Author : Kiran Kumar
 *
 * Abstract: Loads data from staging to raw data vault for Sat_ServiceProvider
 *
 * Input : @RunGroupId	  : RungGroupid from DAF.RunGroup table 
 * 
 * Output :@RowsProcessed : Number of records processed 
 *		   @RowsFailed	  : Number of records failed
 *
 * 
 * *****************************************************************************

  */




IF OBJECT_ID('[STG].[usp_Load_Sat_ServiceProvider]', 'U') IS NOT NULL
   DROP PROCEDURE [STG].[usp_Load_Sat_ServiceProvider]

go




CREATE PROC [STG].[usp_Load_Sat_ServiceProvider] @RunGroupId [int] AS
Begin 
 
 DECLARE @RowsProcessed int 
 DECLARE @RowsFailed int
	SET  @RowsProcessed =	(SELECT   COUNT(1) 	from STG.RDV_SAT_ServiceProvider where RunGroupid=@RunGroupId and processstatus ='TRANSFORMED' )

 BEGIN TRY

	BEGIN TRANSACTION

	
	/* Update records */
	
		UPDATE 	  DV.Sat_ServiceProvider
		SET		  DV.Sat_ServiceProvider.LoadEndDate =GETDATE(),
			      DV.Sat_ServiceProvider.IsCurrentFlag='N'

		FROM	  STG.RDV_Sat_ServiceProvider STG 
	    WHERE	  DV.Sat_ServiceProvider.HubServiceProviderSid=STG.HubServiceProviderSid AND
				  STG.Updatestatus ='UPDATE' AND 
				  STG.ProcessStatus='TRANSFORMED' and 
				  STG.RunGroupid=@RunGroupId


		
	/* Insert NEW records */	

		INSERT INTO DV.Sat_ServiceProvider 
		(HubServiceProviderSid,
		BusinessType,
		EffectiveDate,
		IssuerInd,
		LoadAgentPPInd,
		LoadAgentInd,
		OperatorInd,
		OverrideFundCollectInd,
		SettlementReportType,
		ServiceProviderGrpDesc,
		ServiceProviderGrpId,
		ServiceProviderName,
		ServiceProviderShortname,
		LoadDate,
		LoadEndDate,
		IsCurrentFlag,
		RecordSource
		)
		SELECT	[HubServiceProviderSid] , 
    [BusinessType] , 
    [EffectiveDate], 
    [IssuerInd] , 
    [LoadAgentPPInd] , 
    [LoadAgentInd] , 
    [OperatorInd] , 
    [OverrideFundCollectInd] , 
    [SettlementReportType] , 
    [ServiceProviderGrpDesc] , 
    [ServiceProviderGrpId] , 
    [ServiceProviderName] , 
    [ServiceProviderShortname],
				getdate() LoadDate,
				Null as LoadEndDate,
				IsCurrentFlag,
				RecordSource
		FROM	STG.RDV_Sat_ServiceProvider

		WHERE	Updatestatus ='NEW' and
				ProcessStatus='TRANSFORMED' and 
				RunGroupid=@RunGroupId

		SET @RowsFailed=0  

		SELECT @RowsProcessed, @RowsFailed 

	COMMIT TRANSACTION

 END TRY

 BEGIN CATCH

	IF(@@trancount>0)

	BEGIN
 
		SET  @RowsFailed = @RowsProcessed

		ROLLBACK TRANSACTION

		SELECT @RowsProcessed, @RowsFailed
	END;
 
 THROW 




 END CATCH
		
End 
