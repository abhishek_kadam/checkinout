﻿CREATE VIEW [EXT].[vw_SMARTRAK_Sat_Point] AS (

SELECT CASE WHEN [PointId] ='NULL' then NULL else [PointId] end [PointId]  ,  
       case when [PointName] ='NULL' then NULL else [PointName] end [PointName] ,
	   case when [PointType] ='NULL' then NULL else [PointType] end [PointType] ,
	   case when [Latitude] ='NULL' then NULL else [Latitude] end [Latitude] ,
	   case when [Longitude] ='NULL' then NULL else [Longitude] end [Longitude] ,
	   RECORDSOURCE,
	   [DAF].[GenerateHashKey]((case when PointId ='NULL' then '' else PointId end), '$') as [HubLocationPointSid]

from 
(
	SELECT PointId, PointName, PointType, Latitude, Longitude, RECORDSOURCE,
	ROW_NUMBER() OVER (PARTITION BY PointId, PointName, PointType, Latitude, Longitude ORDER BY  RECORDSOURCE_PRIORITY) RN

	FROM (
			SELECT distinct PointId, PointName, PointType, Latitude, Longitude, 'Smartrak Remote' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY 
			from [EXT].[SMARTRAK_PTDE_Remote]   where PointId <> 'PointId'
			UNION ALL
			SELECT distinct PointId, PointName, PointType, Latitude, Longitude, 'Smartrak Line' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY  
			from [EXT].[SMARTRAK_PTDE_Line]    where PointId <> 'PointId'			
			) JourneyUnion
			   ) JourneyWithrank
		where RN=1
		);