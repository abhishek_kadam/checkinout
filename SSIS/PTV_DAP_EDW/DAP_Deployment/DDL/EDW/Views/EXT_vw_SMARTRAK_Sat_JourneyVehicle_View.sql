﻿CREATE VIEW [EXT].[vw_SMARTRAK_Sat_JourneyVehicle] AS (

SELECT CASE WHEN [ServiceId] ='NULL' then NULL else [ServiceId] end [ServiceId]  ,  
       case when [RemoteId] ='NULL' then NULL else [RemoteId] end [RemoteId] ,
	   case when [Direction] ='NULL' then NULL else [Direction] end [Direction] ,
	   case when [ParentRoute] ='NULL' then NULL else [ParentRoute] end [ParentRoute] ,	   	   
	   case when [OperatingDay] ='NULL' then NULL else [OperatingDay] end [OperatingDay] ,
	   case when [Origin] ='NULL' then NULL else [Origin] end [Origin] ,
	   case when [IsServiceCancelled] ='NULL' then NULL else [IsServiceCancelled] end [IsServiceCancelled] ,
	   case when [RunID] ='NULL' then NULL else [RunID] end [RunID] ,
	   case when [DepotName] ='NULL' then NULL else [DepotName] end [DepotName] ,
	   case when [Destination] ='NULL' then NULL else [Destination] end [Destination],
	   case when [Mode] ='NULL' then NULL else [Mode] end [Mode],	   
	   RECORDSOURCE,
	   [DAF].[GenerateHashKey]((case when ServiceId='NULL' then '' else ServiceId end)+'$'+ 
						(case when RemoteId='NULL' then '' else RemoteId end)+'$'+
						(case when Direction='NULL' then '' else Direction end)+'$'+
						(case when [ParentRoute]='NULL' then '' else ParentRoute end)+'$'+
						(case when OperatingDay='NULL' then '' else OperatingDay end), '$') as [HubJourneyVehicleSid]

from 
(
	SELECT distinct [ServiceId], [RemoteId],[Direction],[ParentRoute],[OperatingDay], [Origin],[IsServiceCancelled],[RunID],[DepotName],[Destination],[Mode],RECORDSOURCE,
	ROW_NUMBER() OVER (PARTITION BY [ServiceId], [RemoteId],[Direction],[ParentRoute],[OperatingDay] ORDER BY  RECORDSOURCE_PRIORITY) RN

	FROM (
			SELECT distinct [ServiceId], [RemoteId],[Direction],[ParentRoute],[OperatingDay], [Origin],[IsServiceCancelled],[RunID],[DepotName],[Destination],[Mode], 'Smartrak Remote' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY
			from [EXT].[SMARTRAK_PTDE_Remote]  WHERE RemoteId<>'RemoteId'
			UNION ALL
			SELECT distinct [ServiceId], [RemoteId],[Direction],[ParentRoute],[OperatingDay], [Origin],[IsServiceCancelled],[RunID],[DepotName],[Destination],[Mode], 'Smartrak Line' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY
			from [EXT].[SMARTRAK_PTDE_Line]  WHERE RemoteId<>'RemoteId'
			
			) JourneyUnion
			   ) JourneyWithrank
		where RN=1
		);