﻿CREATE VIEW [EXT].[vw_TransPROD_Sat_ParentRoute] AS (
SELECT DISTINCT CASE WHEN LineMainID='NULL' THEN NULL ELSE LineMainID END LineMainID,
	CASE WHEN LineNotes='NULL' THEN NULL ELSE LineNotes END LineNotes,
	CASE WHEN LineDescShort='NULL' THEN NULL ELSE LineDescShort END LineDescShort,
	CASE WHEN LineDescLong='NULL' THEN NULL ELSE LineDescLong END LineDescLong,
	CASE WHEN EffectiveDate = NULL THEN DATETIME2FROMPARTS ( 1900, 01, 01, 00, 00, 00, 0, 0 ) ELSE Cast(EffectiveDate as datetime2)  END EffectiveDate,
	CASE WHEN TermDate = NULL THEN DATETIME2FROMPARTS ( 1900, 01, 01, 00, 00, 00, 0, 0 ) ELSE Cast(TermDate as datetime2)  END TermDate,
	CASE WHEN WheelChairAccess='NULL' THEN NULL ELSE WheelChairAccess END WheelChairAccess,
	CASE WHEN PublicDescShort ='NULL' THEN NULL ELSE PublicDescShort END PublicDescShort,
	CASE WHEN ReducedHolServ='NULL' THEN NULL ELSE ReducedHolServ END ReducedHolServ,
	CASE WHEN StopTTtemplate ='NULL' THEN NULL ELSE StopTTtemplate END StopTTtemplate,
	CASE WHEN WheelChairAccessSat='NULL' THEN NULL ELSE WheelChairAccessSat END WheelChairAccessSat,
	CASE WHEN WheelChairAccessSun ='NULL' THEN NULL ELSE WheelChairAccessSun END WheelChairAccessSun

FROM EXT.TRANSPROD_tblLinesMain 
WHERE LineMainID <> 'LineMainID'

);