/*****************************
Author :  PRakash
Modified Date : 03/09/2016
****************************/
IF OBJECT_ID('[EXT].[vw_SMARTRAK_Journey_Vehicle]', 'V') IS NOT NULL
  DROP PROCEDURE [EXT].[vw_SMARTRAK_Journey_Vehicle]

CREATE VIEW [EXT].[vw_SMARTRAK_Journey_Vehicle] AS select distinct   [ServiceId] , [RemoteId],[Direction], [ParentRoute],[OperatingDay]  ,

[DAF].[GenerateHashKey]((case when ServiceId='NULL' then '' else ServiceId end)+'$'+ 
						(case when RemoteId='NULL' then '' else RemoteId end)+'$'+
						(case when Direction='NULL' then '' else Direction end)+'$'+
						(case when ParentRoute='NULL' then '' else ParentRoute end)+'$'+
						(case when OperatingDay='NULL' then '' else OperatingDay end), '$') as [Hub_JourneyVehicleSid]
   
from (
Select  
   case when [ServiceId] ='NULL' then NULL  else [ServiceId] end  [ServiceId],
   case when [RemoteId] ='NULL' then NULL  else [RemoteId] end  [RemoteId],
   case when [Direction] ='NULL' then NULL  else [Direction] end  [Direction],
   case when [ParentRoute] ='NULL' then NULL  else [ParentRoute] end  [ParentRoute],
   case when [OperatingDay] ='NULL' then NULL  else [OperatingDay] end  [OperatingDay]
    
   from [EXT].[SMARTRAK_PTDE_Remote] 
   WHERE RemoteId<>'RemoteId'
   
     UNION ALL

 Select  
   case when [ServiceId] ='NULL' then NULL  else [ServiceId] end  [ServiceId],
   case when [RemoteId] ='NULL' then NULL  else [RemoteId] end  [RemoteId],
   case when [Direction] ='NULL' then NULL  else [Direction] end  [Direction],
   case when [ParentRoute] ='NULL' then NULL  else [ParentRoute] end  [ParentRoute],
   case when [OperatingDay] ='NULL' then NULL  else [OperatingDay] end  [OperatingDay] 
   from [EXT].[SMARTRAK_PTDE_Line] 
   WHERE RemoteId<>'RemoteId') JourneyVehicle;