
/*****************************
Author :  Abhishek Kadam
Modified Date : 03/09/2016
****************************/
IF OBJECT_ID('[EXT].[vw_TORS_LinkSurveyDetail]', 'V') IS NOT NULL
  DROP VIEW [EXT].[vw_TORS_LinkSurveyDetail]

CREATE VIEW [EXT].[vw_TORS_LinkSurveyDetail] AS (
Select Distinct 
case WHEN RawBatchID = 'NULL' then null else RawBatchID end RawBatchID,
case WHEN SurveyorMykiNo1 = 'NULL' then null else SurveyorMykiNo1 end SurveyorMykiNo1,
case WHEN MetlinkStopID = 'NULL' then null else MetlinkStopID end MetlinkStopID ,
case WHEN Vehicle_BusRego_TramNo = 'NULL' then null else Vehicle_BusRego_TramNo end Vehicle_BusRego_TramNo ,
case WHEN SurveyDate = 'NULL' then null else SurveyDate end SurveyDate,
case WHEN SurveyorName1 = 'NULL' then null else SurveyorName1 end SurveyorName1 ,
case WHEN [Route] = 'NULL' then null else [Route] end [Route],
case WHEN ActualSurveyStartTime = 'NULL' then null else ActualSurveyStartTime end ActualSurveyStartTime ,
case WHEN ActualSurveyFinishTime = 'NULL' then null else ActualSurveyFinishTime end ActualSurveyFinishTime,
case WHEN [Time] = 'NULL' then null else [Time] end [Time] ,
case WHEN CountOn = 'NULL' then null else CountOn end CountOn,
case WHEN CountOff = 'NULL' then null else CountOff end CountOff,
case WHEN AlreadyOn = 'NULL' then null else AlreadyOn end AlreadyOn,
case WHEN RemainingOn = 'NULL' then null else RemainingOn end RemainingOn,
case WHEN [Sequence] = 'NULL' then null else [Sequence] end [Sequence] ,
case WHEN ShiftID = 'NULL' then null else ShiftID end ShiftID ,
case WHEN JobNumber = 'NULL' then null else JobNumber end JobNumber ,
case WHEN Door = 'NULL' then null else Door end Door ,
case WHEN Comment = 'NULL' then null else Comment end Comment,
case WHEN ServiceProvider = 'NULL' then null else ServiceProvider end ServiceProvider,
CAST(HASHBYTES('SHA1',
			                      case when RawBatchID ='NULL' then '' else RawBatchID end+
                                  
								  case when MetlinkStopID ='NULL' then '' else MetlinkStopID end+
								  case when Vehicle_BusRego_TramNo ='NULL' then '' else Vehicle_BusRego_TramNo end+
								  case when JobNumber ='NULL' then '' else JobNumber end+
								  case when SurveyDate ='NULL' then '' else SurveyDate end+
								  case when Door ='NULL' then '' else Door end+
								  case when [Time] ='NULL' then '' else [Time] end
								  
								  ) as Binary(20))
								    LinkSurveyDetailSid 




from (

select  DISTINCT RawBatchID , SurveyorMykiNo1 ,MetlinkStopID ,Vehicle_BusRego_TramNo , 
SurveyDate,SurveyorName1,[Route],ActualSurveyStartTime,ActualSurveyFinishTime,[Time],CountOn,CountOff,AlreadyOn,RemainingOn,
[Sequence],ShiftID,JobNumber,Door,Comment,ServiceProvider  from EXT.TORS_BUS where RawBatchID<>'RawBatchID'
Union all
Select DISTINCT RawBatchID  ,SurveyorMykiNo1 ,MetlinkStopID ,Vehicle_BusRego_TramNo  , 
SurveyDate,SurveyorName1,[Route],ActualSurveyStartTime,ActualSurveyFinishTime,[Time],CountOn,CountOff,AlreadyOn,RemainingOn,
[Sequence],ShiftID,JobNumber,Door,Comment,ServiceProvider  from EXT.TORS_SCHOOLBUS where RawBatchID<>'RawBatchID'
UNION ALL
Select DISTINCT RawBatchID ,SurveyorMykiNo1 ,MetlinkStopID ,Vehicle_BusRego_TramNo ,
SurveyDate,SurveyorName1,[Route],ActualSurveyStartTime,ActualSurveyFinishTime,[Time],CountOn,CountOff,AlreadyOn,RemainingOn,
[Sequence],ShiftID,JobNumber,Door,Comment,ServiceProvider from EXT.TORS_Trams where RawBatchID<>'RawBatchID'
)A
);



