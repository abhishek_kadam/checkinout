﻿CREATE VIEW [EXT].[vw_TransPROD_Sat_StopDetail] AS (

	SELECT	CASE WHEN [PrimaryStopName] ='NULL' then NULL else [PrimaryStopName] end [PrimaryStopName] ,  
		CASE WHEN [SecondStopName] ='NULL' then NULL else [SecondStopName] end [SecondStopName],  
		CASE WHEN [StreetAddress] ='NULL' then NULL else [StreetAddress] end [StreetAddress] , 	
			 
		CASE WHEN [SuburbID] ='NULL' then NULL else [SuburbID] end [Suburb],  
		CASE WHEN [StopType] ='NULL' then NULL else [StopType] end [StopType],  
		CASE WHEN [BayNbr] ='NULL' then NULL else [BayNbr] end [BayNumber],  

		CASE WHEN [LowFloorAccess] ='NULL' then NULL else [LowFloorAccess] end [LowFloorAccess],
		CASE WHEN [GPSLat] ='NULL' then NULL else [GPSLat] end [GPSLat],
		CASE WHEN [GPSLong] ='NULL' then NULL else [GPSLong] end [GPSLong],

		CASE WHEN [StopNumber] ='NULL' then NULL else [StopNumber] end [StopNumber],
		CASE WHEN [TimetableCode] ='NULL' then NULL else [TimetableCode] end [TimetableCode],
		CASE WHEN [PlatformNbr] ='NULL' then NULL else [PlatformNbr] end [PlatformNbr],

		CASE WHEN [TrainNbrPlatforms] ='NULL' then NULL else [TrainNbrPlatforms] end [TrainNbrPlatforms],
		CASE WHEN [TrainTimeTable] ='NULL' then NULL else [TrainTimeTable] end [TrainTimeTable],
		CASE WHEN [StationName] ='NULL' then NULL else [StationName] end [StationName],

		CASE WHEN [StopLandmark] ='NULL' then NULL else [StopLandmark] end [StopLandmark],
		CASE WHEN [StopSpecName] ='NULL' then NULL else [StopSpecName] end [StopSpecName],
		Case WHEN [MetlinkStopID] ='NULL' then NULL else [MetlinkStopID] end [MetlinkStopID]
       
			from [EXT].[TRANSPROD_tblStopinformation]  WHERE MetlinkStopID <> 'MetlinkStopID'

		);