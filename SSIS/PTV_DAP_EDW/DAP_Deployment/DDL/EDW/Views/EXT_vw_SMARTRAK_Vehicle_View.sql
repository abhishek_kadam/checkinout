﻿CREATE VIEW [EXT].[vw_SMARTRAK_Vehicle] AS (

SELECT CASE WHEN [ALIAS] ='NULL' then NULL else [ALIAS] end [ALIAS]  ,  
       case when [COMPANYNAME] ='NULL' then NULL else [COMPANYNAME] end [COMPANYNAME] ,
	   case when [FORWARDERID] ='NULL' then NULL else [FORWARDERID] end [FORWARDERID] ,
	   case when [NAME] ='NULL' then NULL else [NAME] end [NAME] ,
	   case when [REGO] ='NULL' then NULL else [REGO] end [REGO] ,
	   case when [REMOTEID] ='NULL' then NULL else [REMOTEID] end [REMOTEID] ,
	   RECORDSOURCE,
	   [DAF].[GenerateHashKey]((case when REGO = 'NULL' then '' else REGO end), '$') as [HubVehicleSid]

from 
(
	SELECT ALIAS,COMPANYNAME,FORWARDERID, NAME, REGO, REMOTEID, RECORDSOURCE,
	ROW_NUMBER() OVER (PARTITION BY [Rego] ORDER BY  RECORDSOURCE_PRIORITY) RN

	FROM (
			SELECT distinct ALIAS,COMPANYNAME,FORWARDERID, NAME, REGO, REMOTEID, 'Smartrak' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY
			from [EXT].[SMARTRAK_GETREMOTES]  WHERE RemoteId<>'RemoteId'		
			
			) JourneyUnion
			   ) JourneyWithrank
		where RN=1
		);