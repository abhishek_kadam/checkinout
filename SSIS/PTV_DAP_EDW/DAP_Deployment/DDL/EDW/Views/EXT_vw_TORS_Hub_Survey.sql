
/*****************************
Author :  Kiran Kumar
Modified Date : 03/09/2016
****************************/
IF OBJECT_ID('[EXT].[vw_TORS_Hub_Survey]', 'V') IS NOT NULL
  DROP View [EXT].[vw_TORS_Hub_Survey]


CREATE VIEW [EXT].[vw_TORS_Hub_Survey] AS (
SELECT DISTINCT
CASE WHEN [RawBatchID]= 'NULL' THEN NULL else [RawBatchID] end as [RawBatchID],
DAF.GenerateHashKey((CASE WHEN RawBatchID = 'NULL' THEN '' ELSE RawBatchID END),'$') AS Hub_ServeySid 
FROM 
(
SELECT DISTINCT RawBatchID From [EXT].[TORS_Bus] where RawBatchID<> 'RawBatchID'
Union All
SELECT DISTINCT RawBatchID From [EXT].[TORS_Train] where RawBatchID<> 'RawBatchID'
Union All
SELECT DISTINCT RawBatchID From [EXT].[TORS_Trams] where RawBatchID<> 'RawBatchID'
Union All
SELECT DISTINCT RawBatchID From [EXT].[TORS_SchoolBus] where RawBatchID<> 'RawBatchID'
)A
);