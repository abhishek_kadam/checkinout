﻿CREATE VIEW [EXT].[vw_GTFS_Sat_Calendar] AS (

SELECT 
CASE WHEN  SERVICE_ID ='NULL' then null else SERVICE_ID END SERVICE_ID, RECORDSOURCE,

[DAF].[GenerateHashKey]  (CASE WHEN SERVICE_ID = 'NULL' THEN '' ELSE SERVICE_ID END, '$')  [HubServiceSid],Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday, StartDate, EndDate

FROM (

		SELECT SERVICE_ID , RECORDSOURCE ,Monday ,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday, StartDate,EndDate,ROW_NUMBER() OVER (Partition by SERVICE_ID ORDER BY RECORDSOURCE_PRIORITY) RN  
			FROM ( 

				SELECT distinct SERVICE_ID,  'V/Line Train' as RECORDSOURCE, 1 RECORDSOURCE_PRIORITY, Monday ,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_1_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'

				UNION ALL

				SELECT distinct SERVICE_ID,  'Metro Train' as RECORDSOURCE,2 RECORDSOURCE_PRIORITY, Monday ,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate 
				FROM [EXT].[GTFS_2_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID' 

				UNION ALL
				SELECT distinct SERVICE_ID,  'Yarra Tram' as RECORDSOURCE, 3 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_3_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'Metro Bus' as RECORDSOURCE,4 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_4_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'Regional Bus' as RECORDSOURCE, 5 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_5_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'V/Line Coach' as RECORDSOURCE, 6 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_6_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'TeleBus' as RECORDSOURCE, 7 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_7_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'NightRider Bus' as RECORDSOURCE,8 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_8_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'Overland Train' as RECORDSOURCE, 10 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_10_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID'
				UNION ALL
				SELECT distinct SERVICE_ID,  'SkyBus' as RECORDSOURCE, 11 RECORDSOURCE_PRIORITY, Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Start_Date as StartDate,End_Date as EndDate
				FROM [EXT].[GTFS_11_Calendar] WHERE SERVICE_ID <> 'SERVICE_ID') ServiceUnion ) ServiceWithRank

		where RN=1
);