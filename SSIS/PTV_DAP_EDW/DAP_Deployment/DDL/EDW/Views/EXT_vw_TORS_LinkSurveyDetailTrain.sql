

/*****************************
Author :  Kiran Kumar
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[EXT].[vw_TORS_LinkSurveyDetailTrain]', 'V') IS NOT NULL
  DROP View [EXT].[vw_TORS_LinkSurveyDetailTrain]



CREATE VIEW [EXT].[vw_TORS_LinkSurveyDetailTrain] AS (
Select Distinct 
case WHEN RawBatchID = 'NULL' then null else RawBatchID end RawBatchID, -- Hub_Survey
case WHEN Entranceid = 'NULL' then null else Entranceid end Entranceid , -- Hub_StationEntrance
case WHEN SurveyorMykiNo1 = 'NULL' then null else SurveyorMykiNo1 end SurveyorMykiNo1, --Hub_smartcard
case WHEN SurveyDate = 'NULL' then null else SurveyDate end SurveyDate,
case WHEN SurveyorName1 = 'NULL' then null else SurveyorName1 end SurveyorName1 ,
case WHEN ActualSurveyStartTime = 'NULL' then null else ActualSurveyStartTime end ActualSurveyStartTime ,
case WHEN ActualSurveyFinishTime = 'NULL' then null else ActualSurveyFinishTime end ActualSurveyFinishTime,
case WHEN CountOn = 'NULL' then null else CountOn end CountOn,
case WHEN CountOff = 'NULL' then null else CountOff end CountOff,
case WHEN ShiftID = 'NULL' then null else ShiftID end ShiftID,
case WHEN JobNumber = 'NULL' then null else JobNumber end JobNumber,
case WHEN Comment = 'NULL' then null else Comment end Comment,
case WHEN Mode = 'NULL' then null else Mode end Mode,
case WHEN ServiceProvider = 'NULL' then null else ServiceProvider end ServiceProvider,
case WHEN SurveyorName2 = 'NULL' then null else SurveyorName2 end SurveyorName2,
case WHEN SurveyorMykiNo2 = 'NULL' then null else SurveyorMykiNo2 end SurveyorMykiNo2,


CAST(HASHBYTES('SHA1',
			                      case when RawBatchID ='NULL' then '' else RawBatchID end+   
								  case when Entranceid ='NULL' then '' else Entranceid end+ 
								 case when SurveyorMykiNo1 ='NULL' then '' else SurveyorMykiNo1 end+  
								 case when JobNumber ='NULL' then '' else JobNumber end+
								 case when SurveyDate ='NULL' then '' else SurveyDate end+
								 case when Mode ='NULL' then '' else Mode end+
								  case when CountOn ='NULL' then '' else CountOn end+
								  case when CountOff ='NULL' then '' else CountOff end
								  ) as Binary(20))
								    LinkSurveyDetailTrainSid 


from (

Select DISTINCT [Rawbatchid] ,
			[EntranceID],
			[SurveyorMykiNo1],
			[SurveyDate] , 
			[SurveyorName1] , 
			[ActualSurveyStartTime] , 
			[ActualSurveyFinishTime] , 
			[CountOn] , 
			[CountOff], 
			[ShiftID], 
			[JobNumber] , 
			[Comment] , 
			[Mode],
			[ServiceProvider], 
			[SurveyorName2], 
			[SurveyorMykiNo2]
		 from [EXT].[TORS_Train] where EntranceID<>'EntranceID'


)A
);