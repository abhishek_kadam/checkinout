
CREATE function [DAF].[GenerateHashKey]  

(
 @Businesskeylist  varchar(4000),
 @BusinessKeyDelimiter varchar(10)
 )

RETURNS binary(20)
as
BEGIN 
	--## delimiter is used to  separate and process individual business key separately if required 
	set @Businesskeylist= replace(REPLACE(UPPER(@Businesskeylist),' ',''),@BusinessKeyDelimiter,';')
	 
	return HASHBYTES('SHA1', @Businesskeylist)

END 