﻿/*****************************
Author : Kiran Kumar
Modified Date : 1/03/2016
****************************/
IF OBJECT_ID('[DV].[Hub_Entrance]', 'U') IS NOT NULL
   DROP TABLE [DV].[Hub_Entrance]
go

CREATE TABLE [DV].[Hub_Entrance] (
    [HubEntranceSid] binary(20) NULL, 
    [EntranceID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
