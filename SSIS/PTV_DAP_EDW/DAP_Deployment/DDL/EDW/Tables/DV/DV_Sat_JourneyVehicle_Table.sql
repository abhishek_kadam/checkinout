﻿/*****************************
Author : Prakash Shanmugam
Modified Date : 03/Mar/2016
****************************/

IF OBJECT_ID('[DV].[Sat_JourneyVehicle]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_JourneyVehicle]
GO

/* Create Tables */

CREATE TABLE [DV].[Sat_JourneyVehicle]
(
	[HubJourneyVehicleSid] binary(20) NULL,    -- This is the unique identifier of the Journey in the Data Vault.     
	[LoadDate] datetime2(7) NULL,    -- The Date and Time when the record was inserted into the Satellite
	[Origin] varchar(100) NULL,    -- Point of origin
	[Destination] varchar(100) NULL,    -- Destination description
	[Run] varchar(50) NULL,    -- The Run Block
	[IsServiceCancelled] varchar(1) NULL,    -- Service canceled Yes/NO
	[DepotName] varchar(100) NULL,    -- the description of the depot
	[Mode] varchar(50) NULL,    -- The description of the Mode
	[LoadEndDate] datetime2(7) NULL,    -- Date to when the record is valid
	[IsCurrentFlag] varchar(1) NULL,    -- Current Record Indicator
	[RecordSource] varchar(100) NULL    -- The source of the Data
)
GO