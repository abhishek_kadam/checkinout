﻿IF OBJECT_ID('[DV].[Hub_RemoteEvent]', 'U') IS NOT NULL
DROP TABLE [DV].[Hub_RemoteEvent]

/*****************************
Author :  Manoj Mayandi
Modified Date : 03/09/2016
****************************/

CREATE TABLE [DV].[Hub_RemoteEvent] (
    [HubRemoteEventSid] binary(20) NULL, 
    [RemoteEventID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RemoteEventDate] datetime2(7) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
