﻿/*****************************
Author : Prakash Shanmugam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[DV].[Sat_Vehicle]', 'U') IS NOT NULL
	DROP TABLE [DV].[Sat_Vehicle]

CREATE TABLE [DV].[Sat_Vehicle] (
    [HubVehicleSid] binary(20) NULL, 
    [Name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CompanyName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);