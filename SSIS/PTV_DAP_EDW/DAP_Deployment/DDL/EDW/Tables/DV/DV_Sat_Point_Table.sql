﻿/*****************************
Author : Prakash shanmugam
Modified Date : 4/03/2016
****************************/

IF OBJECT_ID('[DV].[Sat_Point]', 'U') IS NOT NULL
	DROP TABLE [DV].[Sat_Point]

CREATE TABLE [DV].[Sat_Point] (
    [HubLocationPointSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [PointName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PointType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Latitude] decimal(9, 7) NULL, 
    [Longitude] decimal(9, 6) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
