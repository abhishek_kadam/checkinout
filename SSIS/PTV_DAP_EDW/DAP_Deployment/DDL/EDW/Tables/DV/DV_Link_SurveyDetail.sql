/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[DV].[Link_SurveyDetail]', 'U') IS NOT NULL
                DROP TABLE [DV].[Link_SurveyDetail]
CREATE TABLE [DV].[Link_SurveyDetail] (
    [LinkSurveyDetailSid] binary(20) NULL, 
    [HubSurveySid] binary(20) NULL, 
    [HubSmartCardSid] binary(20) NULL, 
    [HubStopSid] binary(20) NULL, 
    [HubVehicleSid] binary(20) NULL, 
    [LoadDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);