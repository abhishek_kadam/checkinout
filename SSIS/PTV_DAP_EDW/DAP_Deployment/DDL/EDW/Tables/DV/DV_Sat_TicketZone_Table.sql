﻿/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[DV].[Sat_TicketZone]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_TicketZone]


CREATE TABLE [DV].[Sat_TicketZone] (
    [HubZoneSid] binary(20) NULL, 
    [TicketZoneID] int NULL, 
    [Zone] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZoneOrder] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);