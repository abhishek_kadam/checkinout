IF OBJECT_ID('[DV].[Hub_Zone]', 'U') IS NOT NULL
DROP TABLE [DV].[Hub_Zone]

/*****************************
Author :  Kiran
Modified Date : 03/09/2016
****************************/

CREATE TABLE [DV].[Hub_Zone] (
    [HubZoneSid] binary(20) NULL, 
    [ZoneID] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
