CREATE TABLE [DV].[Sat_CalendarDate] (
    [Date] date NULL, 
    [CalendarQuarter] int NULL, 
    [CalendarYear] int NULL, 
    [DayNameOfWeek] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DayOfMonth] int NULL, 
    [DayOfWeek] int NULL, 
    [DayOfYear] int NULL, 
    [LastDayOfMonthInd] bit NULL, 
    [MonthName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MonthOfYear] int NULL, 
    [WeekOfYear] int NULL, 
    [QuarterName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WeekEnding] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
