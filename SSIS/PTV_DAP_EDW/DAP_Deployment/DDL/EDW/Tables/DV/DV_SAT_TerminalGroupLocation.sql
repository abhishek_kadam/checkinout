CREATE TABLE [DV].[Sat_TerminalGroupLocation] (
    [HubTerminalGroupLocationSid] binary(20) NULL, 
    [ServiceLocationId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleTypeId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalGroupLocationDescription] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleRegistrationNumber] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
