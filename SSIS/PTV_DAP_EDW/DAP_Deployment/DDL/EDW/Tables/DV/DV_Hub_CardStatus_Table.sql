CREATE TABLE [DV].[Hub_CardStatus] (
    [HubCardStatusSid] binary(20) NULL, 
    [CardStatusId] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
