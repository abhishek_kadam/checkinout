CREATE TABLE [DV].[Hub_TransactionType] (
    [HubTransactionTypeSid] binary(20) NULL, 
    [TransactionTypeId] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
