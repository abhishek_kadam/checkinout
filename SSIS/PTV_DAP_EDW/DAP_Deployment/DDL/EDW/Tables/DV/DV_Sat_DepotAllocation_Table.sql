﻿/*****************************
Author : Vikram
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[DV].[Sat_DepotAllocation]', 'U') IS NOT NULL
                DROP TABLE [DV].[Sat_DepotAllocation]


CREATE TABLE [DV].[Sat_DepotAllocation] (
    [HubDepotAllocationSid] binary(20) NULL, 
    [DepotAllocationDate] datetime2(7) NULL, 
    [ServicesAllocatedCount] int NULL, 
    [ServicesLoadedCount] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
