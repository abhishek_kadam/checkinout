/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[DV].[Link_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [DV].[Link_TouchOnTransaction]

CREATE TABLE [DV].[Link_TouchOnTransaction] (
    [LinkTouchOnTransactionSid] binary(20) NULL, 
    [TransactionDate] date NULL, 
    [TransactionTime] time(7) NULL, 
    [HubStopSid] binary(20) NULL, 
    [HubSmartCardSid] binary(20) NULL, 
    [HubFareProductSid] binary(20) NULL, 
    [HubRouteSid] binary(20) NULL, 
    [HubTerminalSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
