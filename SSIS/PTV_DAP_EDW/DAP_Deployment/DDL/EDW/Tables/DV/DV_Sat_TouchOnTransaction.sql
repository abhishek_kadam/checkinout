/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[DV].[Sat_TouchOnTransaction]', 'U') IS NOT NULL
                DROP TABLE [DV].[Sat_TouchOnTransaction]

CREATE TABLE [DV].[Sat_TouchOnTransaction] (
    [LinkTouchOnTransactionSid] binary(20) NULL, 
    [RouteStopSequenceNum] int NULL, 
    [EntryLocationId] int NULL, 
    [ProcessingDate] datetime2(7) NULL, 
    [ProductSerialNumber] int NULL, 
    [ServiceProviderId] int NULL, 
    [StopLocationId] int NULL, 
    [ZoneMax] int NULL, 
    [ZoneMin] int NULL, 
    [RouteStopId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TicketingBusinessDate] date NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
