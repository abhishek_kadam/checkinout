﻿/*****************************
Author : Manoj
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[DV].[Sat_ServiceCalendar]', 'U') IS NOT NULL
	DROP TABLE [DV].[Sat_ServiceCalendar] 


CREATE TABLE [DV].[Sat_ServiceCalendar] (
    [HubServiceSid] binary(20) NULL, 
    [Monday] bit NULL, 
    [Tuesday] bit NULL, 
    [Wednesday] bit NULL, 
    [Thursday] bit NULL, 
    [Friday] bit NULL, 
    [Saturday] bit NULL, 
    [Sunday] bit NULL, 
    [StartDate] datetime2(7) NULL, 
    [EndDate] datetime2(7) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);