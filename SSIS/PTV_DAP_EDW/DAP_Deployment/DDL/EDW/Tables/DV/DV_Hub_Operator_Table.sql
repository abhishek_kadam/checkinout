/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[DV].[Hub_Operator]', 'U') IS NOT NULL
   DROP TABLE [DV].[Hub_Operator]

CREATE TABLE [DV].[Hub_Operator] (
    [Hub_OperatorSid] binary(20) NULL, 
    [OperatorID] varchar(100) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
