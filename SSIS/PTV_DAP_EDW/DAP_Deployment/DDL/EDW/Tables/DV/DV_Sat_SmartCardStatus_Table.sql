/*****************************
Author : Usha Rana
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[DV].[Sat_SmartCardStatus]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_SmartCardStatus]


CREATE TABLE [DV].[Sat_SmartCardStatus] (
    [HubSmartCardSid] binary(20) NULL, 
    [ATPYEnableBit] int NULL, 
    [CardStatusDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardStatuslId] int NULL, 
    [CardRegInd] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
