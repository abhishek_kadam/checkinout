CREATE TABLE [DV].[Sat_FinancialDate] (
    [Date] date NULL, 
    [FinancialMonth] int NULL, 
    [FinancialQuarter] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FinancialMonthName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FinancialMonthSequence] int NULL, 
    [FinancialYear] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
