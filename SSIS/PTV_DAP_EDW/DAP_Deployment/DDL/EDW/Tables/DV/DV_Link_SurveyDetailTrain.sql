/*****************************
Author :  Kiran Kumar
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[DV].[Link_SurveyDetailTrain]', 'U') IS NOT NULL
                DROP TABLE [DV].[Link_SurveyDetailTrain]


CREATE TABLE [DV].[Link_SurveyDetailTrain] (
    [LinkSurveyDetailTrainSid] binary(20) NULL, 
    [HubSurveySid] binary(20) NULL, 
    [HubSmartCardSid] binary(20) NULL, 
    [HubEntranceSid] binary(20) NULL, 
    [LoadDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);