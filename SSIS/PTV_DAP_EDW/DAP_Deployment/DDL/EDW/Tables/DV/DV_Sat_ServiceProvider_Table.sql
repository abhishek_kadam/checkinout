﻿/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[DV].[Sat_ServiceProvider]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_ServiceProvider]


CREATE TABLE [DV].[Sat_ServiceProvider] (
    [HubServiceProviderSid] binary(20) NULL, 
    [BusinessType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EffectiveDate] datetime2(7) NULL, 
    [IssuerInd] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadAgentPPInd] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadAgentInd] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorInd] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OverrideFundCollectInd] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SettlementReportType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProviderGrpDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProviderGrpId] int NULL, 
    [ServiceProviderName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProviderShortname] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)