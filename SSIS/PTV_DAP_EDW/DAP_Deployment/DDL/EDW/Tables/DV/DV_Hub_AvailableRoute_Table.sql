/*****************************
Author : Prakash shanmugam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[DV].[Hub_AvailableRoute]', 'U') IS NOT NULL
	DROP TABLE [DV].[Hub_AvailableRoute]

CREATE TABLE [DV].[Hub_AvailableRoute] (
    [HubAvailableRouteSid] binary(20) NULL, 
    [AvailableRouteID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
