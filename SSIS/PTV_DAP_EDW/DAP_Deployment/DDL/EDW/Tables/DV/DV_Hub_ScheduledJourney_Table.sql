/*****************************
Author :  Vikram
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[DV].[Hub_ScheduledJourney]', 'U') IS NOT NULL
                DROP TABLE [DV].[Hub_ScheduledJourney]


CREATE TABLE [DV].[Hub_ScheduledJourney] (
    [HubScheduledJourneySid] binary(20) NULL, 
    [ScheduledJourneyID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
