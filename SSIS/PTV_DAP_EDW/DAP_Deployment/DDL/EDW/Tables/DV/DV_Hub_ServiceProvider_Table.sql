
/*****************************
Author : Kiran Kumar
Modified Date : 2/24/2016
****************************/
IF OBJECT_ID('[DV].[Hub_ServiceProvider]', 'U') IS NOT NULL
   DROP TABLE [DV].[Hub_ServiceProvider]

CREATE TABLE [DV].[Hub_ServiceProvider] (
    [HubServiceProviderSid] binary(20) NULL, 
    [ServiceProviderId] int  NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
