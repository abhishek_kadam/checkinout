﻿IF OBJECT_ID('[DV].[Sat_ServiceException]', 'U') IS NOT NULL
DROP TABLE [DV].[Sat_ServiceException]

/*****************************
Author		: Manoj Mayandi
Create Date : 03/09/2016
****************************/

CREATE TABLE [DV].[Sat_ServiceException] (
    [HubServiceSid] binary(20) NULL, 
    [date] date NULL, 
    [exception_type] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);