IF OBJECT_ID('[DV].[Hub_Stop]', 'U') IS NOT NULL
DROP TABLE [DV].[Hub_Stop]

/*****************************
Author :  Ramakrishna
Modified Date : 03/09/2016
****************************/

CREATE TABLE [DV].[Hub_Stop] (
    [HubStopSid ] binary(20) NULL, 
    [StopID] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
