﻿/*****************************
Author : Prakash Shanmugam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[DV].[Sat_StopMode]', 'U') IS NOT NULL
	DROP TABLE [DV].[Sat_StopMode]

CREATE TABLE [DV].[Sat_StopMode] (
    [HubModeSid] binary(20) NULL, 
    [StopModeName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
