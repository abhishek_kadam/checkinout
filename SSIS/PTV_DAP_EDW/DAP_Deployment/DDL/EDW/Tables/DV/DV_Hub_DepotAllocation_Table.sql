/*****************************
Author :  Vikram

Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[DV].[Hub_DepotAllocation]', 'U') IS NOT NULL
                DROP TABLE [DV].[Hub_DepotAllocation]


CREATE TABLE [DV].[Hub_DepotAllocation] (
    [Hub_DepotAllocationSid] binary(20) NULL, 
    [Depot] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LastUpdateDate] datetime2(7) NULL, 
    [Line] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Operator] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
