CREATE TABLE [DV].[Hub_TerminalStatus] (
    [HubTerminalStatusSid] binary(20) NULL, 
    [TerminalStatusId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
