﻿ 
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[DV].[Sat_PaymentType]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_PaymentType]


CREATE TABLE [DV].[Sat_PaymentType] (
    [HubPaymentTypeSid] binary(20) NULL, 
    [PaymentTypeDescription] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)