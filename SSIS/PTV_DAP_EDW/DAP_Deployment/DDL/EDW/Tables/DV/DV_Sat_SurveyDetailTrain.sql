/*****************************
Author :  Kiran Kumar
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[DV].[Sat_SurveyDetailTrain]', 'U') IS NOT NULL
                DROP TABLE [DV].[Sat_SurveyDetailTrain]

CREATE TABLE [DV].[Sat_SurveyDetailTrain] (
    [LinkSurveyDetailTrainSid] binary(20) NULL, 
    [SurveyDate] date NULL, 
    [SurveyorName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountStartTime] time(7) NULL, 
    [CountEndTime] time(7) NULL, 
    [Mode] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountOn] int NULL, 
    [CountOff] int NULL, 
    [Shift] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [JobNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Comment] varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProvider] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SecondSurveyorName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SecondSurveyorMyki] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadEndDate] datetime2(7) NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);