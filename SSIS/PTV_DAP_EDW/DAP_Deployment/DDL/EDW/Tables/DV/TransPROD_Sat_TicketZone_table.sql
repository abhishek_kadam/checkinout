﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[TransPROD_Sat_TicketZone]', 'U') IS NOT NULL
   DROP TABLE [STG].[TransPROD_Sat_TicketZone]


CREATE TABLE [STG].[TransPROD_Sat_TicketZone] (
    [TicketZoneID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZoneDesc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZoneOrder] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
