IF OBJECT_ID('[DV].[Hub_LocationPoint]', 'U') IS NOT NULL
DROP TABLE [DV].[Hub_LocationPoint]

/*****************************
Author :  Kiran
Modified Date : 03/09/2016
****************************/

CREATE TABLE [DV].[Hub_LocationPoint] (
    [Hub_LocationPointSid] binary(20) NULL, 
    [LocationPointID] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
