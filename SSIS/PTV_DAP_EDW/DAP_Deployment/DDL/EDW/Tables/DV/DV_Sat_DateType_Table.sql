﻿/*****************************
Author : Abhishek Kadam
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[DV].[Sat_DateType]', 'U') IS NOT NULL
   DROP TABLE [DV].[Sat_DateType]

CREATE TABLE DV.[Sat_DateType]
(
	[Date] date NULL,    
	[DayTypeCategory] varchar(50) NULL,    
	[DayType] varchar(50) NULL,    
	[ABSWeek] int NULL,   
	[LoadDate] datetime2(7) NULL,  
	[LoadEndDate] datetime2(7) NULL,   
	[IsCurrentFlag] varchar(1) NULL,   
	[RecordSource] varchar(100) NULL    
)