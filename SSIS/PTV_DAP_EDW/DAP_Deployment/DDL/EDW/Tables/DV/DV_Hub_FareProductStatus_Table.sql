/*****************************
Author : Abhishek Kadam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[DV].[Hub_FareProductStatus]', 'U') IS NOT NULL
	DROP TABLE [DV].[Hub_FareProductStatus]

CREATE TABLE [DV].[Hub_FareProductStatus] (
    [HubFareProductStatusSid] binary(20) NULL, 
    [FareProductStatusId] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
