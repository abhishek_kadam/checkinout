﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_StopMode]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_StopMode]

CREATE TABLE [STG].[TRANSPROD_Sat_StopMode] (
    [StopModeId] int NOT NULL, 
    [StopModeName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
