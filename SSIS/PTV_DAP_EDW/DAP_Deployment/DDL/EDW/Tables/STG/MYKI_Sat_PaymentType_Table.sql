﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_PaymentType]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_PaymentType]


CREATE TABLE [STG].[MYKI_Sat_PaymentType] (
    [PaymentTypeId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PaymentTypeDescription] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
 