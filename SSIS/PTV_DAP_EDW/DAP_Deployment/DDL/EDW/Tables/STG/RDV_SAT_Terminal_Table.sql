﻿/*****************************
Author : Swathi Reddy
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_Terminal]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Sat_Terminal]
CREATE TABLE [STG].[RDV_Sat_Terminal] (
    [HubTerminalSid] binary(20) NULL, 
    [CreatedDate] datetime2(7) NULL, 
    [PositionId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalGroupLocationDescription] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalPosition] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalStatusDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalStatusId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalSubgroupLocationDescription] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalSubgroupLocationIdentifier] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
