﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[TRANSPROD_Sat_Operator]', 'U') IS NOT NULL
   DROP TABLE [STG].[TRANSPROD_Sat_Operator]

CREATE TABLE [STG].[TRANSPROD_Sat_Operator] (
    [Operatorid] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BusinessName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TradingName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GroupName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetAddress] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorMode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PostCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DataSystem] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
