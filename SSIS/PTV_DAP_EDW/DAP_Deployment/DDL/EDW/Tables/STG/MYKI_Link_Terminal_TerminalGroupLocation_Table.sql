﻿/*****************************
Author : Vijender Thakur
Modified Date : 2/24/2016
****************************/

IF OBJECT_ID('[STG].[MYKI_Link_Terminal_TerminalGroupLocation]', 'U') IS NOT NULL
	DROP TABLE [STG].[MYKI_Link_Terminal_TerminalGroupLocation]

CREATE TABLE [STG].[MYKI_Link_Terminal_TerminalGroupLocation] (
    [TERMINAL_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_GROUP_LOC_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [ExtractDateTime] datetime NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);

