﻿/*****************************
Author : Anoop
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_TransactionType]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Sat_TransactionType]

CREATE TABLE [STG].[RDV_Sat_TransactionType] (
    [HubTransactionTypeSid] binary(20) NULL, 
    [TransactionTypeDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);