/*****************************
Author : Usha Rana
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[RDV_SAT_SmartCardStatus]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_SAT_SmartCardStatus]

CREATE TABLE [STG].[RDV_Sat_SmartCardStatus] (
    [HubSmartCardSid] binary(20) NULL, 
    [ATPYEnableBit] int NULL, 
    [CardStatusDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardStatuslId] int NULL, 
    [CardRegInd] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)