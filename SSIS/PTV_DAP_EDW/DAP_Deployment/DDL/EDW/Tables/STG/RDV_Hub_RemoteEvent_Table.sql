﻿IF OBJECT_ID('[STG].[RDV_Hub_RemoteEvent]', 'U') IS NOT NULL
DROP TABLE [STG].[RDV_Hub_RemoteEvent]

/*****************************
Author :  Manoj Mayandi
Modified Date : 03/09/2016
****************************/

CREATE TABLE [STG].[RDV_Hub_RemoteEvent] (
    [HubRemoteEventSid] binary(20) NULL, 
    [RemoteEventID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RemoteEventDate] datetime2(7) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
