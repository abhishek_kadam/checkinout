﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_Operator]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Sat_Operator]

CREATE TABLE [STG].[RDV_Sat_Operator] (
    [HubOperatorSid] binary(20) NULL, 
    [OperatorName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BusinessName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TradingName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GroupName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetAddress] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorMode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PostCode] varchar(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DataSystem] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)





