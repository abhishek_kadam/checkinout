﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_ParentRoute]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_ParentRoute]

CREATE TABLE [STG].[RDV_Sat_ParentRoute] (
    [HubParentRouteSid] binary(20) NULL, 
    [LineDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineDescLong] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EffectiveDate] datetime2(7) NULL, 
    [TermDate] datetime2(7) NULL, 
    [LineNotes] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PublicDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ReducedHolidayService] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopTemplate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccess] bit NULL, 
    [WheelChairAccessSat] bit NULL, 
    [WheelChairAccessSun] bit NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL,
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
