﻿/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_Smartcard]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_Smartcard]


CREATE TABLE [STG].[MYKI_Sat_Smartcard] (
    [CARD_SURFACE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_EXPIRY_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_PAN] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_PHYSICAL_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_SALE_LOAD_AGENT_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_TYPE_DESC] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_TYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_SUB_TYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_SUB_TYPE_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [INIT_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ORDER_REF_LINE_NO] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ORDER_REF_NO] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PASS_TYPE_DDA_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PASS_TYPE_PERS_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FARE_PRODUCT_TYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)