/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Link_CardFinancialTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Link_CardFinancialTransaction]

CREATE TABLE [STG].[RDV_Link_CardFinancialTransaction] (
    [LinkCardFinancialTransactionSid] binary(20) NULL, 
    [TransactionDate] date NULL, 
    [TransactionTime] time(7) NULL, 
    [HubSmartCardSid] binary(20) NULL, 
    [HubFareproductSid] binary(20) NULL, 
    [HubTerminalSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
