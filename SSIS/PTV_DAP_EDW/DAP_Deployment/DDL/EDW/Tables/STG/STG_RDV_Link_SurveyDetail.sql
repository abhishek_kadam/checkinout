/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Link_SurveyDetail] ', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Link_SurveyDetail] 
CREATE TABLE [STG].[RDV_Link_SurveyDetail] (
    [Link_SurveyDetailSid] binary(20) NULL, 
    [Hub_SurveySid] binary(20) NULL, 
    [Hub_SmartCardSid] binary(20) NULL, 
    [Hub_StopSid] binary(20) NULL, 
    [Hub_VehicleSid] binary(20) NULL, 
    [LoadDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);