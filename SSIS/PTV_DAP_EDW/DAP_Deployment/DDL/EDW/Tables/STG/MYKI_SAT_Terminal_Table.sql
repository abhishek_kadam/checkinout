﻿ /*****************************
Author : Swathi Reddy
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_Terminal]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_Terminal]

CREATE TABLE [STG].[MYKI_Sat_Terminal] (
    [CREATEDDATE] datetime NULL, 
    [POSITION_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_GROUP_LOC_DESCRIPTION] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_POSITION] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_STATUS_DESCRIPTION] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_STATUS_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_SUBGROUP_LOC_DESCRIPTION] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_SUBGROUP_LOC_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_TYPE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [TERMINAL_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);