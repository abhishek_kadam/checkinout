/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_SurveyDetail]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Sat_SurveyDetail]
CREATE TABLE [STG].[RDV_Sat_SurveyDetail] (
    [LinkSurveyDetailSid] binary(20) NULL, 
    [SurveyDate] date NULL, 
    [SurveyorName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountStartTime] time(7) NULL, 
    [CountEndTime] time(7) NULL, 
    [Time] time(7) NULL, 
    [CountOn] int NULL, 
    [CountOff] int NULL, 
    [AlreadyOn] int NULL, 
    [RemainedOn] int NULL, 
    [Sequence] int NULL, 
    [Shift] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [JobNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Door] int NULL, 
    [Comment] varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProvider] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);