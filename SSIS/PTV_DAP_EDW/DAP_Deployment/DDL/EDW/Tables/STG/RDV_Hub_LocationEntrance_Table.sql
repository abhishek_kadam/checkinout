/*****************************
Author : Vikram
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Hub_LocationEntrance] ', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Hub_LocationEntrance] 


CREATE TABLE [STG].[RDV_Hub_LocationEntrance] (
    [Hub_LocationEntranceSid] binary(20) NULL, 
    [EntranceID] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
