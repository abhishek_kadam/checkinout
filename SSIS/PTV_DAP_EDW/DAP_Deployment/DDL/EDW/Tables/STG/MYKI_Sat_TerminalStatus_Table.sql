﻿/*****************************
Author : Anoop
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[[MYKI_Sat_TerminalStatus]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_TerminalStatus]

CREATE TABLE [STG].[MYKI_Sat_TerminalStatus] (
    [TerminalStatusID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TerminalStatusDesc] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);