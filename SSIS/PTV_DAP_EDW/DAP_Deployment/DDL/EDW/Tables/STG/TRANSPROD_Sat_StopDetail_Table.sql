﻿
/*****************************
Author : Ramakrishna
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_StopDetail]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_StopDetail]

CREATE TABLE [STG].[TRANSPROD_Sat_StopDetail] (
    [MetLinkID] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PrimaryStopName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SecondStopName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetAddress] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SuburbID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BayNbr] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LowFloorAccess] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLong] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimetableCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PlatformNbr] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TrainNbrPlatforms] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TrainTimeTable] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StationName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopLandmark] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopSpecName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
