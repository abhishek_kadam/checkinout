﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_Route]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_Route]

CREATE TABLE [STG].[RDV_Sat_Route] (
    [HubRouteSid] binary(20) NULL, 
    [DirectionCode] int NULL, 
    [DescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DirectionDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineFrom] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineTo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineVia] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
