/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[TORS_Link_SurveyDetail]', 'U') IS NOT NULL
                DROP TABLE [STG].[TORS_Link_SurveyDetail]
CREATE TABLE [STG].[TORS_Link_SurveyDetail] (
    [LinkSurveyDetailSid] binary(20) NULL, 
    [RawBatchID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardSurfaceId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SurveyDate] date NULL, 
    [SurveyorName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountStartTime] time(7) NULL, 
    [CountEndTime] time(7) NULL, 
    [Time] time(7) NULL, 
    [CountOn] int NULL, 
    [CountOff] int NULL, 
    [AlreadyOn] int NULL, 
    [RemainedOn] int NULL, 
    [Sequence] int NULL, 
    [Shift] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [JobNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Door] int NULL, 
    [Comment] varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProvider] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [ExtractDateTime] datetime NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
