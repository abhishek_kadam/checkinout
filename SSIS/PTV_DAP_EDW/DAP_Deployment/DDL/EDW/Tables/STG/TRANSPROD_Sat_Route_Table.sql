﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_Route]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_Route]

CREATE TABLE [STG].[TRANSPROD_Sat_Route] (
    [LineID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DirectionCode] int NULL, 
    [DescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DirectionDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineFrom] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineTo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineVia] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
