﻿IF OBJECT_ID('[STG].[TRANSPROD_Sat_ServiceDetail]', 'U') IS NOT NULL
DROP TABLE [STG].[TRANSPROD_Sat_ServiceDetail]

/*****************************
Author : Ramakrishna
Modified Date : 03/09/2016
****************************/

CREATE TABLE [STG].[TRANSPROD_Sat_ServiceDetail] (
    [ServiceID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceNbr] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DMon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DTue] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DWed] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DThur] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DFri] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DSat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DSun] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
