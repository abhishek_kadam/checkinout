﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_Entrance]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_Entrance]

CREATE TABLE [STG].[RDV_Sat_Entrance] (
    [HubLocationEntranceSid] binary(20) NULL, 
    [POIEntranceID] int NULL, 
    [Latitude] decimal(15, 12) NULL, 
    [Longitude] decimal(15, 12) NULL, 
    [StreetNumber] nvarchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
