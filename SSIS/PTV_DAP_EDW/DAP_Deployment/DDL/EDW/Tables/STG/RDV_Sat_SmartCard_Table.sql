﻿/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_SmartCard]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Sat_SmartCard]


CREATE TABLE [STG].[RDV_Sat_SmartCard] (
    [HubSmartCardSid] binary(20) NULL, 
    [CardExpiryDate] datetime2(7) NULL, 
    [CardPhysicalId] bigint NULL, 
    [CardPAN] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardSaleLoadAgentId] int NULL, 
    [CardSubTypeDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardSubTypeId] int NULL, 
    [CardTypeDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardTypeId] int NULL, 
    [InitDate] datetime2(7) NULL, 
    [OrderRefLineNo] int NULL, 
    [OrderReferenceNo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardDDAInd] int NULL, 
    [CardPersonlisedInd] int NULL, 
    [DSCFareProductTypeId] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)