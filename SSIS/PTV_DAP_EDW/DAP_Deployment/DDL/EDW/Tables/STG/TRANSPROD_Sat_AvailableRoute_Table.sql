﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_AvailableRoute]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_AvailableRoute]

CREATE TABLE [STG].[TRANSPROD_Sat_AvailableRoute] (
    [RouteID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteFrom] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteTo] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteVia] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccess] bit NULL, 
    [Myki] bit NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
