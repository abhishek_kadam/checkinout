﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_ParentRoute]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_ParentRoute]

CREATE TABLE [STG].[TRANSPROD_Sat_ParentRoute] (
    [LineMainID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineDescLong] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EffectiveDate] datetime2(7) NULL, 
    [TermDate] datetime2(7) NULL, 
    [LineNotes] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PublicDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ReducedHolServ] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopTTtemplate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccess] bit NULL, 
    [WheelChairAccessSat] bit NULL, 
    [WheelChairAccessSun] bit NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);

