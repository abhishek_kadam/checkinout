﻿/*****************************
Author : Prakash Shanmugam
Modified Date : 03/Mar/2016
****************************/

IF OBJECT_ID('[STG].[SMARTRAK_Sat_JourneyVehicle]', 'U') IS NOT NULL
   DROP TABLE [STG].[SMARTRAK_Sat_JourneyVehicle]
GO

/* Create Tables */

CREATE TABLE [STG].[SMARTRAK_Sat_JourneyVehicle]
(	
	[ServiceId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[RemoteId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Direction] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ParentRoute] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OperatingDay] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 	
	[Origin] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- Point of origin
	[Destination] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- Destination description
	[Run] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- The Run Block
	[IsServiceCancelled] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- Service canceled Yes/NO
	[DepotName] varchar(100)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- the description of the depot
	[Mode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    -- The description of the Mode		
	[RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO