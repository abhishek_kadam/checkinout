﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[GTFS_Sat_Shape]', 'U') IS NOT NULL
   DROP TABLE [STG].[GTFS_Sat_Shape]


CREATE TABLE [STG].[GTFS_Sat_Shape] (
    [shape_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_sequence] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_dist_traveled] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
