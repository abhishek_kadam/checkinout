﻿/*****************************
Author : Kiran Kumar
Modified Date : 1/03/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Hub_Survey]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Hub_Survey]



CREATE TABLE [STG].[RDV_Hub_Survey] (
    [Hub_SurveySid] binary(20) NULL, 
    [Survey_RawBatchID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);

