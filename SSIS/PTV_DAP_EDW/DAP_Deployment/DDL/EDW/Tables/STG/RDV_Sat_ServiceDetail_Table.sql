﻿IF OBJECT_ID('[STG].[RDV_Sat_ServiceDetail]', 'U') IS NOT NULL
DROP TABLE [STG].[RDV_Sat_ServiceDetail]

/*****************************
Author :  Ramakrishna
Modified Date : 03/09/2016
****************************/

CREATE TABLE [STG].[RDV_Sat_ServiceDetail] (
    [HubServiceSid] binary(20) NULL, 
    [ServiceNbr] nvarchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DMon] bit NULL, 
    [DTue] bit NULL, 
    [DWed] bit NULL, 
    [DThur] bit NULL, 
    [DFri] bit NULL, 
    [DSat] bit NULL, 
    [DSun] bit NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);


