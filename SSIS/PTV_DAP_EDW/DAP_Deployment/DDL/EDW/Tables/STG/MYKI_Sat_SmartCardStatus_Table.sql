/*****************************
Author : Usha Rana
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[MYKI_SAT_SmartCardStatus]', 'U') IS NOT NULL
	DROP TABLE [STG].[MYKI_SAT_SmartCardStatus]

CREATE TABLE [STG].[MYKI_Sat_SmartCardStatus] (
    [CARD_SURFACE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ATPYEnableBit] int NULL, 
    [CardStatusDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CardStatuslId] int NULL, 
    [CardRegInd] int NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)