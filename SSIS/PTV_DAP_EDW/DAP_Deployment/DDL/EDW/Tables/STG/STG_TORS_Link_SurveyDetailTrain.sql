
/*****************************
Author :  Kiran Kumar
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[STG].[TORS_Link_SurveyDetailTrain]', 'U') IS NOT NULL
                DROP TABLE [STG].[TORS_Link_SurveyDetailTrain]


CREATE TABLE [STG].[TORS_Link_SurveyDetailTrain] (
    [Link_SurveyDetailTrainSid] binary(20) NULL, 
    [RawBatchID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EntranceID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SurveyorMykiNo1] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [ExtractDateTime] datetime NULL, 
    [SurveyDate] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SurveyorName1] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ActualSurveyStartTime] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ActualSurveyFinishTime] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountOn] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CountOff] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ShiftID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [JobNumber] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Comment] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Mode] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProvider] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SurveyorName2] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SurveyorMykiNo2] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);