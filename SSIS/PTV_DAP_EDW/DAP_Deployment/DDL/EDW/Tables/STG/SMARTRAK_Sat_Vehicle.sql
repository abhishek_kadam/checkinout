﻿/*****************************
Author : Prakashs
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[SMARTRAK_Sat_Vehicle]', 'U') IS NOT NULL
	DROP TABLE [STG].[SMARTRAK_Sat_Vehicle]

CREATE TABLE [STG].[SMARTRAK_Sat_Vehicle] (
    [Alias] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CompanyName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ForwarderId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Rego] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RemoteId] int NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
