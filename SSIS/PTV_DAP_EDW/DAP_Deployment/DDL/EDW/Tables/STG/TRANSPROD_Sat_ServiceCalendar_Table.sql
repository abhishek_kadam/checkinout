﻿
/*****************************
Author : Manoj
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_ServiceCalendar]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_ServiceCalendar]

CREATE TABLE [STG].[TRANSPROD_Sat_ServiceCalendar] (
    [service_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [monday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [tuesday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [wednesday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [thursday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [friday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [saturday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [sunday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [start_date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [end_date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
