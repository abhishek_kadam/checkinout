﻿/*****************************
Author : Vijender Thakur
Modified Date : 2/24/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Link_ServiceLocation_ServiceProvider]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Link_ServiceLocation_ServiceProvider]

CREATE TABLE [STG].[RDV_Link_ServiceLocation_ServiceProvider] (
    [LinkServiceLocationServiceProviderSid] binary(20) NULL, 
    [HubServiceLocationSid] binary(20) NULL, 
    [HubServiceProviderSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
