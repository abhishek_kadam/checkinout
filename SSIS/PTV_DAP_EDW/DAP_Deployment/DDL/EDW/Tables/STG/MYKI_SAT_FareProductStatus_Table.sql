﻿/*****************************
Author : Abhishek Kadam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[MYKI_SAT_FareProductStatus]', 'U') IS NOT NULL
	DROP TABLE [STG].[MYKI_SAT_FareProductStatus]

CREATE TABLE [STG].[MYKI_Sat_FareProductStatus](
    [FareProductStatusId] int NULL, 
	[FareProductStatusDesc] [varchar](50) NULL,
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)