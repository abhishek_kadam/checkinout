/*****************************
Author :  Anoopraj
Modified Date : 03/09/2016
****************************/

IF OBJECT_ID('[STG].[TORS_Sat_StationEntrance]', 'U') IS NOT NULL
                DROP TABLE [STG].[TORS_Sat_StationEntrance]
CREATE TABLE [STG].[TORS_Sat_StationEntrance] (
    [EntranceID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EntranceName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);