﻿/*****************************
Author : PrakashS
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[GTFS_Sat_Headsign]', 'U') IS NOT NULL
	DROP TABLE [STG].[GTFS_Sat_Headsign]

CREATE TABLE [STG].[GTFS_Sat_Headsign] (
    [Route_Id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Service_Id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Trip_Id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Shape_Id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Trip_Headsign] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Direction_Id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
