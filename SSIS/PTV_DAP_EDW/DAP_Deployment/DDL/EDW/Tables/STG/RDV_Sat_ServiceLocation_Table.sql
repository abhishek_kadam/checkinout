﻿/*****************************
Author : Asha Shridhara
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_ServiceLocation]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_ServiceLocation]

CREATE TABLE [STG].[RDV_Sat_ServiceLocation] (
    [HubServiceLocationSid] binary(20) NULL, 
    [CoverageName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [InventLocationId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LocationTypeDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LocationTypeId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [NtsZoneId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceLocationDescription] varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceProviderId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceLocName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceLocShortname] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
