﻿/*****************************
Author : Abhishek Kadam
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[RDV_SAT_FareProductStatus]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_SAT_FareProductStatus]

CREATE TABLE [STG].[RDV_SAT_FareProductStatus](
	[HubFareProductStatusSid] [binary](20) NULL,
	[FareProductStatusDesc] [varchar](50) NULL,
	[LoadDate] [datetime2](7) NULL,
	[LoadEndDate] [datetime2](7) NULL,
	[IsCurrentFlag] [varchar](1) NULL,
	[RecordSource] [varchar](100) NULL,
	[RunGroupId] [int] NULL,
	[ProcessStatus] [varchar](50) NULL,
	[UpdateStatus] [varchar](50) NULL
)