﻿/*****************************
Author : Prakash
Modified Date : 4/03/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_Point]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_Point]

CREATE TABLE [STG].[RDV_Sat_Point] (
    [HubLocationPointSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [PointName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PointType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Latitude] decimal(9, 7) NULL, 
    [Longitude] decimal(9, 6) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);