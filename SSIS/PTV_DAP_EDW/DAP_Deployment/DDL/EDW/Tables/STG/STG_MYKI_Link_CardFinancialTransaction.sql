/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[MYKI_Link_CardFinancialTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[MYKI_Link_CardFinancialTransaction]

CREATE TABLE [STG].[MYKI_Link_CardFinancialTransaction] (
    [LinkCardFinancialTransactionSid] binary(20) NULL, 
    [SETTLEMENT_DATE_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_TYPE_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_DATE_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_TIME_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_TXN_SEQ_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PRODUCT_SERIAL_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_PROD_SEQ_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PAYMENT_TYPE_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ENTRY_POINT_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GST_AMT] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GST_RATE] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TPURSE_AMT] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REFUND_FEE_AMT] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZONE_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LINE_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ACTIONLIST_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BUS_STOP] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DSC_CARD_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LLSC_CARD_KEY] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LLSC_TPURSE_LOAD_CSN] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_TIMESTAMP] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ExtractDateTime] datetime NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
