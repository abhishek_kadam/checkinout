﻿/*****************************
Author : Prakash
Modified Date : 4/03/2016
****************************/

IF OBJECT_ID('[STG].[SMARTRAK_Sat_Point]', 'U') IS NOT NULL
	DROP TABLE [STG].[SMARTRAK_Sat_Point]

CREATE TABLE [STG].[SMARTRAK_Sat_Point] (
    [PointId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PointName] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PointType] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Latitude] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Longitude] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
