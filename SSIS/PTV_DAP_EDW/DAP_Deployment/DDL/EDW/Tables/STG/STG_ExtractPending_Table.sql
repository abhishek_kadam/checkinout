CREATE TABLE [STG].[ExtractPending] (
    [SourceEntityID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [RunGroupID] int NOT NULL, 
    [SourceBusinessID1] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [SourceBusinessID2] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SourceBusinessID3] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SourceBusinessID4] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SourceBusinessID5] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CreatedBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [CreatedDateTime] datetime NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
