﻿
/*****************************
Author : Ramakrishna
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_StopZone]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_StopZone] 

CREATE TABLE [STG].[TRANSPROD_Sat_StopZone] (
    [ZoneStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TicketZoneID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MetlinkStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);