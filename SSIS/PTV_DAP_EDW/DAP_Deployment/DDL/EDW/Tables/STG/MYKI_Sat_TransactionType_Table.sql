﻿/*****************************
Author : Anoop
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_TransactionType]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_TransactionType]

CREATE TABLE [STG].[MYKI_Sat_TransactionType] (
    [TransactionTypeID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TransactionTypeDesc] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);