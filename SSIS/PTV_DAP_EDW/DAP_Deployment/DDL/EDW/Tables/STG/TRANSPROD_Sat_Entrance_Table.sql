﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[TRANSPROD_Sat_Entrance]', 'U') IS NOT NULL
	DROP TABLE [STG].[TRANSPROD_Sat_Entrance]

CREATE TABLE [STG].[TRANSPROD_Sat_Entrance] (
    [POIEntranceID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Latitude] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Longitude] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
