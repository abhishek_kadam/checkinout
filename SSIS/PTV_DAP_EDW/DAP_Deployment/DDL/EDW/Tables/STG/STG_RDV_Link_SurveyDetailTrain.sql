/*****************************
Author :  Kiran Kumar
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Link_SurveyDetailTrain]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Link_SurveyDetailTrain]

CREATE TABLE [STG].[RDV_Link_SurveyDetailTrain] (
    [Link_SurveyDetailTrainSid] binary(20) NULL, 
    [Hub_SurveySid] binary(20) NULL, 
    [Hub_SmartCardSid] binary(20) NULL, 
    [Hub_EntranceSid] binary(20) NULL, 
    [LoadDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);