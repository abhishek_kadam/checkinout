﻿/*****************************
Author : Abhishek Kadam
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[STG].[MYKI_Sat_DateType]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_DateType]

CREATE TABLE [STG].[MYKI_Sat_DateType]
(
	[Date] date NULL,    
	[DayTypeCategory] varchar(50) NULL,    
	[DayType] varchar(50) NULL,    
	[ABSWeek] int NULL,   
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL  
)
