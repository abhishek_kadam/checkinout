CREATE TABLE [STG].[MYKI_Sat_MYKIJourney] (
    [ACTUAL_TRIP_STARTTIME] datetime2(7) NULL, 
    [SHIFT_ID] int NULL, 
    [ROUTE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_OPEN_TIME] datetime2(7) NULL, 
    [SHIFT_CLOSE_TIME] datetime2(7) NULL, 
    [SP_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_ID] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRANSACTION_ID] bigint NULL, 
    [TRIP_NO] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ENTRY_POINT_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRIP_STARTTIME] date NULL, 
    [TRIP_ENDTIME] date NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
