﻿/*****************************
Author : Charan Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_FareProductType]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_FareProductType]

CREATE TABLE [STG].[MYKI_Sat_FareProductType] (
    [FAREPRODUCT_TYPE_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FAREPRODUCT_EFFECTIVITY_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FAREPRODUCT_LONG_DESCRIPTION] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FAREPRODUCT_TYPE_DESCRIPTION] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PRODUCT_SUBTYPE] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PRODUCT_TYPE] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
