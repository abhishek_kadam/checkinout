﻿/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_ServiceProvider]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_ServiceProvider]


CREATE TABLE [STG].[MYKI_Sat_ServiceProvider] (
    [SP_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_NAME] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_SHORTNAME] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ISSUER_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OPERATOR_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LOAD_AGENT_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LA_AGRM_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OVERRIDE_FUND_COLLECT_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BUSINESS_TYPE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_GRP_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_GRP_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SETT_REPORT_TYPE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EFF_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)