﻿IF OBJECT_ID('[STG].[GTFS_Sat_ServiceException]', 'U') IS NOT NULL
DROP TABLE [STG].[GTFS_Sat_ServiceException] 

/*****************************
Author :  Abhishek Kadam
Modified Date : 03/09/2016
****************************/

CREATE TABLE [STG].[GTFS_Sat_ServiceException] (
    [service_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [exception_type] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
