/*****************************
Author :  Prakash
Modified Date : 03/09/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Hub_JourneyVehicle]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Hub_JourneyVehicle] 

CREATE TABLE [STG].[RDV_Hub_JourneyVehicle] (
    [HubJourneyVehicleSid] binary(20) NULL, 
    [Vehicle] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ParentRoute] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Service] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatingDay] datetime NULL, 
    [Direction] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
