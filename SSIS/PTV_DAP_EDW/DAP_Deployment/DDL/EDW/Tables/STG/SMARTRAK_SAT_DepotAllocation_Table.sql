﻿/*****************************
Author :  Vikram
Modified Date : 03/10/2016
****************************/
IF OBJECT_ID('[STG].[SMARTRAK_Sat_DepotAllocation]', 'U') IS NOT NULL
                DROP TABLE [STG].[SMARTRAK_Sat_DepotAllocation]


CREATE TABLE [STG].[SMARTRAK_Sat_DepotAllocation] (
    [Depot] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LastUpdateDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Line] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Operator] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServicesAllocatedCount] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServicesLoadedCount] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
