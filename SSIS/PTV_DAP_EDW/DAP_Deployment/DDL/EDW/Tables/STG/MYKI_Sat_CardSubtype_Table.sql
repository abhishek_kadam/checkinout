﻿
/*****************************
Author : Kiran Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[MYKI_Sat_CardSubType]', 'U') IS NOT NULL
   DROP TABLE [STG].[MYKI_Sat_CardSubType]


CREATE TABLE [STG].[MYKI_Sat_CardSubType] (
    [CARD_SUBTYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CARD_SUBTYPE_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)