﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/23/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_AvailableRoute]', 'U') IS NOT NULL
	DROP TABLE [STG].[RDV_Sat_AvailableRoute]

CREATE TABLE [STG].[RDV_Sat_AvailableRoute] (
    [Hub_AvailableRouteSid] binary(20) NULL, 
    [RouteDesc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteFrom] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteTo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteVia] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccess] bit NULL, 
    [Myki] bit NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);

