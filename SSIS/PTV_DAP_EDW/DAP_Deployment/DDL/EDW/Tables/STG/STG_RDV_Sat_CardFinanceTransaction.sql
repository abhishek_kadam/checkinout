/*****************************
Author :  Naveen
Modified Date : 03/03/2016
****************************/

IF OBJECT_ID('[STG].[RDV_Sat_CardFinanceTransaction]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Sat_CardFinanceTransaction]

CREATE TABLE [STG].[RDV_Sat_CardFinanceTransaction] (
    [LinkCardFinancialTransactionSid] binary(20) NULL, 
    [ActionListId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopSequenceNumber] int NULL, 
    [EntryPointId] int NULL, 
    [GstAmt] money NULL, 
    [GstRate] money NULL, 
    [PaymentCardTransactionSequenceNum] int NULL, 
    [PaymentTypeId] int NULL, 
    [ProductSerialNo] int NULL, 
    [RefundFeeAmt] money NULL, 
    [SettlementDate] datetime2(7) NULL, 
    [ServiceProviderId] int NULL, 
    [TerminalId] int NULL, 
    [PaymentCardId] bigint NULL, 
    [TpurseAmt] money NULL, 
    [TransactionAmt] money NULL, 
    [TransactionTimeStamp] datetime2(7) NULL, 
    [ZoneNo] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [TicketingBusinessDate] date NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupID] int NOT NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
