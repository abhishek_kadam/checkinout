CREATE TABLE [STG].[RDV_Hub_Zone] (
    [HubZoneSid] binary(20) NULL, 
    [ZoneID] int NULL, 
    [LoadDate] datetime2(7) NULL, 
    [RecordSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
