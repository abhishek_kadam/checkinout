﻿ /*****************************
Author : Charan Kumar
Modified Date : 2/25/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_FareProductType]', 'U') IS NOT NULL
   DROP TABLE [STG].[RDV_Sat_FareProductType]


CREATE TABLE [STG].[RDV_Sat_FareProductType] (
    [HubFareProductSid] binary(20) NULL, 
    [FareproductEffectivityDate] datetime2(7) NULL, 
    [FareProductLongDesccription] varchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [FareProductTypeDescription] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProductSubType] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ProductType] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
