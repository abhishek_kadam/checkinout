CREATE EXTERNAL TABLE [EXT].[GTFS_10_trips] (
    [route_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [service_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [trip_id] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_id] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [trip_headsign] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [direction_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/10_trips.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)