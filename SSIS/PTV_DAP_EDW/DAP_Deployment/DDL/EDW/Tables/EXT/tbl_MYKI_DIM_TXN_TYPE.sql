CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_TXN_TYPE] (
    [TXN_TYPE_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_TYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TXN_TYPE_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='incoming/DIM_TXN_TYPE.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 1
)