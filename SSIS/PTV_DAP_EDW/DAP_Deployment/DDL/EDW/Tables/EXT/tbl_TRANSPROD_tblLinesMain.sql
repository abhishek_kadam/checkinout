CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblLinesMain] (
    [LineMainID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineDescLong] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EffectiveDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TermDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineNotes] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccess] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MapImageID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [URL1] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PublicDescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ReducedHolServ] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopTTtemplate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccessSat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelChairAccessSun] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PdfLink] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteTTLink] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RegionId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MDMServGrp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MDMContractCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MDMLegalEnt] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblLinesMain.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)