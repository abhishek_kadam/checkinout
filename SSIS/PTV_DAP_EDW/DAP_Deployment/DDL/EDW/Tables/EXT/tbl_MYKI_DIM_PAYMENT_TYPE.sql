CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_PAYMENT_TYPE] (
    [PAYMENT_TYPE_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PAYMENT_TYPE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PAYMENT_TYPE_DESC] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='incoming/DIM_PAYMENT_TYPE.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 1
)