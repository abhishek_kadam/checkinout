﻿CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblStopNames] (
    [StopNamesID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MetlinkStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopName] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopNameType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ManualEdit] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblStopNames.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)