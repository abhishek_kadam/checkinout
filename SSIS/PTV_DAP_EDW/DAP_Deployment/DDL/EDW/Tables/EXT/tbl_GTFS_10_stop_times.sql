CREATE EXTERNAL TABLE [EXT].[GTFS_10_stop_times] (
    [trip_id] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [arrival_time] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [departure_time] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_sequence] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_headsign] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [pickup_type] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [drop_off_type] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_dist_traveled] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/10_stop_times.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)