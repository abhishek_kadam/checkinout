CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblModes] (
    [OID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopModeID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopModeName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblModes.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)