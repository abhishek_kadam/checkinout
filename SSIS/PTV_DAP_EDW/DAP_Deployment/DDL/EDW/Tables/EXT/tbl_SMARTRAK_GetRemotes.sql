﻿CREATE EXTERNAL TABLE [EXT].[SMARTRAK_GetRemotes] (
    [Alias] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CompanyName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ForwarderId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Rego] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RemoteId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/11/30/GetRemotes.csv.gz',
      DATA_SOURCE = datalake_smartrak,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)