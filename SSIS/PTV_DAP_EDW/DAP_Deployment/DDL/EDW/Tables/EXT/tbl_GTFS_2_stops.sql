CREATE EXTERNAL TABLE [EXT].[GTFS_2_stops] (
    [stop_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_name] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_lat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [stop_lon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/2_stops.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)