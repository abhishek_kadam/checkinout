CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblRoutesAvailable] (
    [RouteID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteFrom] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteTo] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteVia] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Defined] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceModeID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WCA] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LastStopFootnote] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Myki] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblRoutesAvailable.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)