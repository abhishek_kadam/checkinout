CREATE EXTERNAL TABLE [EXT].[GTFS_2_Agency] (
    [agency_id] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [agency_name] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [agency_url] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [agency_timezone] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [agency_lang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/2_agency.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)