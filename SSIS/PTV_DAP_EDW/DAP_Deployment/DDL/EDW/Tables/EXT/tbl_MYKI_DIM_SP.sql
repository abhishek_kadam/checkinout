CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_SP] (
    [SP_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_NAME] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_SHORTNAME] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ISSUER_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OPERATOR_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LOAD_AGENT_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LA_AGRM_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OVERRIDE_FUND_COLLECT_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BUSINESS_TYPE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_GRP_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_GRP_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SETT_REPORT_TYPE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ABN] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EFF_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SRC_UPDATED_DATE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='incoming/DIM_SP.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 1
)