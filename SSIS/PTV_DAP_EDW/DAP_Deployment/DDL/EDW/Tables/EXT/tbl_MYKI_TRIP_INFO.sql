CREATE EXTERNAL TABLE [EXT].[MYKI_TRIP_INFO] (
    [DRIVER_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_OPEN_TIME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_CLOSE_TIME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ACTUAL_TRIP_STARTTIME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DRIVER_SHIFT_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRIP_NO] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ROUTE_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SP_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ENTRY_POINT_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRIP_STARTTIME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRIP_ENDTIME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TERMINAL_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_CLOSE_OPER_LOG] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHIFT_OPEN_OPER_LOG] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TRANSACTION_ID] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_SOURCE] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [COMMENTS] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/TRIP_INFO.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)