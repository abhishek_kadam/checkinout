CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblCalendar] (
    [CalendarID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [CalendarDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PublicHolidayID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Timestamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblCalendar.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)