CREATE EXTERNAL TABLE [EXT].[SMARTRAK_DepotAllocationStats] (
    [Depot] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LastUpdateDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Line] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Operator] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServicesAllocatedCount] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServicesLoadedCount] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2016/02/02/DepotAllocationStats.csv.gz',
      DATA_SOURCE = datalake_smartrak,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)