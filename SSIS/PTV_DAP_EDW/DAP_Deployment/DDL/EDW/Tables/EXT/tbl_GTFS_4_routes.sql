CREATE EXTERNAL TABLE [EXT].[GTFS_4_routes] (
    [route_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [agency_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [route_short_name] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [route_long_name] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [route_type] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/4_routes.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)