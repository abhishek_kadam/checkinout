CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_SERVICE_LOCATION] (
    [SVCE_LOC_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SVCE_LOC_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SVCE_LOC_NAME] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SVCE_LOC_SHORTNAME] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [COVERAGE_NAME] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LOCATIONTYPEID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SERVICEPROVIDERID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SERVICELOCATIONDESCRIPTION] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [NTSZONEID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [INVENTLOCATIONID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LOCATIONTYPE_DESC] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SERVICEPROVIDER_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [NTSCCAPROVIDERID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/DIM_SERVICE_LOCATION.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)