CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblPOIEntrances] (
    [POIEntrance_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [POI_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLong] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SuburbID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ImageID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblPOIEntrances.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)