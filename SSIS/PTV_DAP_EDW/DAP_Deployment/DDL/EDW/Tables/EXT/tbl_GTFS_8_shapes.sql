CREATE EXTERNAL TABLE [EXT].[GTFS_8_shapes] (
    [shape_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_sequence] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_dist_traveled] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2016/01/29/8_shapes.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)