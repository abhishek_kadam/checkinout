CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblPOI] (
    [POI_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Description] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SuburbID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [URL1] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [URL2] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Phone] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MapImageID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MelwaysRef] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WheelchairAccess] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSLong] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblPOI.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)