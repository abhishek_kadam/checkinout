CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblServiceTimes] (
    [TimeRecordID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MetlinkStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ArrivalTime] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DepartTime] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BayPlatform] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Sequence] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblServiceTimes.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)