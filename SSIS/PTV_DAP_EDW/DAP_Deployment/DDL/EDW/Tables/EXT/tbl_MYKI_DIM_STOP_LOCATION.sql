CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_STOP_LOCATION] (
    [STOP_LOCATION_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [STOP_LOCATION_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZONE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZONE_DESC] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [STOP_LOCATION_SHORT_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [STOP_LOCATION_LONG_DESC] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LAT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LONG] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LAT_TOLERENCE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LONG_TOLERENCE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LAT_OFFSET] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPS_LONG_OFFSET] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [INNER_ZONE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OUTER_ZONE] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='incoming/DIM_STOP_LOCATION.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 1
)