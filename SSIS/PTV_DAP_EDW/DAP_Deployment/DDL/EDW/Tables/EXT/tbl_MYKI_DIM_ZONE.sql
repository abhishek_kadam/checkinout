CREATE EXTERNAL TABLE [EXT].[MYKI_DIM_ZONE] (
    [ZONE_KEY] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZONE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PHYSICAL_ZONE_ID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SHORT_NAME] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LONG_NAME] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ZONE_ORDERNBR] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ISCITYSAVER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LOGICAL_ZONE_IND] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_DT] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [REC_UPDT_USER] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='incoming/DIM_ZONE.csv.gz',
      DATA_SOURCE = DataLake_Myki,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 1
)