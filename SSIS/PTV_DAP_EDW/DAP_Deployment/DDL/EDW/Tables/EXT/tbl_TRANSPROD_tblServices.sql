CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblServices] (
    [ServiceID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteScheduleID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ServiceNbr] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleDisplay] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [VehicleTypeID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [ConnGroupID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DMon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DTue] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DWed] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DThu] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DFri] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DSat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DSun] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DestinationText] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DestinationText2] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WCA] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TicketCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblServices.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)