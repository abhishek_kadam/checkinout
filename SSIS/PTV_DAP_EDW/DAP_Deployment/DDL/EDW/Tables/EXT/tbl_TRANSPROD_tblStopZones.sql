﻿CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblStopZones] (
    [ZoneStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TicketZoneID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MetlinkStopID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblStopZones.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)