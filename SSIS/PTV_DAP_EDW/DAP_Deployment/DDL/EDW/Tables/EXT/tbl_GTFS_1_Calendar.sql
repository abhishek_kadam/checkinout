CREATE EXTERNAL TABLE [EXT].[GTFS_1_Calendar] (
    [service_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [monday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [tuesday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [wednesday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [thursday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [friday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [saturday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [sunday] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [start_date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [end_date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/1_calendar.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)