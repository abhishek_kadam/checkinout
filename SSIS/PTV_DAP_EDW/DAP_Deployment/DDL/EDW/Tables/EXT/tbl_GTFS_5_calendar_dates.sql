CREATE EXTERNAL TABLE [EXT].[GTFS_5_calendar_dates] (
    [service_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [date] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [exception_type] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/5_calendar_dates.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)