CREATE EXTERNAL TABLE [EXT].[GTFS_5_shapes] (
    [shape_id] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_lon] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_pt_sequence] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [shape_dist_traveled] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/5_shapes.txt.gz',
      DATA_SOURCE = datalake_gtfs,
      FILE_FORMAT = GTFSZip,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)