CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblLines] (
    [OID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineMainID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DescShort] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DirectionDesc] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineFrom] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineTo] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineVia] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Defined] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LineNotes] varchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DataCollected] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopsDeDuped] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OpChecked] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GPSChecked] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RouteComplete] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Sequenced] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [MapInfo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DirectionCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblLines.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)