CREATE EXTERNAL TABLE [EXT].[TRANSPROD_tblOperators] (
    [OperatorID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperatorName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StartDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [EndDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BusinessName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TradingName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [GroupName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetAddress] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PostCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Phone] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Fax] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [email] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [WebSite] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [DataSystem] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [OperModeID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimeStamp] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (LOCATION='2015/10/30/tblOperators.csv.gz',
      DATA_SOURCE = datalake_transprod,
      FILE_FORMAT = format_csv_gz,
      REJECT_TYPE = VALUE,
      REJECT_VALUE = 0
)