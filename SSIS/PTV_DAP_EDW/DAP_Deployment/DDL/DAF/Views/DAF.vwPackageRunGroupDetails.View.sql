/****** Object:  View [DAF].[vwPackageRunGroupDetails]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[vwPackageRunGroupDetails]
GO
/****** Object:  View [DAF].[vwPackageRunGroupDetails]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [DAF].[vwPackageRunGroupDetails]
WITH SCHEMABINDING
AS
SELECT
	RG.RunGroupID
	, RG.[Description] AS RunGroupDescription
	, RG.LastExtractDateTime
	, RG.LastExtractLoadedDateTime
	, RG.EnabledFlag AS RunGroupEnabled
	, RG.DataSourceRefCode
	, PRG.DataEntityRefCode
	, PRG.EnabledFlag AS PackageEnabled
	, PRG.ExitActionRefCode
	, PRG.ExecutionOrderNum
	, P.PackageID
	, P.PackageName
	, P.[Description] AS PackageDescription
FROM DAF.PackageRunGroup PRG
	JOIN DAF.Package P ON P.PackageID = PRG.PackageID
	JOIN DAF.RunGroup RG ON RG.RunGroupID = PRG.RunGroupID;

GO
