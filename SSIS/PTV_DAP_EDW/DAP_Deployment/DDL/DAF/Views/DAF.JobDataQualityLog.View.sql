/****** Object:  View [DAF].[JobDataQualityLog]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobDataQualityLog]
GO
/****** Object:  View [DAF].[JobDataQualityLog]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [DAF].[JobDataQualityLog]
AS
select jpr.JobNumber,
       jpr.JobRunId,
	   dql.DataQualityLogID,
	   dql.SeverityLevelRefCode,
	   dsr.ShortName as DataSource,
	   der.ShortName as Entity,
	   (case when dqr.DataQualityRuleID = 21 then dql.MessageText else dqr.Description end) as RuleDescription,
	   (case when der.KeyAttribute1 is null then '' else der.KeyAttribute1 + ': ''' +isnull(dql.KeyAttributeValue1, '') +'''' end) +
	       (case when der.KeyAttribute2 is null then '' else ', ' +der.KeyAttribute2 + ': ''' +isnull(dql.KeyAttributeValue2, '') +'''' end) +
	       (case when der.KeyAttribute3 is null then '' else ', ' +der.KeyAttribute3 + ': ''' +isnull(dql.KeyAttributeValue3, '') +'''' end) +
	       (case when der.KeyAttribute4 is null then '' else ', ' +der.KeyAttribute4 + ': ''' +isnull(dql.KeyAttributeValue4, '') +'''' end) +
	       (case when der.KeyAttribute5 is null then '' else ', ' +der.KeyAttribute5 + ': ''' +isnull(dql.KeyAttributeValue5, '') +'''' end) +
	       (case when der.KeyAttribute6 is null then '' else ', ' +der.KeyAttribute6 + ': ''' +isnull(dql.KeyAttributeValue6, '') +'''' end) +
	       (case when der.KeyAttribute7 is null then '' else ', ' +der.KeyAttribute7 + ': ''' +isnull(dql.KeyAttributeValue7, '') +'''' end) +
	       (case when der.KeyAttribute8 is null then '' else ', ' +der.KeyAttribute8 + ': ''' +isnull(dql.KeyAttributeValue8, '') +'''' end) +
	       (case when der.KeyAttribute9 is null then '' else ', ' +der.KeyAttribute9 + ': ''' +isnull(dql.KeyAttributeValue9, '') +'''' end) as ReferenceText
FROM   DAF.JobPackageRun jpr
JOIN   DAF.DataQualityLog dql on dql.ServicePackageRunID = jpr.ServicePackageRunId
join   DAF.DataQualityRule dqr on  dqr.DataQualityRuleID = dql.DataQualityRuleId
                               and dqr.RuleVersionNum = dql.RuleVersionNum
join   DAF.DataSourceRef dsr on dsr.DataSourceRefCode = dql.DataSourceRefCode
join   DAF.DataEntityRef der on der.DataEntityRefCode = dql.DataEntityRefCode



GO
