/****** Object:  View [DAF].[JobSummary]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobSummary]
GO
/****** Object:  View [DAF].[JobSummary]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [DAF].[JobSummary]
AS

	SELECT s.ScheduleName,
	       s.DisplayOrder,
	       j.JobNumber, 
		   j.JobName, 
		   j.ScheduleNumber, 
		   j.ScheduleSequence, 
		   j.LastJobRunId, 
		   jr.JobStartDate, 
		   jr.JobEndDate, 
		   DATEDIFF(s, jr.JobStartDate, jr.JobEndDate) AS Duration, 
		   jr.ExitStatus
	FROM   DAF.Job AS j 
	JOIN   DAF.Schedule s on j.ScheduleNumber = s.ScheduleNumber
	LEFT   OUTER JOIN DAF.JobRun AS jr ON j.LastJobRunId = jr.JobRunId


GO
