/****** Object:  View [DAF].[MatchingException]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[MatchingException]
GO
/****** Object:  View [DAF].[MatchingException]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [DAF].[MatchingException]
AS


Select 
	DQL.DataQualityLogID,
	DQL.DataQualityRuleID,
	DQR.[Description],
	DQLD_DataSrc.ValueExisting DataSourceRefCodeExisting,
	IsNull(DQLD_DataSrc.ValueNew, DQL.DataSourceRefCode) DataSourceRefCodeNew,
	DQLD_Summary.ValueExisting,
	DQLD_Summary.ValueNew,
	DQR.SuggestedResolution,
	DQL.CreatedDateTime,
	DQL.DataEntityRefCode,
	DQL.OrganisationalUnitCode,
	DQL.OrganisationalUnitName,
	DQL.SeverityLevelRefCode,
	DQL.IssueStatusRefCode,
	1 As TotalOutstandingIssues,
	DQL.FullTextSearch
From 
	DAF.DataQualityLog DQL Join
	(
		Select distinct ld.DataQualityLogId,
			Replace(
			substring(
				(
					Select '~~' + ld_inner.FieldName + '=' + Replace(IsNull(ld_inner.ValueExisting, '(null)'), '~~', '--')  AS [text()]
					From daf.DataQualityLogDetail ld_inner
					Where ld_inner.DataQualityLogId = ld.DataQualityLogId And ld_inner.FieldName != 'LastUpdatedDataSource'
					ORDER BY ld_inner.DataQualityLogID
					For XML PATH ('')
				), 3, 8000), '~~', CHAR(13) + CHAR(10)) ValueExisting,
			Replace(
			substring(
				(
					Select '~~' + ld_inner.FieldName + '=' + Replace(IsNull(ld_inner.ValueNew, '(null)'), '~~', '--')  AS [text()]
					From daf.DataQualityLogDetail ld_inner
					Where ld_inner.DataQualityLogId = ld.DataQualityLogId And ld_inner.FieldName != 'LastUpdatedDataSource'
					ORDER BY ld_inner.DataQualityLogID
					For XML PATH ('')
				), 3, 8000), '~~', CHAR(13) + CHAR(10)) ValueNew
		From daf.DataQualityLogDetail ld 
	) DQLD_Summary On DQL.DataQualityLogID = DQLD_Summary.DataQualityLogID Join
	DAF.DataQualityRule DQR on DQL.DataQualityRuleID = DQR.DataQualityRuleID Left Join
	DAF.DataQualityLogDetail DQLD_DataSrc on DQL.DataQualityLogID = DQLD_DataSrc.DataQualityLogID And DQLD_DataSrc.FieldName = 'LastUpdatedDataSource'
Where
	DQL.SeverityLevelRefCode = 'HOLD'

	

GO
