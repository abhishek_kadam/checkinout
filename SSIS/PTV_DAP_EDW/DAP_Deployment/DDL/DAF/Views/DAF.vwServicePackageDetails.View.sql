/****** Object:  View [DAF].[vwServicePackageDetails]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[vwServicePackageDetails]
GO
/****** Object:  View [DAF].[vwServicePackageDetails]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [DAF].[vwServicePackageDetails]
WITH SCHEMABINDING
AS
SELECT
	P.PackageID
	, P.PackageName
	, S.ServicePackageRunID
	, S.ProgressStatusRefCode
	, S.NumRecordsProcessed
	, S.NumRecordsFailed
	, S.StartDateTime AS ServicePackageStart
	, S.EndDateTime AS ServicePackageEnd
	, C.ControllingPackageRunID
	, C.ControllingPackageName
	, C.StartDateTime AS ControllingPackageStart
	, C.EndDateTime AS ControllingPackageEnd
	, R.RunGroupID
	, R.[Description] AS RunGroupDescription
	, R.EnabledFlag AS RunGroupEnabledFlag
	, R.DataSourceRefCode
	, R.LastExtractDateTime
	, R.LastExtractLoadedDateTime
FROM DAF.ServicePackageRun S
	JOIN DAF.Package P ON P.PackageID = S.PackageID
	JOIN DAF.ControllingPackageRun C ON C.ControllingPackageRunID = S.ControllingPackageRunID
	JOIN DAF.RunGroup R ON R.RunGroupID = C.RunGroupID;

GO
