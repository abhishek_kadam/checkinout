/****** Object:  View [DAF].[JobEventLog]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobEventLog]
GO
/****** Object:  View [DAF].[JobEventLog]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [DAF].[JobEventLog]
AS
	SELECT jpr.JobNumber,
		   jpr.JobRunId,
		   el.EventLogID,
		   el.SeverityLevelRefCode, 
		   el.EventSource, 
		   el.DataEntityRefCode,
		   el.EventText, 	   
		   el.ServicePackageRunID
	FROM   DAF.JobPackageRun jpr
	JOIN   DAF.EventLog el ON el.ServicePackageRunID = jpr.ServicePackageRunID
	                       OR el.ControllingPackageRunID = jpr.ControllingPackageRunID

GO
