/****** Object:  View [DAF].[JobRunDetail]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobRunDetail]
GO
/****** Object:  View [DAF].[JobRunDetail]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [DAF].[JobRunDetail]
AS
	SELECT j.JobNumber, 
		   jr.JobRunId, 
		   cpr.RunGroupID, 
		   rg.Description AS RunGroupDescription, 
		   cpr.StartDateTime as ControllingPackageStartDateTime, 
		   cpr.EndDateTime as ControllingPackageEndDateTime, 
		   datediff(ss, cpr.StartDateTime, cpr.EndDateTime) ControllingPackageRunTimeSecs,
		   cpr.ProgressStatusRefCode as ControllingPackageProgressStatusRefCode, 
		   spr.StartDateTime as ServicePackageStartDateTime, 
		   spr.EndDateTime as ServicePackageEndDateTime, 
		   datediff(ss, spr.StartDateTime, spr.EndDateTime) ServicePackageRunTimeSecs,
		   spr.ProgressStatusRefCode as ServicePackageProgressStatusRefCode,
		   spr.NumRecordsProcessed,
		   spr.NumRecordsFailed,	   
		   replace (p.PackageName,'.dtsx','') Process,
		   p.Description as ProcessType
	FROM   daf.job AS j 
	INNER  JOIN daf.jobrun AS jr ON jr.jobnumber = j.jobnumber 
	INNER  JOIN daf.jobstep AS js ON js.jobnumber = j.jobnumber 
	INNER  JOIN daf.rungroup AS rg ON js.rungroupid = rg.rungroupid 
	LEFT   OUTER JOIN daf.controllingpackagerun AS cpr ON cpr.rungroupid = js.rungroupid 
													   AND cpr.startdatetime BETWEEN jr.jobstartdate AND jr.jobenddate 
													   AND cpr.enddatetime BETWEEN jr.jobstartdate AND jr.jobenddate 
	LEFT   OUTER JOIN daf.servicepackagerun AS spr ON cpr.controllingpackagerunid = spr.controllingpackagerunid 
	LEFT   OUTER JOIN daf.package AS p ON spr.packageid = p.packageid  
	WHERE j.JobNumber <> 11
	UNION ALL
	SELECT j.JobNumber, 
		   jr.JobRunId, 
		   cpr.RunGroupID, 
		   rg.Description AS RunGroupDescription, 
		   cpr.StartDateTime as ControllingPackageStartDateTime, 
		   cpr.EndDateTime as ControllingPackageEndDateTime, 
		   datediff(ss, cpr.StartDateTime, cpr.EndDateTime) ControllingPackageRunTimeSecs,
		   cpr.ProgressStatusRefCode as ControllingPackageProgressStatusRefCode, 
		   spr.StartDateTime as ServicePackageStartDateTime, 
		   spr.EndDateTime as ServicePackageEndDateTime, 
		   datediff(ss, spr.StartDateTime, spr.EndDateTime) ServicePackageRunTimeSecs,
		   spr.ProgressStatusRefCode as ServicePackageProgressStatusRefCode,
		   spr.NumRecordsProcessed,
		   spr.NumRecordsFailed,	   
		   replace (p.PackageName,'.dtsx','') Process,
		   p.Description as ProcessType
	FROM   daf.job AS j 
	INNER  JOIN daf.jobrun AS jr ON jr.jobnumber = j.jobnumber 
	LEFT   OUTER JOIN daf.controllingpackagerun AS cpr ON  cpr.startdatetime BETWEEN jr.jobstartdate AND jr.jobenddate 
													   AND cpr.enddatetime BETWEEN jr.jobstartdate AND jr.jobenddate 
	LEFT   OUTER JOIN DAF.RunGroup rg on cpr.RunGroupID = rg.RunGroupID 
	LEFT   OUTER JOIN daf.servicepackagerun AS spr ON cpr.controllingpackagerunid = spr.controllingpackagerunid 
	LEFT   OUTER JOIN daf.package AS p ON spr.packageid = p.packageid  
	WHERE  j.JobNumber = 11


GO
