/****** Object:  View [DAF].[JobDetail]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobDetail]
GO
/****** Object:  View [DAF].[JobDetail]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [DAF].[JobDetail]
AS
SELECT     jr.JobRunId, j.JobNumber, j.JobName, jr.JobStartDate, jr.JobEndDate, DATEDIFF(s, jr.JobStartDate, jr.JobEndDate) AS Duration, jr.ExitStatus
FROM         DAF.Job AS j INNER JOIN
                      DAF.JobRun AS jr ON jr.JobNumber = j.JobNumber



GO
