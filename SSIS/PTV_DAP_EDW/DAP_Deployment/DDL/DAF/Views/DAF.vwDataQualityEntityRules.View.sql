/****** Object:  View [DAF].[vwDataQualityEntityRules]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[vwDataQualityEntityRules]
GO
/****** Object:  View [DAF].[vwDataQualityEntityRules]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [DAF].[vwDataQualityEntityRules]
WITH SCHEMABINDING
AS
SELECT
	E.DataQualityEntityID
	, E.DataSourceRefCode
	, E.DataEntityRefCode
	, E.ExecutionOrderNum
	, E.DataQualityProcessFlag
	, R.DataQualityRuleID
	, R.[Description]
	, R.ShortName
	, R.SeverityLevelRefCode
	, R.ImplementationName
	, R.ImplementationRefCode
	, R.EnabledFlag AS RuleEnabledFlag
	, R.RuleImplimentationType
	, R.SuggestedResolution
FROM DAF.DataQualityEntity E
	JOIN DAF.DataQualityRule R ON R.DataQualityRuleID = E.DataQualityRuleID

GO
