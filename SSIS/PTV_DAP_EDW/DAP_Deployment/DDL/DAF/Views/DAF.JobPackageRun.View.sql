/****** Object:  View [DAF].[JobPackageRun]    Script Date: 2/23/2016 5:28:19 PM ******/
DROP VIEW [DAF].[JobPackageRun]
GO
/****** Object:  View [DAF].[JobPackageRun]    Script Date: 2/23/2016 5:28:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [DAF].[JobPackageRun]
AS

	SELECT     j.JobNumber, 
			   jr.JobRunId, 
			   cpr.RunGroupId, 
			   cpr.ControllingPackageRunID, 
			   spr.ServicePackageRunID
	FROM       DAF.Job AS j 
	INNER JOIN DAF.JobRun AS jr ON jr.JobNumber = j.JobNumber 
	INNER JOIN DAF.JobStep AS js ON js.JobNumber = j.JobNumber 
	INNER JOIN DAF.RunGroup AS rg ON js.RunGroupId = rg.RunGroupID 
	INNER JOIN DAF.ControllingPackageRun AS cpr ON  cpr.RunGroupID = js.RunGroupId 
												AND cpr.StartDateTime BETWEEN jr.JobStartDate AND jr.JobEndDate 
												AND cpr.EndDateTime BETWEEN jr.JobStartDate AND jr.JobEndDate 
	INNER JOIN DAF.ServicePackageRun AS spr ON cpr.ControllingPackageRunID = spr.ControllingPackageRunID
	WHERE      j.JobNumber <> 11 --ad hoc job id
	UNION ALL
	SELECT     j.JobNumber, 
			   jr.JobRunId, 
			   cpr.RunGroupID,
			   cpr.ControllingPackageRunID, 
			   spr.ServicePackageRunID
	FROM       DAF.Job AS j 
	INNER JOIN DAF.JobRun AS jr ON jr.JobNumber = j.JobNumber 
	INNER JOIN DAF.ControllingPackageRun AS cpr ON  cpr.StartDateTime BETWEEN jr.JobStartDate AND jr.JobEndDate 
												AND cpr.EndDateTime BETWEEN jr.JobStartDate AND jr.JobEndDate 
	INNER JOIN DAF.ServicePackageRun AS spr ON cpr.ControllingPackageRunID = spr.ControllingPackageRunID
	WHERE      j.JobNumber = 11 --ad hoc job id

GO
