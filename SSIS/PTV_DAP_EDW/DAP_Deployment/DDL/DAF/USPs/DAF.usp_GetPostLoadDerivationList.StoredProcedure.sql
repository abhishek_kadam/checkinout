/****** Object:  StoredProcedure [DAF].[usp_GetPostLoadDerivationList]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetPostLoadDerivationList]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetPostLoadDerivationList]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [DAF].[usp_GetPostLoadDerivationList]
(
    @DataSourceRefCode varchar(50),
    @DataEntityRefCode varchar(50)
)
as

/* *****************************************************************************
 * Name    : usp_GetPostLoadDerivationList
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(400)

    --check that parameters are set
    if @DataSourceRefCode is null
    begin
        set @errorMsg = 'Parameter @DataSourceRefCode is not set'
        goto HandleError
    end

    if @DataEntityRefCode is null
    begin
        set @errorMsg = 'Parameter @DataEntityRefCode is not set'
        goto HandleError
    end
    
    select ple.PostLoadDerivationID,
           pld.ImplementationName,
		   pld.ImplementationRefCode,
		   IsNull(pld.DataQualityRuleId, -1) DataQualityRuleId
    from   DAF.PostLoadEntity ple
	join   DAF.PostLoadDerivation pld on ple.PostLoadDerivationID = pld.PostLoadDerivationID
    where  ple.DataSourceRefCode = @DataSourceRefCode
    and    ple.DataEntityRefCode = @DataEntityRefCode
	and    pld.EnabledFlag = 'Y'
    order  by ple.ExecutionOrderNum asc
    
    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

end


GO
