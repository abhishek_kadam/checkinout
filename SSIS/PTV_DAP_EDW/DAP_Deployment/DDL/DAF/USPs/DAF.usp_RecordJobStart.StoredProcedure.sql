/****** Object:  StoredProcedure [DAF].[usp_RecordJobStart]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_RecordJobStart]
GO
/****** Object:  StoredProcedure [DAF].[usp_RecordJobStart]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [DAF].[usp_RecordJobStart] 
	( @pJobNumber int)
AS

/* *****************************************************************************
 * Name    : DAF.usp_RecordJobStart
 *
 * Created : 12th February 2012
 * Author  : Shaun Wadsworth
 *
 * Abstract: Adds a new record to the JobRun table to record the start of a run.
 *
 * Input   : pJobId	= The job Id that is running
  *          
 * Output  : None
 *
 * Returns : 0 = Success, -1 Error
 *           
 * *****************************************************************************
 * Change History
 * Author           Version   Date         CR/DF   Description
 * ------           -------   ----------  -------  -----------
 * Shaun Wadsworth      1.0   20/12/2012    --     Initial version
 */

SET NOCOUNT ON
BEGIN

	INSERT INTO DAF.JobRun 
		(JobNumber,
		 JobStartDate,
		 JobEndDate,
		 ExitStatus
		 )
	VALUES
		(@pJobNumber,
		 getdate(),
		 null,
		 'In Progress'
		 )

	UPDATE DAF.Job
	set    LastJobRunId = @@IDENTITY
	where  JobNumber = @pJobNumber

END;


GO
