/****** Object:  StoredProcedure [DAF].[usp_InsertControllingPackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertControllingPackageRun]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertControllingPackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [DAF].[usp_InsertControllingPackageRun]
(
@ControllingPackageName VARCHAR(50),
@RunBy varchar(50),
@ProgressStatusRefCode varchar(50),
@RunGroupID int,
@ControllingPackageRunID INT OUTPUT
)
as

/* *****************************************************************************
 * Name    : Usp_InsertControllingPackageRun
 *
 * Abstract: This function will be used to insert a new record into the ControllingPackageRun table. 
 It creates a new ControllingPackageRunID by getting the next ControllingPackageRun sequence number.

 * Input   : none
 *          
 * Output  : Return the PackageJobRunID
 *
 * Returns : 
 *           
 * *****************************************************************************
 */

BEGIN
	DECLARE @CntControllingPackageRun INT,
			@errorMsg VARCHAR(400)

	IF @ControllingPackageName IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ControllingPackageName is not set'
        GOTO HandleError
    END

	IF @RunBy IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @RunBy is not set'
        GOTO HandleError
    END
	
	IF @ProgressStatusRefCode IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ProgressStatusRefCodeRunBy is not set'
        GOTO HandleError
    END

	IF @RunGroupID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @RunGroupID is not set'
        GOTO HandleError
    END

    BEGIN TRAN

	    SELECT @CntControllingPackageRun = ISNULL(MAX(ControllingPackageRunID),0) + 1 FROM DAF.ControllingPackageRun

	    INSERT INTO ControllingPackageRun
		    (ControllingPackageRunID,
		    ControllingPackageName,
            RunGroupID,
		    ProgressStatusRefCode,
		    RunBy, 
		    StartDateTime
		    )
	    VALUES 
		    (@CntControllingPackageRun,
		    @ControllingPackageName,
            @RunGroupID,
		    @ProgressStatusRefCode,
		    @RunBy,
		    GETDATE() --start date
		    )  

    COMMIT TRAN
	
	SET @ControllingPackageRunID = @CntControllingPackageRun 
	RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

END

GO
