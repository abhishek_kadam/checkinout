/****** Object:  StoredProcedure [DAF].[usp_UpdateServicePackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_UpdateServicePackageRun]
GO
/****** Object:  StoredProcedure [DAF].[usp_UpdateServicePackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DAF].[usp_UpdateServicePackageRun]
(
	@ServicePackageRunID INT,
	@ProgressStatusRefCode VARCHAR(50),
	@NumRecordsProcessed INT = null,
	@NumRecordsFailed INT = null
)
as
/* *****************************************************************************
 * Name    : Usp_UpdateServicePackageRun
 *
 * Abstract: This function will be used by service package to update a record in the ServicePackageRun table.
 *           If no record counts are included, they will not be updated. 
 *
 * Input   : ServicePackageRunID
 *          
 * Output  : none
 *
 * Returns : 
 *           
 * *****************************************************************************
 */

BEGIN
    DECLARE @errorMsg varchar(400)

    --check that parameters are set
    IF @ServicePackageRunID is null
    BEGIN
        SET @errorMsg = 'Parameter @ServicePackageRunID is not set'
        GOTO HandleError
    END

	IF @ProgressStatusRefCode is null
    BEGIN
        SET @errorMsg = 'Parameter @ProgressStatusRefCode is not set'
        GOTO HandleError
    END

	IF @NumRecordsFailed is not null
    BEGIN
       IF @NumRecordsFailed = -1  SET @NumRecordsFailed = 0
    END


    UPDATE ServicePackageRun
		SET ProgressStatusRefCode = @ProgressStatusRefCode,
			NumRecordsProcessed = isnull(@NumRecordsProcessed, NumRecordsProcessed),
			NumRecordsFailed	= isnull(@NumRecordsFailed, NumRecordsFailed),
			EndDateTime			= GETDATE()
    WHERE
		ServicePackageRunID = @ServicePackageRunID

RETURN
    HandleError:
        raiserror(@errorMsg, 16, 1)
        RETURN
END


GO
