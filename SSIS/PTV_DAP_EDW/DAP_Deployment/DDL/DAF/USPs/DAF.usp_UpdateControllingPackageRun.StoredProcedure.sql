/****** Object:  StoredProcedure [DAF].[usp_UpdateControllingPackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_UpdateControllingPackageRun]
GO
/****** Object:  StoredProcedure [DAF].[usp_UpdateControllingPackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DAF].[usp_UpdateControllingPackageRun]
(
    @ControllingPackageRunID INT,
	@ProgressStatusRefCode VARCHAR(50)
	--,@CancelFlag varchar(1),
	--@CancelledBy varchar(100)
)
as
/* *****************************************************************************
 * Name    : Usp_UpdateControllingPackageRun
 *
 * Abstract: This function will be used to update a ControllingPackageRun record.

 * Input   : ControllingPackageRunID
 *          
 * Output  : none
 *
 * Returns : 
 *           
 * *****************************************************************************
 */


 
--exec [Usp_UpdateControllingPackageRun] 3, 'Completed', 'N', 'Nel'
  
BEGIN
    DECLARE @errorMsg varchar(400)

    --check that parameters are set
    IF @ControllingPackageRunID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ControllingPackageRunID is not set'
        GOTO HandleError
    END

	IF @ProgressStatusRefCode IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ProgressStatusRefCode is not set'
        GOTO HandleError
    END


    UPDATE ControllingPackageRun
		SET ProgressStatusRefCode = @ProgressStatusRefCode,
			EndDateTime	= GETDATE()
    WHERE
		ControllingPackageRunID = @ControllingPackageRunID		
		
RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        RETURN
END






GO
