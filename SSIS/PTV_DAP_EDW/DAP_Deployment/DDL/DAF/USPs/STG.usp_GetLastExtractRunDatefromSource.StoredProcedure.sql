/****** Object:  StoredProcedure [STG].[usp_GetLastExtractRunDatefromSource]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [STG].[usp_GetLastExtractRunDatefromSource]
GO
/****** Object:  StoredProcedure [STG].[usp_GetLastExtractRunDatefromSource]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [STG].[usp_GetLastExtractRunDatefromSource] @RunGroupId [int] AS
BEGIN 
	
	--Declare @gDate Date
	--SET @gDate = (Select getdate() )
	--Return @gDate
	Select DATEFROMPARTS(1900,1,1)
END 
GO
