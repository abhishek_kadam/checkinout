/****** Object:  StoredProcedure [DAF].[usp_GetServicePackageList]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetServicePackageList]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetServicePackageList]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetServicePackageList 3

CREATE PROCEDURE [DAF].[usp_GetServicePackageList]
(
    @RunGroupID INT
)
as

/* *****************************************************************************
 * Name    : usp_GetServicePackageList
 *
 * Abstract: 
 *
 * Input   :	RunGroupID
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
BEGIN
    DECLARE @errorMsg VARCHAR(400)

    --check that parameters are set
    IF @RunGroupID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @RunGroupID is not set'
        GOTO HandleError
    END


    SELECT PackageRunGroupID,
           PackageID,
           RunGroupID,
           ExecutionOrderNum,
           EnabledFlag,
           ExitActionRefCode,
           CreatedDate,
           CreatedBy,
           LastModifiedDate,
           LastModifiedBy
    FROM   DAF.PackageRunGroup
    WHERE  RunGroupID = @RunGroupID 
			and EnabledFlag = 'Y'
    ORDER BY ExecutionOrderNum asc
    RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return
END



GO
