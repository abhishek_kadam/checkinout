/****** Object:  StoredProcedure [STG].[usp_Load_DataVaultDateEntities]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [STG].[usp_Load_DataVaultDateEntities]
GO
/****** Object:  StoredProcedure [STG].[usp_Load_DataVaultDateEntities]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [STG].[usp_Load_DataVaultDateEntities] @RunGroupId as int, @RecordSource as varchar(100) AS
/* *****************************************************************************
 * Name : STG.usp_Load_DataVaultDate
 *
 * Created :Feb 8th 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Prepare data for Date entities in Staging (SQL server) i.e. Hub_Date,Hub_CalendarDate,Hub_FinancialDate
 *
 * Input : @RunGroupId int --rungroup id 
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			08/02/2016	--		Initial version  
 *
  */

Begin 
DECLARE @StartDate AS DATE
DECLARE @EndDate AS DATE
DECLARE @CurrentDate AS DATE


		/* assign values from daf.parameter ...assign default values to create 40 years data */

		select @StartDate=ISNULL(DateTimeValue,datefromparts(1996,1,1)) from daf.Parameter 
		where RunGroupID=@RunGroupID and NAME ='STARTDATE'

		select @EndDate=ISNULL(DateTimeValue,datefromparts(2036,12,31)) from daf.Parameter 
		where RunGroupID=@RunGroupID and NAME ='ENDDATE'
		
			 

		/* Clear data from staging tables */
		truncate table STG.RDV_HUB_DATE
		truncate table STG.RDV_Sat_Calendardate
		truncate table STG.RDV_Sat_FinancialDate



		SET @CurrentDate=@StartDate

		WHILE @CurrentDate <= @EndDate
		BEGIN 
		   
		   ---Hub_Date 
		   insert into STG.RDV_Hub_Date (Date,LoadDate,RecordSource)
		   SELECT @CurrentDate,getdate(),@RecordSource
		   --- Sat_Calendar
		   insert into STG.RDV_Sat_CalendarDate
		   (Date,
			CalendarQuarter,
			CalendarYear,
			DayNameOfWeek,
			DayOfMonth,
			DayOfWeek,
			DayOfYear,
			LastDayOfMonthInd,
			MonthName,
			MonthOfYear,
			WeekOfYear,
			QuarterName,
			WeekEnding,
			LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
			)
		   SELECT  @CurrentDate,
		   datename(q,@CurrentDate)  CalendarQtr, 
		   year(@CurrentDate) CalendarYr ,
		   datename(dw,@CurrentDate) DayNameOfWeek ,
		   day(@CurrentDate) DayOfMonth,
		   datepart(dw,@CurrentDate) DayOfWeek,
		   datepart(dy,@CurrentDate) DayOfYear,
		   case when day(dateadd(dd,1,@CurrentDate)) =1 then 1 else 0 end  LastDayOfMonthInd,
		   datename(mm,@CurrentDate)  MonthName,

		    datepart(mm,@CurrentDate) MonthOfYear,
			datepart(wk,@CurrentDate) WeekOfYear,
			
			datename(mm,dateadd(mm, case when (3-datepart(mm,@CurrentDate)%3) =3 then 0 else (3-datepart(mm,@CurrentDate)%3) end  ,@CurrentDate))+' Qtr. '+CAST(YEAR(@CurrentDate) AS VARCHAR(4)) QuarterName,

			dateadd(dd,7-datepart(dw,@CurrentDate),@CurrentDate) WeekEnding,
			GETDATE() LoadDate,
			NULL LoadEndDate,
			'Y' IsCurrentFlag,
			@RecordSource RecordSource
				
				

			---Sat_FinancialCalendar
		

			insert into STG.RDV_Sat_FinancialDate
			   (Date,
				FinancialMonth,
				FinancialQuarter,
				FinancialMonthName,
				FinancialMonthSequence,
				FinancialYear,
				LoadDate,
				LoadEndDate,
				IsCurrentFlag,
				RecordSource
				)
			SELECT 
			@CurrentDate Date,
			case when datepart(mm,@CurrentDate) <=6 then  datepart(mm,@CurrentDate)+12 else datepart(mm,@CurrentDate) end   FinancialMonth,
			
			case when datepart(mm,@CurrentDate) between 7 and 12 
             then 'FY'+substring(cast(year(@CurrentDate) as varchar(4)),3,2)+'-'+substring(cast(year(@CurrentDate)+1 as varchar(4)),3,2)+'Q'+
					CAST(case when datepart(qq,@CurrentDate) in ( 1,2 ) then datepart(qq,@CurrentDate)+2 else datepart(qq,@CurrentDate)-2   end AS varchar(4))
			 else 'FY'+substring(cast(year(@CurrentDate)-1 as varchar(4)),3,2)+'-'+substring(cast(year(@CurrentDate) as varchar(4)),3,2)+
			       'Q'+ CAST(case when datepart(qq,@CurrentDate) in ( 1,2 ) then datepart(qq,@CurrentDate)+2 else datepart(qq,@CurrentDate)-2   end AS varchar(4)) end 


			FinancialQuarter,
			
			case when datepart(mm,@CurrentDate) between 7 and 12 
             then substring(datename(mm,@CurrentDate),1,3)+' '+substring(cast(year(@CurrentDate) as varchar(4)),3,2)+'/'+substring(cast(year(@CurrentDate)+1 as varchar(4)),3,2) 
			 else 'FY '+substring(cast(year(@CurrentDate)-1 as varchar(4)),3,2)+'-'+substring(cast(year(@CurrentDate) as varchar(4)),3,2) end FinancialMonthName,
			case when datepart(mm,@CurrentDate) between 7 and 12  
		    then year(@CurrentDate)*100 + (case when datepart(mm,@CurrentDate) <=6 then  datepart(mm,@CurrentDate)+12 else datepart(mm,@CurrentDate)  end ) 
		    else (year(@CurrentDate)-1)*100 + ( case when datepart(mm,@CurrentDate) <=6 then  datepart(mm,@CurrentDate)+12 else datepart(mm,@CurrentDate) end )
		    end  FinancialMonthSequence,
			
			case when datepart(mm,@CurrentDate) between 7 and 12 
             then 'FY'+cast(year(@CurrentDate) as varchar(4))+'-'+cast(year(@CurrentDate)+1 as varchar(4))
			 else 'FY'+cast(year(@CurrentDate)-1 as varchar(4))+'-'+cast(year(@CurrentDate) as varchar(4)) end  FinancialYear,			
			getdate() LoadDate,
			NULL LoadEndDate,
			'Y' IsCurrentFlag,
			@RecordSource RecordSource

		   SET @CurrentDate = DATEADD(DD, 1, @CurrentDate)
		END 
End 






GO
