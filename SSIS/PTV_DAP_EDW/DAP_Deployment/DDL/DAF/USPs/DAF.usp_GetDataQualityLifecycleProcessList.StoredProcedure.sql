/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityLifecycleProcessList]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetDataQualityLifecycleProcessList]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityLifecycleProcessList]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [DAF].[usp_GetDataQualityLifecycleProcessList]
(
    @DataSourceRefCode varchar(50),
    @DataEntityRefCode varchar(50)
)
as

/* *****************************************************************************
 * Name    : usp_GetDataQualityLifecycleProcessList
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(400)

    --check that parameters are set
    if @DataSourceRefCode is null
    begin
        set @errorMsg = 'Parameter @DataSourceRefCode is not set'
        goto HandleError
    end

    if @DataEntityRefCode is null
    begin
        set @errorMsg = 'Parameter @DataEntityRefCode is not set'
        goto HandleError
    end
    
    select dqle.DataQualityLifecycleEntityID,
           dqlp.ImplementationName,
		   dqlp.ImplementationRefCode
    from   DAF.DataQualityLifecycleEntity dqle
	join   DAF.DataQualityLifecycleProcess dqlp on dqle.DataQualityLifecycleProcessID = dqlp.DataQualityLifecycleProcessID
    where  dqle.DataSourceRefCode = @DataSourceRefCode
    and    dqle.DataEntityRefCode = @DataEntityRefCode
	and    dqlp.EnabledFlag = 'Y'
    order  by dqle.ExecutionOrderNum asc
    
    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

end


GO
