/****** Object:  StoredProcedure [DAF].[usp_InsertEventLogCP]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertEventLogCP]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertEventLogCP]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [DAF].[usp_InsertEventLogCP]
(
    @EventClassRefCode varchar(50),
    @SeverityLevelRefCode varchar(50),
    @ServicePackageRunID int = null,
	@ControllingPackageRunID int = null,
    @EventSource varchar(50),
    @EventText varchar(800),
    @ExceptionCode varchar(50) = null,
    @DataEntityRefCode varchar(50) = null,
    @AttributeName varchar(50) = null
)
as

/* *****************************************************************************
 * Name    : usp_InsertEventLogCP
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(800)

    --check that parameters are set
    if @EventClassRefCode is null
    begin
        set @errorMsg = 'Parameter @EventClassRefCode is not set'
        goto HandleError
    end

        if @SeverityLevelRefCode is null
    begin
        set @errorMsg = 'Parameter @SeverityLevelRefCode is not set'
        goto HandleError
    end

        if @EventSource is null
    begin
        set @errorMsg = 'Parameter @EventSource is not set'
        goto HandleError
    end

        if @EventText is null
    begin
        set @errorMsg = 'Parameter @EventText is not set'
        goto HandleError
    end

    insert into DAF.EventLog 
           (EventClassRefCode
           ,SeverityLevelRefCode
           ,ServicePackageRunID
		   ,ControllingPackageRunID
           ,EventSource
           ,EventText
           ,ExceptionCode
           ,DataEntityRefCode
           ,AttributeName
           ,CreatedBy
           ,CreatedDateTime)
     values(@EventClassRefCode
           ,@SeverityLevelRefCode
           ,@ServicePackageRunID
		   ,@ControllingPackageRunID
           ,@EventSource
           ,@EventText
           ,@ExceptionCode
           ,@DataEntityRefCode
           ,@AttributeName
           ,system_user
           ,getdate())

    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return
end



GO
