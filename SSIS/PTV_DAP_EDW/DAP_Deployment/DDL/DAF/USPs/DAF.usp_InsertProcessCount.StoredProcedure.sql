/****** Object:  StoredProcedure [DAF].[usp_InsertProcessCount]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertProcessCount]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertProcessCount]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [DAF].[usp_InsertProcessCount]
(
    @ServicePackageRunID int,
    @Entity varchar(50),
    @RowsProcessed int = 0,
    @RowsFailed int = 0
)
as

/* *****************************************************************************
 * Name    : usp_InsertProcessCount
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(800)

    --check that parameters are set
    if @ServicePackageRunID is null
    begin
        set @errorMsg = 'Parameter @ServicePackageRunID is not set'
        goto HandleError
    end

        if @Entity is null
    begin
        set @errorMsg = 'Parameter @Entity is not set'
        goto HandleError
    end

    insert into DAF.ProcessCount 
           (ServicePackageRunID
           ,Entity
           ,RowsProcessed
           ,RowsFailed)
     values(@ServicePackageRunID
           ,@Entity
           ,@RowsProcessed
           ,@RowsFailed)

    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return
end


GO
