/****** Object:  StoredProcedure [dbo].[usp_cleanup_Generic]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [dbo].[usp_cleanup_Generic]
GO
/****** Object:  StoredProcedure [dbo].[usp_cleanup_Generic]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[usp_cleanup_Generic]
(
    @RunGroupID int
)
as

/* *****************************************************************************
* Name    : usp_cleanup_Generic
*
* Abstract: 
 *
* Input   : 
 *          
 * Output  : 
 *
* Returns : 
 *           
 * *****************************************************************************
*/
begin
      
       update DAF.RunGroup
       set    LastExtractLoadedDateTime = LastExtractDateTime,
              LastModifiedDate = getdate(),
                 LastModifiedBy = SYSTEM_USER
       where  RunGroupID = @RunGroupID
end







GO
