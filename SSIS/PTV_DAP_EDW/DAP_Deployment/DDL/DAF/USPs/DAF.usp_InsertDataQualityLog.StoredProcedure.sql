/****** Object:  StoredProcedure [DAF].[usp_InsertDataQualityLog]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertDataQualityLog]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertDataQualityLog]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [DAF].[usp_InsertDataQualityLog]
(
    @DataQualityRuleID int,
    @RuleVersionNum smallint,
    @ServicePackageRunID int,
    @DataEntityRefCode varchar(50),
    @SeverityLevelRefCode varchar(50),
    @DataSourceRefCode varchar(50),
    @QualityIssueRefCode varchar(50),
    @AttributeName varchar(50),
    @OriginalValue varchar(150),
    @TransformedValue varchar(150) = null,
    @MessageText varchar(500) = null,
	@ReferenceID varchar(50),
	@KeyAttribute1 varchar(50) = null,
	@KeyAttribute2 varchar(50) = null,
	@KeyAttribute3 varchar(50) = null,
	@KeyAttribute4 varchar(50) = null,
	@KeyAttribute5 varchar(50) = null,
	@KeyAttribute6 varchar(50) = null,
	@KeyAttribute7 varchar(50) = null,
	@KeyAttribute8 varchar(50) = null,
	@KeyAttribute9 varchar(50) = null,
	@DataQualityLogId int = null output
)
as

/* *****************************************************************************
 * Name    : usp_InsertDataQualityLog
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(400)

    --check that parameters are set
    if @DataQualityRuleID is null
    begin
        set @errorMsg = 'Parameter @DataQualityRuleID is not set'
        goto HandleError
    end

    if @RuleVersionNum is null
    begin
        set @errorMsg = 'Parameter @RuleVersionNum is not set'
        goto HandleError
    end

    if @ServicePackageRunID is null
    begin
        set @errorMsg = 'Parameter @ServicePackageRunID is not set'
        goto HandleError
    end

    if @DataEntityRefCode is null
    begin
        set @errorMsg = 'Parameter @DataEntityRefCode is not set'
        goto HandleError
    end

    if @SeverityLevelRefCode is null
    begin
        set @errorMsg = 'Parameter @SeverityLevelRefCode is not set'
        goto HandleError
    end

    if @DataSourceRefCode is null
    begin
        set @errorMsg = 'Parameter @DataSourceRefCode is not set'
        goto HandleError
    end

    if @QualityIssueRefCode is null
    begin
        set @errorMsg = 'Parameter @QualityIssueRefCode is not set'
        goto HandleError
    end

    if @AttributeName is null
    begin
        set @errorMsg = 'Parameter @AttributeName is not set'
        goto HandleError
    end


    insert into DataQualityLog
           (DataQualityRuleID
           ,RuleVersionNum
           ,ServicePackageRunID
           ,DataEntityRefCode
           ,SeverityLevelRefCode
           ,DataSourceRefCode
           ,QualityIssueRefCode
           ,AttributeName
           ,OriginalValue
           ,TransformedValue
           ,MessageText
		   ,ReferenceID
           ,CreatedDateTime
           ,CreatedBy
		   ,KeyAttributeValue1
		   ,KeyAttributeValue2
		   ,KeyAttributeValue3
		   ,KeyAttributeValue4
		   ,KeyAttributeValue5
		   ,KeyAttributeValue6
		   ,KeyAttributeValue7
		   ,KeyAttributeValue8
		   ,KeyAttributeValue9
		   )
	values (@DataQualityRuleID
           ,@RuleVersionNum
           ,@ServicePackageRunID
           ,@DataEntityRefCode
           ,@SeverityLevelRefCode
           ,@DataSourceRefCode
           ,@QualityIssueRefCode
           ,@AttributeName
           ,@OriginalValue
           ,@TransformedValue
           ,@MessageText
		   ,(case when @DataQualityRuleID = 0 then null else @ReferenceID end)
           ,getdate()
           ,system_user
		   ,@KeyAttribute1
		   ,@KeyAttribute2
		   ,@KeyAttribute3
		   ,@KeyAttribute4
		   ,@KeyAttribute5
		   ,@KeyAttribute6
		   ,@KeyAttribute7
		   ,@KeyAttribute8
		   ,@KeyAttribute9
		   )

		Select @DataQualityLogId = SCOPE_IDENTITY()

    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return
end



GO
