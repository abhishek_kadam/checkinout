/****** Object:  StoredProcedure [DAF].[usp_InsertEventLog]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertEventLog]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertEventLog]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [DAF].[usp_InsertEventLog]
(
    
	@EventClassRefCode varchar(50),
    @SeverityLevelRefCode varchar(50),
    @ServicePackageRunID int = null,
    @EventSource varchar(50),
    @EventText varchar(800),
    @ExceptionCode varchar(50) = null,
    @DataEntityRefCode varchar(50) = null,
    @AttributeName varchar(50) = null
)
as

/* *****************************************************************************
 * Name    : usp_InsertEventLog
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(800)

    --check that parameters are set
    if @EventClassRefCode is null
    begin
        set @errorMsg = 'Parameter @EventClassRefCode is not set'
        goto HandleError
    end

        if @SeverityLevelRefCode is null
    begin
        set @errorMsg = 'Parameter @SeverityLevelRefCode is not set'
        goto HandleError
    end

        if @EventSource is null
    begin
        set @errorMsg = 'Parameter @EventSource is not set'
        goto HandleError
    end

        if @EventText is null
    begin
        set @errorMsg = 'Parameter @EventText is not set'
        goto HandleError
    end

    insert into DAF.EventLog 
           (EventClassRefCode
           ,SeverityLevelRefCode
           ,ServicePackageRunID
           ,EventSource
           ,EventText
           ,ExceptionCode
           ,DataEntityRefCode
           ,AttributeName
           ,CreatedBy
           ,CreatedDateTime)
     values(@EventClassRefCode
           ,@SeverityLevelRefCode
           ,@ServicePackageRunID
           ,@EventSource
           ,@EventText
           ,@ExceptionCode
           ,@DataEntityRefCode
           ,@AttributeName
           ,system_user
           ,getdate())

    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return
end



GO
