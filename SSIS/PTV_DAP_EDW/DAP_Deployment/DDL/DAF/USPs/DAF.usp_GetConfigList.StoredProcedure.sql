/****** Object:  StoredProcedure [DAF].[usp_GetConfigList]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetConfigList]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetConfigList]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [DAF].[usp_GetConfigList]
(
    @ConfigGroupID INT,
    @ConfigKey VARCHAR(50) = NULL
)
as

/* *****************************************************************************
 * Name    : usp_GetConfigList
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
BEGIN
    DECLARE @errorMsg VARCHAR(400)

    --check that parameters are set
    IF @ConfigGroupID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ConfigGroupID is not set'
        GOTO HandleError
    END

    SELECT ConfigKey,
           ConfigValue
    FROM   DAF.Configuration c
    join   DAF.ConfigurationGroup cg ON c.ConfigGroupID = cg.ConfigGroupID
    WHERE  cg.ConfigGroupID = @ConfigGroupID
    and    (@ConfigKey is null or c.ConfigKey = @ConfigKey)

    RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

END


GO
