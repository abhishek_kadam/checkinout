/****** Object:  StoredProcedure [DAF].[usp_InsertServicePackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_InsertServicePackageRun]
GO
/****** Object:  StoredProcedure [DAF].[usp_InsertServicePackageRun]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [DAF].[usp_InsertServicePackageRun]
(
    @ControllingPackageRunID INT, 
	@PackageID INT,
	@ProgressStatusRefCode VARCHAR(50),
	@ServicePackageRunID INT OUTPUT
)
as
/* *****************************************************************************
 * Name    : Usp_InsertServicePackageRun
 *
 * Abstract: This function will be used by service packages to insert a new record into the ServicePackageRun table.
			It creates a new ServicePackageRunID by getting the next ServicePackageRun sequence number.

 * Input   : ControllingPackageRunID
 *          
 * Output  : Returns the ServicePackageRunID
 *
 * Returns : 
 *           
 * *****************************************************************************
 */

BEGIN
    DECLARE 
	@errorMsg VARCHAR(400),
	@CntServicePackageRun INT

    --check that parameters are set
    IF @ControllingPackageRunID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ControllingPackageRunID is not set'
        GOTO HandleError
    END

	IF @PackageID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @PackageID is not set'
        GOTO HandleError
    END

	IF @ProgressStatusRefCode IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ProgressStatusRefCode is not set'
        GOTO HandleError
    END
	
	SET NOCOUNT ON

    BEGIN TRAN

	    SELECT @CntServicePackageRun = ISNULL(MAX(ServicePackageRunID), 0) + 1 FROM DAF.ServicePackageRun

	    INSERT INTO ServicePackageRun
	    (ServicePackageRunID, 
	    ControllingPackageRunID,
	    PackageID,
	    ProgressStatusRefCode,
	    StartDateTime) 
	    VALUES 
	    (@CntServicePackageRun, 
	    @ControllingPackageRunID,
	    @PackageID,
	    @ProgressStatusRefCode,
	    GETDATE())

    COMMIT TRAN
	
	SET @ServicePackageRunID = @CntServicePackageRun 

	RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        RETURN
END

GO
