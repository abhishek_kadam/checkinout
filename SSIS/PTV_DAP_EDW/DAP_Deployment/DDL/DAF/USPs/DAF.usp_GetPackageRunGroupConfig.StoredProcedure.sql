/****** Object:  StoredProcedure [DAF].[usp_GetPackageRunGroupConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetPackageRunGroupConfig]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetPackageRunGroupConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DAF].[usp_GetPackageRunGroupConfig]
(
    @PackageRunGroupID INT
)
as

/* *****************************************************************************
 * Name    : usp_GetPackageRunGroupConfig
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
BEGIN
    DECLARE @errorMsg VARCHAR(400)

	--check that parameters are set
	IF @PackageRunGroupID IS NULL
	BEGIN
		SET @errorMsg = 'Parameter @PackageRunGroupID is not set'
		GOTO HandleError
	END

	SELECT isnull(LastExtractLoadedDateTime, '1900-01-01') as LastExtractLoadedDateTime,
		   rg.DataSourceRefCode,
		   prg.DataEntityRefCode,
		   rg.ExtractSQL,
		   prg.PackageRunGroupConfiguration1


	FROM   DAF.RunGroup rg
	join   DAF.PackageRunGroup prg on rg.RunGroupID = prg.RunGroupID
	WHERE  prg.PackageRunGroupID = @PackageRunGroupID

	RETURN

	HandleError:
		raiserror(@errorMsg, 16, 1)
		return

END

GO
