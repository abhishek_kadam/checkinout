/****** Object:  StoredProcedure [DAF].[usp_RecordJobSuccess]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_RecordJobSuccess]
GO
/****** Object:  StoredProcedure [DAF].[usp_RecordJobSuccess]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [DAF].[usp_RecordJobSuccess]
	( @pJobNumber int	)
AS

/* *****************************************************************************
 * Name    : DAF.usp_RecordJobCompletion
 *
 * Created : 12th February 2012
 * Author  : Shaun Wadsworth
 *
 * Abstract: Records the completion of a SQL Agent run.
 *
 * Input   : @pJobId	  = The job Id that is running
 *          
 * Output  : None
 *
 * Returns : 0 = Success, -1 Error
 *           
 * *****************************************************************************
 * Change History
 * Author           Version   Date         CR/DF   Description
 * ------           -------   ----------  -------  -----------
 * Shaun Wadsworth      1.0   20/12/2012    --     Initial version
 */

SET NOCOUNT ON
BEGIN

	 UPDATE DAF.JobRun 
		set JobEndDate = getdate(),
			ExitStatus = 'Success',
			ControllingPackageRunId = (select max(ControllingPackageRunId) 
										   from ControllingPackageRun CPR 
											inner join DAF.JobStep J on CPR.RunGroupID = J.RunGroupId
										   where J.JobNumber = JobRun.JobNumber)
	  where JobNumber = @pJobNumber
	    and JobStartDate = (select max(JobStartDate) from DAF.JobRun J1 where J1.JobNumber = JobRun.JobNumber);

END;


GO
