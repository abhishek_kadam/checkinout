/****** Object:  StoredProcedure [STG].[usp_Load_DataVaultTimeEntities]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [STG].[usp_Load_DataVaultTimeEntities]
GO
/****** Object:  StoredProcedure [STG].[usp_Load_DataVaultTimeEntities]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [STG].[usp_Load_DataVaultTimeEntities] 'DW'


CREATE PROC [STG].[usp_Load_DataVaultTimeEntities] @RecordSource as Varchar(100) AS
/* *****************************************************************************
 * Name : STG.usp_Load_DataVaultTimeEntities
 *
 * Created :Feb 8th 2016
 * Author : VIJENDER THAKUR
 *
 * Abstract: Prepare data for Time entities in Staging (SQL server) i.e. Hub_Time,Sat_Time
 *
 * Input : @RecordSource --data source referenccces from DAF.DataSourceRef
 *		   
 * 
 * Output :na
 *
 * 
 * *****************************************************************************
 * Change History
 * Author			Version		Date		CR/DF	Description
 * ------			-------		----------	-------	-----------
 * VIJENDER THAKUR	1.0			18/02/2016	--		Initial version  
 *
  */

Begin 
DECLARE @Second AS time = cast( '00:00:00' as time(0))

		/* Clear data from staging tables */
		truncate table STG.RDV_HUB_TIME
		truncate table STG.RDV_SAT_TIME
		


		WHILE 1=1
		BEGIN 
		   
		   PRINT @Second
		   ---Hub_Time
		   insert into STG.RDV_Hub_Time 
		   (Time,
			LoadDate,
			RecordSource)
			VALUES (@Second,getdate(),@RecordSource)

		   --- Sat_Time
		   insert into STG.RDV_Sat_Time
		   (Time,
			TimeHour,
			TimeMinute,
			TimeSecond,
			AmPm,
			FifteenMinuteTimeBlock,
			FiveMinuteTimeBlock,
			LoadDate,
			LoadEndDate,
			IsCurrentFlag,
			RecordSource
			)
		   SELECT 
		    @Second Time,
			datepart(hh,@Second) TimeHour,
			datepart(MI,@Second) TimeMinute,
			datepart(ss,@Second)  TimeSecond,
			case when @Second<=CAST('11:59:59'AS TIME) then 'AM' else 'PM' end AmPm,
			'15 minute block commenced '+cast(cast(dateadd(ss, -1*datepart(ss,@Second)  ,dateadd(mi,-1*datepart(mi,@Second)%15,@Second)) as time(0)) as varchar(20)) FifteenMinuteTimeBlock,
			'5 minute block commenced '+cast(cast(dateadd(ss, -1*datepart(ss,@Second)  ,dateadd(mi,-1*datepart(mi,@Second)%5,@Second)) as time(0)) as varchar(20)) FiveMinuteTimeBlock,
			GETDATE() LoadDate,
			NULL LoadEndDate,
			'Y' IsCurrentFlag,
			@RecordSource RecordSource

		   
		   IF(@Second=CAST('23:59:59' AS TIME))
		   break

		   SET @Second=dateadd(ss,1,@Second)
		
		END 
End 


GO
