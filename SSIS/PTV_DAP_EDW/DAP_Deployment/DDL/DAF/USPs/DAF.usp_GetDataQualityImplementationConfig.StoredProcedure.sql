/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityImplementationConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetDataQualityImplementationConfig]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityImplementationConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [DAF].[usp_GetDataQualityImplementationConfig]
(
    @DataQualityEntityID int
)
as

/* *****************************************************************************
 * Name    : usp_GetDataQualityImplementationConfig
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
BEGIN
    select dqe.DataQualityRuleID,
           dqe.DataEntityRefCode,
           dqe.DataSourceRefCode,
           dqr.RuleVersionNum,
           dqr.SeverityLevelRefCode,
		   dqr.Description
    from   DAF.DataQualityEntity dqe
    join   DAF.DataQualityRule dqr on dqr.DataQualityRuleID = dqe.DataQualityRuleID
    where  dqe.DataQualityEntityID = @DataQualityEntityID

END

GO
