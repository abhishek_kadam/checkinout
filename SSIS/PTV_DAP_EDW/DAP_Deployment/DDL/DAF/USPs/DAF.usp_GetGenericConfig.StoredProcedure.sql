/****** Object:  StoredProcedure [DAF].[usp_GetGenericConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetGenericConfig]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetGenericConfig]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [DAF].[usp_GetGenericConfig]
(
    @ConfigGroupID INT
)
as

/* *****************************************************************************
* Name    : usp_GetGenericConfig
*
* Abstract: 
 *
* Input   : 
 *          
 * Output  : 
 *
* Returns : 
 *           
 * *****************************************************************************
*/
BEGIN
    DECLARE @errorMsg VARCHAR(400)

    --check that parameters are set
    IF @ConfigGroupID IS NULL
    BEGIN
        SET @errorMsg = 'Parameter @ConfigGroupID is not set'
        GOTO HandleError
    END

    SELECT
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'Data_Lake') AS Data_Lake,
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'STAGING_ADONET') AS STAGING_ADONET,
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'STAGING_OLEDB') AS STAGING_OLEDB,
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'RDV_ADONET') AS RDV_ADONET,
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'RDV_OLEDB') AS RDV_OLEDB,
		(SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'DATALAKE_OLEDB') AS DATALAKE_OLEDB,
		(SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'DATALAKE_DB_PASSWORD') AS DATALAKE_DB_PASSWORD,
		(SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'RDV_DB_PASSWORD') AS RDV_DB_PASSWORD,
		(SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'STAGE_DB_PASSWORD') AS STAGE_DB_PASSWORD,		
		(SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'DAP_REFERENCE_ADONET') AS DAP_REFERENCE_ADONET,
        (SELECT ConfigValue FROM DAF.Configuration WHERE ConfigGroupID = @ConfigGroupID AND ConfigKey = 'DAP_REFERENCE_OLEDB') AS DAP_REFERENCE_OLEDB



              ----** generalize this code
       RETURN

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

END






GO
