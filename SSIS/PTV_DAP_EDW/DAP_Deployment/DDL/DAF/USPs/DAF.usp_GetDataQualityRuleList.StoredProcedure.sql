/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityRuleList]    Script Date: 2/23/2016 5:31:07 PM ******/
DROP PROCEDURE [DAF].[usp_GetDataQualityRuleList]
GO
/****** Object:  StoredProcedure [DAF].[usp_GetDataQualityRuleList]    Script Date: 2/23/2016 5:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [DAF].[usp_GetDataQualityRuleList]
(
    @DataSourceRefCode varchar(50),
    @DataEntityRefCode varchar(50)
)
as

/* *****************************************************************************
 * Name    : usp_GetDataQualityRuleList
 *
 * Abstract: 
 *
 * Input   : 
 *          
 * Output  : 
 *
 * Returns : 
 *           
 * *****************************************************************************
 */
begin
    declare @errorMsg varchar(400)

    --check that parameters are set
    if @DataSourceRefCode is null
    begin
        set @errorMsg = 'Parameter @DataSourceRefCode is not set'
        goto HandleError
    end

    if @DataEntityRefCode is null
    begin
        set @errorMsg = 'Parameter @DataEntityRefCode is not set'
        goto HandleError
    end
    
    select dqe.DataQualityEntityID,
           dqr.ImplementationName,
		   dqr.ImplementationRefCode,
		   IsNull(dqr.RuleImplimentationType, 'STAGING') RuleImplimentationType
    from   DAF.DataQualityEntity dqe
	join   DAF.DataQualityRule dqr on dqe.DataQualityRuleID = dqr.DataQualityRuleID
    where  dqe.DataSourceRefCode = @DataSourceRefCode
    and    dqe.DataEntityRefCode = @DataEntityRefCode
	and    dqr.EnabledFlag = 'Y' and dqe.DataQualityProcessFlag = 'Y'
    order  by dqe.ExecutionOrderNum asc
    
    return

    HandleError:
        raiserror(@errorMsg, 16, 1)
        return

end


GO
