ALTER TABLE [DAF].[DataQualityRuleHistory] DROP CONSTRAINT [FK_DataQualityRuleHistory_SeverityLevelRef]
GO
ALTER TABLE [DAF].[DataQualityRuleHistory] DROP CONSTRAINT [FK_DataQualityRuleHistory_ImplementationRef]
GO
ALTER TABLE [DAF].[DataQualityRuleHistory] DROP CONSTRAINT [FK_DataQualityRuleHistory_DataQualityRule]
GO
DROP TABLE [DAF].[DataQualityRuleHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityRuleHistory](
	[DataQualityRuleHistoryID] [int] NOT NULL,
	[DataQualityRuleID] [int] NOT NULL,
	[RuleVersionNum] [smallint] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[ImplementationRefCode] [varchar](50) NOT NULL,
	[ImplementationName] [varchar](50) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataQualityRuleHistory] PRIMARY KEY CLUSTERED 
(
	[DataQualityRuleHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityRuleHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityRuleHistory_DataQualityRule] FOREIGN KEY([DataQualityRuleID])
REFERENCES [DAF].[DataQualityRule] ([DataQualityRuleID])
GO
ALTER TABLE [DAF].[DataQualityRuleHistory] CHECK CONSTRAINT [FK_DataQualityRuleHistory_DataQualityRule]
GO
ALTER TABLE [DAF].[DataQualityRuleHistory]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityRuleHistory_ImplementationRef] FOREIGN KEY([ImplementationRefCode])
REFERENCES [DAF].[ImplementationRef] ([ImplementationRefCode])
GO
ALTER TABLE [DAF].[DataQualityRuleHistory] CHECK CONSTRAINT [FK_DataQualityRuleHistory_ImplementationRef]
GO
ALTER TABLE [DAF].[DataQualityRuleHistory]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityRuleHistory_SeverityLevelRef] FOREIGN KEY([SeverityLevelRefCode])
REFERENCES [DAF].[SeverityLevelRef] ([SeverityLevelRefCode])
GO
ALTER TABLE [DAF].[DataQualityRuleHistory] CHECK CONSTRAINT [FK_DataQualityRuleHistory_SeverityLevelRef]
GO
