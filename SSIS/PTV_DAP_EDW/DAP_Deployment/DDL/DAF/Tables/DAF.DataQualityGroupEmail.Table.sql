ALTER TABLE [DAF].[DataQualityGroupEmail] DROP CONSTRAINT [chk_Priority]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] DROP CONSTRAINT [FK_DataQualityGroupEmail_DataQualityGroup]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] DROP CONSTRAINT [DF__DataQuali__Prior__403A8C7D]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] DROP CONSTRAINT [DF__DataQuali__Attac__3F466844]
GO
DROP TABLE [DAF].[DataQualityGroupEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityGroupEmail](
	[DataQualityGroupEmailID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityGroupID] [int] NOT NULL,
	[OrganisationalUnitCode] [varchar](4) NULL,
	[OrganisationalUnitName] [varchar](100) NULL,
	[GroupEmailAddress] [varchar](256) NULL,
	[AttachFileFormat] [varchar](10) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_DataQualityGroupEmail] PRIMARY KEY CLUSTERED 
(
	[DataQualityGroupEmailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] ADD  DEFAULT ('PDF') FOR [AttachFileFormat]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] ADD  DEFAULT ('Normal') FOR [Priority]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityGroupEmail_DataQualityGroup] FOREIGN KEY([DataQualityGroupID])
REFERENCES [DAF].[DataQualityGroup] ([DataQualityGroupID])
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] CHECK CONSTRAINT [FK_DataQualityGroupEmail_DataQualityGroup]
GO
ALTER TABLE [DAF].[DataQualityGroupEmail]  WITH CHECK ADD  CONSTRAINT [chk_Priority] CHECK  (([Priority]='Low' OR [Priority]='Normal' OR [Priority]='High'))
GO
ALTER TABLE [DAF].[DataQualityGroupEmail] CHECK CONSTRAINT [chk_Priority]
GO
