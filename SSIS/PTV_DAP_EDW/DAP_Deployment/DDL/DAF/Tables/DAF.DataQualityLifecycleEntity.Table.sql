ALTER TABLE [DAF].[DataQualityLifecycleEntity] DROP CONSTRAINT [FK_DataQualityLifecycleEntity_DataSourceRef]
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity] DROP CONSTRAINT [FK_DataQualityLifecycleEntity_DataQualityLifecycleProcess]
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity] DROP CONSTRAINT [FK_DataQualityLifecycleEntity_DataEntityRef]
GO
DROP TABLE [DAF].[DataQualityLifecycleEntity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLifecycleEntity](
	[DataQualityLifecycleEntityID] [int] IDENTITY(1,1) NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[DataQualityLifecycleProcessID] [int] NOT NULL,
	[ExecutionOrderNum] [smallint] NOT NULL,
 CONSTRAINT [PK_DataQualityLifecycleEntity] PRIMARY KEY CLUSTERED 
(
	[DataQualityLifecycleEntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityLifecycleEntity_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity] CHECK CONSTRAINT [FK_DataQualityLifecycleEntity_DataEntityRef]
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityLifecycleEntity_DataQualityLifecycleProcess] FOREIGN KEY([DataQualityLifecycleProcessID])
REFERENCES [DAF].[DataQualityLifecycleProcess] ([DataQualityLifecycleProcessID])
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity] CHECK CONSTRAINT [FK_DataQualityLifecycleEntity_DataQualityLifecycleProcess]
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLifecycleEntity_DataSourceRef] FOREIGN KEY([DataSourceRefCode])
REFERENCES [DAF].[DataSourceRef] ([DataSourceRefCode])
GO
ALTER TABLE [DAF].[DataQualityLifecycleEntity] CHECK CONSTRAINT [FK_DataQualityLifecycleEntity_DataSourceRef]
GO
