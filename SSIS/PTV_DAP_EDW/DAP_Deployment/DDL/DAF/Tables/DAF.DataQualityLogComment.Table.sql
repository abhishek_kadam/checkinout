ALTER TABLE [DAF].[DataQualityLogComment] DROP CONSTRAINT [FK_DataQualityLog_DataQualityLogComment]
GO
DROP TABLE [DAF].[DataQualityLogComment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLogComment](
	[DataQualityLogCommentID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityLogID] [int] NOT NULL,
	[Comment] [varchar](1024) NOT NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_DataQualityIssueComment] PRIMARY KEY CLUSTERED 
(
	[DataQualityLogCommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityLogComment]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_DataQualityLogComment] FOREIGN KEY([DataQualityLogID])
REFERENCES [DAF].[DataQualityLog] ([DataQualityLogID])
GO
ALTER TABLE [DAF].[DataQualityLogComment] CHECK CONSTRAINT [FK_DataQualityLog_DataQualityLogComment]
GO
