/****** Object:  Table [DAF].[JobStep]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[JobStep]
GO
/****** Object:  Table [DAF].[JobStep]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[JobStep](
	[JobStepNumber] [int] IDENTITY(1,1) NOT NULL,
	[JobNumber] [int] NOT NULL,
	[StepSequence] [smallint] NOT NULL,
	[RunGroupId] [int] NOT NULL,
	[FailureAction] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
