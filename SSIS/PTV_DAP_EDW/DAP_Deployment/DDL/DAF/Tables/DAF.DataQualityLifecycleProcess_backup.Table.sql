DROP TABLE [DAF].[DataQualityLifecycleProcess_backup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLifecycleProcess_backup](
	[DataQualityLifecycleProcessID] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ImplementationRefCode] [varchar](50) NOT NULL,
	[ImplementationName] [varchar](100) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
