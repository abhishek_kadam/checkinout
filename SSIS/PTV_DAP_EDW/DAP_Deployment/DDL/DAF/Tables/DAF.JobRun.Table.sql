/****** Object:  Table [DAF].[JobRun]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[JobRun]
GO
/****** Object:  Table [DAF].[JobRun]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[JobRun](
	[JobRunId] [int] IDENTITY(1,1) NOT NULL,
	[JobNumber] [int] NOT NULL,
	[JobStartDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[ControllingPackageRunId] [int] NULL,
	[ExitStatus] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
