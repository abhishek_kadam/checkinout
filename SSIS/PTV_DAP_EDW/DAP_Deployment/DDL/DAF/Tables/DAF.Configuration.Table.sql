ALTER TABLE [DAF].[Configuration] DROP CONSTRAINT [FK_Configuration_ConfigurationGroup]
GO
DROP TABLE [DAF].[Configuration]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[Configuration](
	[ConfigGroupID] [int] NOT NULL,
	[ConfigKey] [varchar](50) NOT NULL,
	[ConfigValue] [varchar](1000) NOT NULL,
	[Description] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigGroupID] ASC,
	[ConfigKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Configuration_ConfigurationGroup] FOREIGN KEY([ConfigGroupID])
REFERENCES [DAF].[ConfigurationGroup] ([ConfigGroupID])
GO
ALTER TABLE [DAF].[Configuration] CHECK CONSTRAINT [FK_Configuration_ConfigurationGroup]
GO
