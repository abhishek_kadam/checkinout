DROP TABLE [DAF].[ConfigurationGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[ConfigurationGroup](
	[ConfigGroupID] [int] NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[Description] [varchar](150) NOT NULL,
 CONSTRAINT [PK_ConfigurationGroup] PRIMARY KEY CLUSTERED 
(
	[ConfigGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
