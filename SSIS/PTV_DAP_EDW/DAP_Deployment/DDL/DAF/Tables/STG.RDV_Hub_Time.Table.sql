/****** Object:  Table [STG].[RDV_Hub_Time]    Script Date: 2/23/2016 6:48:29 PM ******/
DROP TABLE [STG].[RDV_Hub_Time]
GO
/****** Object:  Table [STG].[RDV_Hub_Time]    Script Date: 2/23/2016 6:48:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[RDV_Hub_Time](
	[Time] [time](7) NULL,
	[LoadDate] [datetime2](7) NULL,
	[RecordSource] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
