ALTER TABLE [DAF].[PostLoadEntity] DROP CONSTRAINT [FK_PostLoadEntity_PostLoadDerivation]
GO
ALTER TABLE [DAF].[PostLoadEntity] DROP CONSTRAINT [FK_PostLoadEntity_DataSourceRef]
GO
ALTER TABLE [DAF].[PostLoadEntity] DROP CONSTRAINT [FK_PostLoadEntity_DataEntityRef]
GO
/****** Object:  Table [DAF].[PostLoadEntity]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[PostLoadEntity]
GO
/****** Object:  Table [DAF].[PostLoadEntity]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[PostLoadEntity](
	[PostLoadEntityID] [int] IDENTITY(1,1) NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[PostLoadDerivationID] [int] NOT NULL,
	[ExecutionOrderNum] [smallint] NOT NULL,
 CONSTRAINT [PK_PostLoadEntity] PRIMARY KEY CLUSTERED 
(
	[PostLoadEntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[PostLoadEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_PostLoadEntity_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[PostLoadEntity] CHECK CONSTRAINT [FK_PostLoadEntity_DataEntityRef]
GO
ALTER TABLE [DAF].[PostLoadEntity]  WITH CHECK ADD  CONSTRAINT [FK_PostLoadEntity_DataSourceRef] FOREIGN KEY([DataSourceRefCode])
REFERENCES [DAF].[DataSourceRef] ([DataSourceRefCode])
GO
ALTER TABLE [DAF].[PostLoadEntity] CHECK CONSTRAINT [FK_PostLoadEntity_DataSourceRef]
GO
ALTER TABLE [DAF].[PostLoadEntity]  WITH CHECK ADD  CONSTRAINT [FK_PostLoadEntity_PostLoadDerivation] FOREIGN KEY([PostLoadDerivationID])
REFERENCES [DAF].[PostLoadDerivation] ([PostLoadDerivationID])
GO
ALTER TABLE [DAF].[PostLoadEntity] CHECK CONSTRAINT [FK_PostLoadEntity_PostLoadDerivation]
GO
