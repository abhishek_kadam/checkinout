ALTER TABLE [DAF].[Parameter] DROP CONSTRAINT [FK_Parameters_RunGroup]
GO
ALTER TABLE [DAF].[Parameter] DROP CONSTRAINT [FK_Parameter_ParameterTypeRef]
GO
/****** Object:  Table [DAF].[Parameter]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[Parameter]
GO
/****** Object:  Table [DAF].[Parameter]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[Parameter](
	[ParameterID] [int] IDENTITY(1,1) NOT NULL,
	[RunGroupID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ParameterTypeRefCode] [varchar](50) NOT NULL,
	[Description] [varchar](200) NULL,
	[StringValue] [varchar](200) NULL,
	[IntegerValue] [int] NULL,
	[DateTimeValue] [datetime] NULL,
 CONSTRAINT [PK_Parameters] PRIMARY KEY CLUSTERED 
(
	[ParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[Parameter]  WITH CHECK ADD  CONSTRAINT [FK_Parameter_ParameterTypeRef] FOREIGN KEY([ParameterTypeRefCode])
REFERENCES [DAF].[ParameterTypeRef] ([ParameterTypeRefCode])
GO
ALTER TABLE [DAF].[Parameter] CHECK CONSTRAINT [FK_Parameter_ParameterTypeRef]
GO
ALTER TABLE [DAF].[Parameter]  WITH CHECK ADD  CONSTRAINT [FK_Parameters_RunGroup] FOREIGN KEY([RunGroupID])
REFERENCES [DAF].[RunGroup] ([RunGroupID])
GO
ALTER TABLE [DAF].[Parameter] CHECK CONSTRAINT [FK_Parameters_RunGroup]
GO
