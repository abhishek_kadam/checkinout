DROP TABLE [DAF].[DataQualityLifecycleEntity_backup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLifecycleEntity_backup](
	[DataQualityLifecycleEntityID] [int] IDENTITY(1,1) NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[DataQualityLifecycleProcessID] [int] NOT NULL,
	[ExecutionOrderNum] [smallint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
