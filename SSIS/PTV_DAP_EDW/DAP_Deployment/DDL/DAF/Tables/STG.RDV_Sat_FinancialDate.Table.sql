/****** Object:  Table [STG].[RDV_Sat_FinancialDate]    Script Date: 2/23/2016 6:48:29 PM ******/
DROP TABLE [STG].[RDV_Sat_FinancialDate]
GO
/****** Object:  Table [STG].[RDV_Sat_FinancialDate]    Script Date: 2/23/2016 6:48:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[RDV_Sat_FinancialDate](
	[Date] [date] NULL,
	[FinancialMonth] [int] NULL,
	[FinancialQuarter] [varchar](50) NULL,
	[FinancialMonthName] [varchar](50) NULL,
	[FinancialMonthSequence] [int] NULL,
	[FinancialYear] [varchar](50) NULL,
	[LoadDate] [datetime2](7) NULL,
	[LoadEndDate] [datetime2](7) NULL,
	[IsCurrentFlag] [varchar](1) NULL,
	[RecordSource] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
