ALTER TABLE [DAF].[PackageRunGroup] DROP CONSTRAINT [FK_PackageRunGroup_RunGroup]
GO
ALTER TABLE [DAF].[PackageRunGroup] DROP CONSTRAINT [FK_PackageRunGroup_Package]
GO
ALTER TABLE [DAF].[PackageRunGroup] DROP CONSTRAINT [FK_PackageRunGroup_ExitActionRef]
GO
ALTER TABLE [DAF].[PackageRunGroup] DROP CONSTRAINT [FK_PackageRunGroup_DataEntityRef]
GO
/****** Object:  Table [DAF].[PackageRunGroup]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[PackageRunGroup]
GO
/****** Object:  Table [DAF].[PackageRunGroup]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[PackageRunGroup](
	[PackageRunGroupID] [int] NOT NULL,
	[PackageID] [int] NOT NULL,
	[RunGroupID] [int] NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[ExecutionOrderNum] [smallint] NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[ExitActionRefCode] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[PackageRunGroupConfiguration1] [varchar](100) NULL,
	[PackageRunGroupConfiguration2] [varchar](100) NULL,
 CONSTRAINT [PK_PackageRunGroup] PRIMARY KEY CLUSTERED 
(
	[PackageRunGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[PackageRunGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_PackageRunGroup_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[PackageRunGroup] CHECK CONSTRAINT [FK_PackageRunGroup_DataEntityRef]
GO
ALTER TABLE [DAF].[PackageRunGroup]  WITH CHECK ADD  CONSTRAINT [FK_PackageRunGroup_ExitActionRef] FOREIGN KEY([ExitActionRefCode])
REFERENCES [DAF].[ExitActionRef] ([ExitActionRefCode])
GO
ALTER TABLE [DAF].[PackageRunGroup] CHECK CONSTRAINT [FK_PackageRunGroup_ExitActionRef]
GO
ALTER TABLE [DAF].[PackageRunGroup]  WITH CHECK ADD  CONSTRAINT [FK_PackageRunGroup_Package] FOREIGN KEY([PackageID])
REFERENCES [DAF].[Package] ([PackageID])
GO
ALTER TABLE [DAF].[PackageRunGroup] CHECK CONSTRAINT [FK_PackageRunGroup_Package]
GO
ALTER TABLE [DAF].[PackageRunGroup]  WITH CHECK ADD  CONSTRAINT [FK_PackageRunGroup_RunGroup] FOREIGN KEY([RunGroupID])
REFERENCES [DAF].[RunGroup] ([RunGroupID])
GO
ALTER TABLE [DAF].[PackageRunGroup] CHECK CONSTRAINT [FK_PackageRunGroup_RunGroup]
GO
