/****** Object:  Table [DAF].[ParameterTypeRef]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[ParameterTypeRef]
GO
/****** Object:  Table [DAF].[ParameterTypeRef]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[ParameterTypeRef](
	[ParameterTypeRefCode] [varchar](50) NOT NULL,
	[Description] [varchar](150) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ParameterTypeRef] PRIMARY KEY CLUSTERED 
(
	[ParameterTypeRefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
