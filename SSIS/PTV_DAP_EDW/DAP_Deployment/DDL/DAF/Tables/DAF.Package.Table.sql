/****** Object:  Table [DAF].[Package]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[Package]
GO
/****** Object:  Table [DAF].[Package]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[Package](
	[PackageID] [int] NOT NULL,
	[PackageName] [varchar](100) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CleanupStoredProcedureName] [varchar](100) NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
