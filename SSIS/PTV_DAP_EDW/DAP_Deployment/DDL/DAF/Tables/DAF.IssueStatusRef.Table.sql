ALTER TABLE [DAF].[IssueStatusRef] DROP CONSTRAINT [DF__IssueStat__Displ__66603565]
GO
ALTER TABLE [DAF].[IssueStatusRef] DROP CONSTRAINT [DF__IssueStat__Displ__656C112C]
GO
/****** Object:  Table [DAF].[IssueStatusRef]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[IssueStatusRef]
GO
/****** Object:  Table [DAF].[IssueStatusRef]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[IssueStatusRef](
	[IssueStatusRefCode] [varchar](20) NOT NULL,
	[ShortName] [varchar](50) NULL,
	[Description] [varchar](256) NULL,
	[DisplayInDQIMFlag] [char](1) NULL,
	[DisplayInDQIMEditFlag] [char](1) NULL,
	[DisplayInDQIMEditMatchingExceptionFlag] [char](1) NULL,
 CONSTRAINT [PK_IssueStatusRef] PRIMARY KEY CLUSTERED 
(
	[IssueStatusRefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[IssueStatusRef] ADD  DEFAULT ('Y') FOR [DisplayInDQIMFlag]
GO
ALTER TABLE [DAF].[IssueStatusRef] ADD  DEFAULT ('Y') FOR [DisplayInDQIMEditFlag]
GO
