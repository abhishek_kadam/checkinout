ALTER TABLE [DAF].[RunGroup] DROP CONSTRAINT [FK_RunGroup_DataSourceRef]
GO
/****** Object:  Table [DAF].[RunGroup]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[RunGroup]
GO
/****** Object:  Table [DAF].[RunGroup]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[RunGroup](
	[RunGroupID] [int] NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[CleanupStoredProcedureName] [varchar](100) NULL,
	[LastExtractDateTime] [datetime] NULL,
	[LastExtractLoadedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[ExtractSQL] [nvarchar](1000) NULL,
 CONSTRAINT [PK_RunGroup] PRIMARY KEY CLUSTERED 
(
	[RunGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[RunGroup]  WITH CHECK ADD  CONSTRAINT [FK_RunGroup_DataSourceRef] FOREIGN KEY([DataSourceRefCode])
REFERENCES [DAF].[DataSourceRef] ([DataSourceRefCode])
GO
ALTER TABLE [DAF].[RunGroup] CHECK CONSTRAINT [FK_RunGroup_DataSourceRef]
GO
