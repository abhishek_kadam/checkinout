ALTER TABLE [DAF].[DataQualityThreshold] DROP CONSTRAINT [FK_DataQualityThreshold_SeverityLevelRef]
GO
DROP TABLE [DAF].[DataQualityThreshold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityThreshold](
	[DataQualityThresholdID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](150) NULL,
	[ShortName] [varchar](50) NULL,
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[ThresholdLevel1] [int] NOT NULL,
	[ThresholdLevel2] [int] NOT NULL,
	[ThresholdLevel3] [int] NOT NULL,
 CONSTRAINT [PK_DataQualityThreshhold] PRIMARY KEY CLUSTERED 
(
	[DataQualityThresholdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityThreshold]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityThreshold_SeverityLevelRef] FOREIGN KEY([SeverityLevelRefCode])
REFERENCES [DAF].[SeverityLevelRef] ([SeverityLevelRefCode])
GO
ALTER TABLE [DAF].[DataQualityThreshold] CHECK CONSTRAINT [FK_DataQualityThreshold_SeverityLevelRef]
GO
