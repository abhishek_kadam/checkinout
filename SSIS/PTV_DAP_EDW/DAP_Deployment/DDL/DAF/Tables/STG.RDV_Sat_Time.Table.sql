/****** Object:  Table [STG].[RDV_Sat_Time]    Script Date: 2/23/2016 6:48:29 PM ******/
DROP TABLE [STG].[RDV_Sat_Time]
GO
/****** Object:  Table [STG].[RDV_Sat_Time]    Script Date: 2/23/2016 6:48:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[RDV_Sat_Time](
	[Time] [time](7) NULL,
	[TimeHour] [int] NULL,
	[TimeMinute] [int] NULL,
	[TimeSecond] [int] NULL,
	[AmPm] [varchar](50) NULL,
	[FifteenMinuteTimeBlock] [varchar](50) NULL,
	[FiveMinuteTimeBlock] [varchar](50) NULL,
	[LoadDate] [datetime2](7) NULL,
	[LoadEndDate] [datetime2](7) NULL,
	[IsCurrentFlag] [varchar](1) NULL,
	[RecordSource] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
