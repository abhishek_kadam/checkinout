/****** Object:  Table [DAF].[SeverityLevelRef]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[SeverityLevelRef]
GO
/****** Object:  Table [DAF].[SeverityLevelRef]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[SeverityLevelRef](
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[Description] [varchar](150) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[SeverityLevel] [tinyint] NULL,
	[DisplayInDQIMFlag] [char](1) NULL DEFAULT ('Y'),
 CONSTRAINT [PK_EventLevel] PRIMARY KEY CLUSTERED 
(
	[SeverityLevelRefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
