ALTER TABLE [DAF].[DataQualityLogDetail] DROP CONSTRAINT [FK_DataQualityLog_DataQualityLogDetail]
GO
DROP TABLE [DAF].[DataQualityLogDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLogDetail](
	[DataQualityLogDetailID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityLogID] [int] NOT NULL,
	[FieldName] [varchar](50) NOT NULL,
	[ValueNew] [varchar](200) NULL,
	[ValueExisting] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DataQualityLogDetail] PRIMARY KEY CLUSTERED 
(
	[DataQualityLogDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityLogDetail]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_DataQualityLogDetail] FOREIGN KEY([DataQualityLogID])
REFERENCES [DAF].[DataQualityLog] ([DataQualityLogID])
GO
ALTER TABLE [DAF].[DataQualityLogDetail] CHECK CONSTRAINT [FK_DataQualityLog_DataQualityLogDetail]
GO
