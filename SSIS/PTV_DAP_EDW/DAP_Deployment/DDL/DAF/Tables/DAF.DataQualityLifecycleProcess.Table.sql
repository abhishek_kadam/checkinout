ALTER TABLE [DAF].[DataQualityLifecycleProcess] DROP CONSTRAINT [FK_DataQualityLifecycleProcess_ImplementationRef]
GO
DROP TABLE [DAF].[DataQualityLifecycleProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLifecycleProcess](
	[DataQualityLifecycleProcessID] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ImplementationRefCode] [varchar](50) NOT NULL,
	[ImplementationName] [varchar](100) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataQualityLifecycleProcess] PRIMARY KEY CLUSTERED 
(
	[DataQualityLifecycleProcessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityLifecycleProcess]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLifecycleProcess_ImplementationRef] FOREIGN KEY([ImplementationRefCode])
REFERENCES [DAF].[ImplementationRef] ([ImplementationRefCode])
GO
ALTER TABLE [DAF].[DataQualityLifecycleProcess] CHECK CONSTRAINT [FK_DataQualityLifecycleProcess_ImplementationRef]
GO
