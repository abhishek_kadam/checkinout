ALTER TABLE [DAF].[DataQualityGroupEntity] DROP CONSTRAINT [FK_DataQualityGroupEntity_DataQualityGroup]
GO
ALTER TABLE [DAF].[DataQualityGroupEntity] DROP CONSTRAINT [FK_DataQualityGroupEntity_DataQualityEntity]
GO
DROP TABLE [DAF].[DataQualityGroupEntity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DAF].[DataQualityGroupEntity](
	[DataQualityGroupEntityID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityGroupID] [int] NOT NULL,
	[DataQualityEntityID] [int] NOT NULL,
 CONSTRAINT [PK_DataQualityGroupEntity] PRIMARY KEY CLUSTERED 
(
	[DataQualityGroupEntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [DAF].[DataQualityGroupEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityGroupEntity_DataQualityEntity] FOREIGN KEY([DataQualityEntityID])
REFERENCES [DAF].[DataQualityEntity] ([DataQualityEntityID])
GO
ALTER TABLE [DAF].[DataQualityGroupEntity] CHECK CONSTRAINT [FK_DataQualityGroupEntity_DataQualityEntity]
GO
ALTER TABLE [DAF].[DataQualityGroupEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityGroupEntity_DataQualityGroup] FOREIGN KEY([DataQualityGroupID])
REFERENCES [DAF].[DataQualityGroup] ([DataQualityGroupID])
GO
ALTER TABLE [DAF].[DataQualityGroupEntity] CHECK CONSTRAINT [FK_DataQualityGroupEntity_DataQualityGroup]
GO
