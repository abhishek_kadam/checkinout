DROP TABLE [DAF].[DataEntityRef]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataEntityRef](
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[Description] [varchar](150) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[KeyAttribute1] [varchar](100) NULL,
	[KeyAttribute2] [varchar](100) NULL,
	[KeyAttribute3] [varchar](100) NULL,
	[KeyAttribute4] [varchar](100) NULL,
	[KeyAttribute5] [varchar](100) NULL,
	[KeyAttribute6] [varchar](100) NULL,
	[KeyAttribute7] [varchar](100) NULL,
	[KeyAttribute8] [varchar](100) NULL,
	[KeyAttribute9] [varchar](100) NULL,
	[DataEntityType] [varchar](50) NULL,
 CONSTRAINT [PK_DataEntityRef] PRIMARY KEY CLUSTERED 
(
	[DataEntityRefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
