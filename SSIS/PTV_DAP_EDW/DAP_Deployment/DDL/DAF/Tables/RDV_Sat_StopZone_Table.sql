﻿/*****************************
Author : Ramakrishnan
Modified Date : 2/25/2016
****************************/

IF OBJECT_ID('[DV].[RDV_Sat_StopZone]', 'U') IS NOT NULL
	DROP TABLE [DV].[RDV_Sat_StopZone] 


CREATE TABLE [STG].[RDV_Sat_StopZone] (
    [HubStopSid] binary(20) NULL, 
    [ZoneStopId] int NULL, 
    [Zone] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadDate] datetime2(7) NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);