ALTER TABLE [DAF].[EventLog] DROP CONSTRAINT [FK_EventLog_SeverityLevel]
GO
ALTER TABLE [DAF].[EventLog] DROP CONSTRAINT [FK_EventLog_PackageJobRunDetail]
GO
ALTER TABLE [DAF].[EventLog] DROP CONSTRAINT [FK_EventLog_EventClass]
GO
ALTER TABLE [DAF].[EventLog] DROP CONSTRAINT [FK_EventLog_DataEntityRef]
GO
ALTER TABLE [DAF].[EventLog] DROP CONSTRAINT [FK_EventLog_ControllingPackageRun]
GO
DROP TABLE [DAF].[EventLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[EventLog](
	[EventLogID] [int] IDENTITY(23,1) NOT NULL,
	[EventClassRefCode] [varchar](50) NOT NULL,
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[ServicePackageRunID] [int] NULL,
	[EventSource] [varchar](50) NOT NULL,
	[EventText] [varchar](800) NOT NULL,
	[ExceptionCode] [varchar](50) NULL,
	[DataEntityRefCode] [varchar](50) NULL,
	[AttributeName] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ControllingPackageRunID] [int] NULL,
 CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED 
(
	[EventLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[EventLog]  WITH CHECK ADD  CONSTRAINT [FK_EventLog_ControllingPackageRun] FOREIGN KEY([ControllingPackageRunID])
REFERENCES [DAF].[ControllingPackageRun] ([ControllingPackageRunID])
GO
ALTER TABLE [DAF].[EventLog] CHECK CONSTRAINT [FK_EventLog_ControllingPackageRun]
GO
ALTER TABLE [DAF].[EventLog]  WITH NOCHECK ADD  CONSTRAINT [FK_EventLog_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[EventLog] CHECK CONSTRAINT [FK_EventLog_DataEntityRef]
GO
ALTER TABLE [DAF].[EventLog]  WITH CHECK ADD  CONSTRAINT [FK_EventLog_EventClass] FOREIGN KEY([EventClassRefCode])
REFERENCES [DAF].[EventClassRef] ([EventClassRefCode])
GO
ALTER TABLE [DAF].[EventLog] CHECK CONSTRAINT [FK_EventLog_EventClass]
GO
ALTER TABLE [DAF].[EventLog]  WITH CHECK ADD  CONSTRAINT [FK_EventLog_PackageJobRunDetail] FOREIGN KEY([ServicePackageRunID])
REFERENCES [DAF].[ServicePackageRun] ([ServicePackageRunID])
GO
ALTER TABLE [DAF].[EventLog] CHECK CONSTRAINT [FK_EventLog_PackageJobRunDetail]
GO
ALTER TABLE [DAF].[EventLog]  WITH CHECK ADD  CONSTRAINT [FK_EventLog_SeverityLevel] FOREIGN KEY([SeverityLevelRefCode])
REFERENCES [DAF].[SeverityLevelRef] ([SeverityLevelRefCode])
GO
ALTER TABLE [DAF].[EventLog] CHECK CONSTRAINT [FK_EventLog_SeverityLevel]
GO
