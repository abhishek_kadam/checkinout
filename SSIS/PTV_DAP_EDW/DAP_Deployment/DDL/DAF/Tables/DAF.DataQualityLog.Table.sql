ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_SeverityLevelRef]
GO
ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_QualityIssueRef]
GO
ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_PackageJobRunDetail]
GO
ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_DataSourceRef]
GO
ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_DataQualityRule]
GO
ALTER TABLE [DAF].[DataQualityLog] DROP CONSTRAINT [FK_DataQualityLog_DataEntityRef]
GO
DROP TABLE [DAF].[DataQualityLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityLog](
	[DataQualityLogID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityRuleID] [int] NOT NULL,
	[RuleVersionNum] [smallint] NOT NULL,
	[ServicePackageRunID] [int] NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[QualityIssueRefCode] [varchar](50) NOT NULL,
	[AttributeName] [varchar](50) NULL,
	[OriginalValue] [varchar](150) NULL,
	[TransformedValue] [varchar](150) NULL,
	[MessageText] [varchar](500) NULL,
	[ReferenceID] [varchar](50) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[KeyAttributeValue1] [varchar](100) NULL,
	[KeyAttributeValue2] [varchar](100) NULL,
	[KeyAttributeValue3] [varchar](100) NULL,
	[KeyAttributeValue4] [varchar](100) NULL,
	[KeyAttributeValue5] [varchar](100) NULL,
	[KeyAttributeValue6] [varchar](100) NULL,
	[KeyAttributeValue7] [varchar](100) NULL,
	[KeyAttributeValue8] [varchar](100) NULL,
	[KeyAttributeValue9] [varchar](100) NULL,
	[FullTextSearch]  AS (((((((((isnull([DataEntityRefCode],'')+isnull(' '+[KeyAttributeValue1],''))+isnull(' '+[KeyAttributeValue2],''))+isnull(' '+[KeyAttributeValue3],''))+isnull(' '+[KeyAttributeValue4],''))+isnull(' '+[KeyAttributeValue5],''))+isnull(' '+[KeyAttributeValue6],''))+isnull(' '+[KeyAttributeValue7],''))+isnull(' '+[KeyAttributeValue8],''))+isnull(' '+[KeyAttributeValue9],'')),
 CONSTRAINT [PK_DataQualityLog] PRIMARY KEY CLUSTERED 
(
	[DataQualityLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityLog_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_DataEntityRef]
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityLog_DataQualityRule] FOREIGN KEY([DataQualityRuleID])
REFERENCES [DAF].[DataQualityRule] ([DataQualityRuleID])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_DataQualityRule]
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_DataSourceRef] FOREIGN KEY([DataSourceRefCode])
REFERENCES [DAF].[DataSourceRef] ([DataSourceRefCode])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_DataSourceRef]
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_PackageJobRunDetail] FOREIGN KEY([ServicePackageRunID])
REFERENCES [DAF].[ServicePackageRun] ([ServicePackageRunID])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_PackageJobRunDetail]
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_QualityIssueRef] FOREIGN KEY([QualityIssueRefCode])
REFERENCES [DAF].[QualityIssueRef] ([QualityIssueRefCode])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_QualityIssueRef]
GO
ALTER TABLE [DAF].[DataQualityLog]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityLog_SeverityLevelRef] FOREIGN KEY([SeverityLevelRefCode])
REFERENCES [DAF].[SeverityLevelRef] ([SeverityLevelRefCode])
GO
ALTER TABLE [DAF].[DataQualityLog] CHECK CONSTRAINT [FK_DataQualityLog_SeverityLevelRef]
GO
