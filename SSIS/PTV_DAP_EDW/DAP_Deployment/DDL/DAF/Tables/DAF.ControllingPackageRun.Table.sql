ALTER TABLE [DAF].[ControllingPackageRun] DROP CONSTRAINT [FK_ControllingPackageRun_RunGroup]
GO
DROP TABLE [DAF].[ControllingPackageRun]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[ControllingPackageRun](
	[ControllingPackageRunID] [int] NOT NULL,
	[ControllingPackageName] [varchar](50) NOT NULL,
	[RunGroupID] [int] NOT NULL,
	[ProgressStatusRefCode] [varchar](50) NOT NULL,
	[RunBy] [varchar](50) NOT NULL,
	[CancelFlag] [char](1) NULL,
	[CancelledBy] [varchar](50) NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
 CONSTRAINT [PK_PackageJobRun] PRIMARY KEY CLUSTERED 
(
	[ControllingPackageRunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[ControllingPackageRun]  WITH CHECK ADD  CONSTRAINT [FK_ControllingPackageRun_RunGroup] FOREIGN KEY([RunGroupID])
REFERENCES [DAF].[RunGroup] ([RunGroupID])
GO
ALTER TABLE [DAF].[ControllingPackageRun] CHECK CONSTRAINT [FK_ControllingPackageRun_RunGroup]
GO
