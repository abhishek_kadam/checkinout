/****** Object:  Table [DAF].[Schedule]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[Schedule]
GO
/****** Object:  Table [DAF].[Schedule]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[Schedule](
	[ScheduleNumber] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleName] [varchar](50) NULL,
	[DisplayOrder] [smallint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
