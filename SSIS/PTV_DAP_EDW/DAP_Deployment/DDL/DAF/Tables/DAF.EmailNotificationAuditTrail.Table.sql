ALTER TABLE [DAF].[EmailNotificationAuditTrail] DROP CONSTRAINT [DF__EmailNoti__Email__5AEE82B9]
GO
ALTER TABLE [DAF].[EmailNotificationAuditTrail] DROP CONSTRAINT [DF__EmailNoti__Subsc__59FA5E80]
GO
DROP TABLE [DAF].[EmailNotificationAuditTrail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[EmailNotificationAuditTrail](
	[EmailNotificationAuditTrailID] [int] IDENTITY(1,1) NOT NULL,
	[DataQualityGroupID] [int] NOT NULL,
	[OrganisationalUnitCode] [varchar](4) NOT NULL,
	[OrganisationalUnitName] [varchar](100) NULL,
	[GroupEmailAddress] [varchar](256) NULL,
	[CCEmailAddress] [varchar](256) NULL,
	[SubscribedFlag] [varchar](1) NULL,
	[EmailSentFlag] [varchar](1) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDatetime] [datetime] NULL,
	[Message] [varchar](1024) NULL,
 CONSTRAINT [PK_EmailNotificationAuditTrail] PRIMARY KEY CLUSTERED 
(
	[EmailNotificationAuditTrailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[EmailNotificationAuditTrail] ADD  DEFAULT ('N') FOR [SubscribedFlag]
GO
ALTER TABLE [DAF].[EmailNotificationAuditTrail] ADD  DEFAULT ('Y') FOR [EmailSentFlag]
GO
