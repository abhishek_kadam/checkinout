ALTER TABLE [DAF].[DataQualityEntity] DROP CONSTRAINT [FK_DataQualityEntity_DataSourceRef]
GO
ALTER TABLE [DAF].[DataQualityEntity] DROP CONSTRAINT [FK_DataQualityEntity_DataQualityRule]
GO
ALTER TABLE [DAF].[DataQualityEntity] DROP CONSTRAINT [FK_DataQualityEntity_DataEntityRef]
GO
ALTER TABLE [DAF].[DataQualityEntity] DROP CONSTRAINT [DF__DataQuali__DataQ__38996AB5]
GO
DROP TABLE [DAF].[DataQualityEntity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityEntity](
	[DataQualityEntityID] [int] IDENTITY(1,1) NOT NULL,
	[DataEntityRefCode] [varchar](50) NOT NULL,
	[DataSourceRefCode] [varchar](50) NOT NULL,
	[DataQualityRuleID] [int] NOT NULL,
	[ExecutionOrderNum] [smallint] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [DAF].[DataQualityEntity] ADD [DataQualityProcessFlag] [char](1) NULL
 CONSTRAINT [PK_DataQualityEntity] PRIMARY KEY CLUSTERED 
(
	[DataQualityEntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityEntity] ADD  DEFAULT ('Y') FOR [DataQualityProcessFlag]
GO
ALTER TABLE [DAF].[DataQualityEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityEntity_DataEntityRef] FOREIGN KEY([DataEntityRefCode])
REFERENCES [DAF].[DataEntityRef] ([DataEntityRefCode])
GO
ALTER TABLE [DAF].[DataQualityEntity] CHECK CONSTRAINT [FK_DataQualityEntity_DataEntityRef]
GO
ALTER TABLE [DAF].[DataQualityEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_DataQualityEntity_DataQualityRule] FOREIGN KEY([DataQualityRuleID])
REFERENCES [DAF].[DataQualityRule] ([DataQualityRuleID])
GO
ALTER TABLE [DAF].[DataQualityEntity] CHECK CONSTRAINT [FK_DataQualityEntity_DataQualityRule]
GO
ALTER TABLE [DAF].[DataQualityEntity]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityEntity_DataSourceRef] FOREIGN KEY([DataSourceRefCode])
REFERENCES [DAF].[DataSourceRef] ([DataSourceRefCode])
GO
ALTER TABLE [DAF].[DataQualityEntity] CHECK CONSTRAINT [FK_DataQualityEntity_DataSourceRef]
GO
