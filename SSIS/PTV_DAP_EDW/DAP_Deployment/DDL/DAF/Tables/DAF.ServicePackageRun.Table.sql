ALTER TABLE [DAF].[ServicePackageRun] DROP CONSTRAINT [FK_PackageJobRunDetail_ProgressStatusRef]
GO
ALTER TABLE [DAF].[ServicePackageRun] DROP CONSTRAINT [FK_PackageJobRunDetail_PackageJobRun]
GO
ALTER TABLE [DAF].[ServicePackageRun] DROP CONSTRAINT [FK_PackageJobRunDetail_Package]
GO
/****** Object:  Table [DAF].[ServicePackageRun]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[ServicePackageRun]
GO
/****** Object:  Table [DAF].[ServicePackageRun]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[ServicePackageRun](
	[ServicePackageRunID] [int] NOT NULL,
	[ControllingPackageRunID] [int] NOT NULL,
	[PackageID] [int] NOT NULL,
	[ProgressStatusRefCode] [varchar](50) NOT NULL,
	[NumRecordsProcessed] [int] NULL,
	[NumRecordsFailed] [int] NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
 CONSTRAINT [PK_PackageJobRunDetail] PRIMARY KEY CLUSTERED 
(
	[ServicePackageRunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[ServicePackageRun]  WITH CHECK ADD  CONSTRAINT [FK_PackageJobRunDetail_Package] FOREIGN KEY([PackageID])
REFERENCES [DAF].[Package] ([PackageID])
GO
ALTER TABLE [DAF].[ServicePackageRun] CHECK CONSTRAINT [FK_PackageJobRunDetail_Package]
GO
ALTER TABLE [DAF].[ServicePackageRun]  WITH CHECK ADD  CONSTRAINT [FK_PackageJobRunDetail_PackageJobRun] FOREIGN KEY([ControllingPackageRunID])
REFERENCES [DAF].[ControllingPackageRun] ([ControllingPackageRunID])
GO
ALTER TABLE [DAF].[ServicePackageRun] CHECK CONSTRAINT [FK_PackageJobRunDetail_PackageJobRun]
GO
ALTER TABLE [DAF].[ServicePackageRun]  WITH CHECK ADD  CONSTRAINT [FK_PackageJobRunDetail_ProgressStatusRef] FOREIGN KEY([ProgressStatusRefCode])
REFERENCES [DAF].[ProgressStatusRef] ([ProgressStatusRefCode])
GO
ALTER TABLE [DAF].[ServicePackageRun] CHECK CONSTRAINT [FK_PackageJobRunDetail_ProgressStatusRef]
GO
