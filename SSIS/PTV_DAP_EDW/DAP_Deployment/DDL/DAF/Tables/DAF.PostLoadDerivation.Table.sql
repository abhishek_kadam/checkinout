ALTER TABLE [DAF].[PostLoadDerivation] DROP CONSTRAINT [FK_PostLoadDerivation_ImplementationRef]
GO
ALTER TABLE [DAF].[PostLoadDerivation] DROP CONSTRAINT [FK_PostLoadDerivation_DataQualityRuleId]
GO
/****** Object:  Table [DAF].[PostLoadDerivation]    Script Date: 2/23/2016 5:24:24 PM ******/
DROP TABLE [DAF].[PostLoadDerivation]
GO
/****** Object:  Table [DAF].[PostLoadDerivation]    Script Date: 2/23/2016 5:24:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[PostLoadDerivation](
	[PostLoadDerivationID] [int] NOT NULL,
	[VersionNum] [smallint] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[ImplementationRefCode] [varchar](50) NOT NULL,
	[ImplementationName] [varchar](100) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
	[DataQualityRuleId] [int] NULL,
 CONSTRAINT [PK_PostLoadDerivation] PRIMARY KEY CLUSTERED 
(
	[PostLoadDerivationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[PostLoadDerivation]  WITH CHECK ADD  CONSTRAINT [FK_PostLoadDerivation_DataQualityRuleId] FOREIGN KEY([DataQualityRuleId])
REFERENCES [DAF].[DataQualityRule] ([DataQualityRuleID])
GO
ALTER TABLE [DAF].[PostLoadDerivation] CHECK CONSTRAINT [FK_PostLoadDerivation_DataQualityRuleId]
GO
ALTER TABLE [DAF].[PostLoadDerivation]  WITH CHECK ADD  CONSTRAINT [FK_PostLoadDerivation_ImplementationRef] FOREIGN KEY([ImplementationRefCode])
REFERENCES [DAF].[ImplementationRef] ([ImplementationRefCode])
GO
ALTER TABLE [DAF].[PostLoadDerivation] CHECK CONSTRAINT [FK_PostLoadDerivation_ImplementationRef]
GO
