/****** Object:  Table [STG].[RDV_Sat_CalendarDate]    Script Date: 2/23/2016 6:48:29 PM ******/
DROP TABLE [STG].[RDV_Sat_CalendarDate]
GO
/****** Object:  Table [STG].[RDV_Sat_CalendarDate]    Script Date: 2/23/2016 6:48:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[RDV_Sat_CalendarDate](
	[Date] [date] NULL,
	[CalendarQuarter] [int] NULL,
	[CalendarYear] [int] NULL,
	[DayNameOfWeek] [varchar](50) NULL,
	[DayOfMonth] [int] NULL,
	[DayOfWeek] [int] NULL,
	[DayOfYear] [int] NULL,
	[LastDayOfMonthInd] [bit] NULL,
	[MonthName] [varchar](50) NULL,
	[MonthOfYear] [int] NULL,
	[WeekOfYear] [int] NULL,
	[QuarterName] [varchar](50) NULL,
	[WeekEnding] [varchar](50) NULL,
	[LoadDate] [datetime2](7) NULL,
	[LoadEndDate] [datetime2](7) NULL,
	[IsCurrentFlag] [varchar](1) NULL,
	[RecordSource] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
