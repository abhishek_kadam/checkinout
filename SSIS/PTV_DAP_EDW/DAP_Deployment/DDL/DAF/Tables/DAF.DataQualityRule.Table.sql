ALTER TABLE [DAF].[DataQualityRule] DROP CONSTRAINT [FK_DataQualityRule_SeverityLevelRef]
GO
ALTER TABLE [DAF].[DataQualityRule] DROP CONSTRAINT [FK_DataQualityRule_ImplementationRef]
GO
DROP TABLE [DAF].[DataQualityRule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityRule](
	[DataQualityRuleID] [int] NOT NULL,
	[RuleVersionNum] [smallint] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ShortName] [varchar](50) NOT NULL,
	[SeverityLevelRefCode] [varchar](50) NOT NULL,
	[ImplementationRefCode] [varchar](50) NOT NULL,
	[ImplementationName] [varchar](100) NOT NULL,
	[EnabledFlag] [char](1) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](50) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
	[RuleImplimentationType] [varchar](10) NULL,
	[SuggestedResolution] [varchar](1000) NULL,
 CONSTRAINT [PK_DataQualityRule] PRIMARY KEY CLUSTERED 
(
	[DataQualityRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityRule]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityRule_ImplementationRef] FOREIGN KEY([ImplementationRefCode])
REFERENCES [DAF].[ImplementationRef] ([ImplementationRefCode])
GO
ALTER TABLE [DAF].[DataQualityRule] CHECK CONSTRAINT [FK_DataQualityRule_ImplementationRef]
GO
ALTER TABLE [DAF].[DataQualityRule]  WITH CHECK ADD  CONSTRAINT [FK_DataQualityRule_SeverityLevelRef] FOREIGN KEY([SeverityLevelRefCode])
REFERENCES [DAF].[SeverityLevelRef] ([SeverityLevelRefCode])
GO
ALTER TABLE [DAF].[DataQualityRule] CHECK CONSTRAINT [FK_DataQualityRule_SeverityLevelRef]
GO
