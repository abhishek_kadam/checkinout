﻿/*****************************
Author : Ramakrishna
Modified Date : 03/09/2016
****************************/
IF OBJECT_ID('[STG].[RDV_Sat_StopDetail]', 'U') IS NOT NULL
                DROP TABLE [STG].[RDV_Sat_StopDetail]
GO
CREATE TABLE [STG].[RDV_Sat_StopDetail] (
    [HubStopSid] binary(20) NULL, 
    [LoadDate] datetime2(7) NULL, 
    [PrimaryStopName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [SecondaryStopName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StreetAddress] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [Suburb] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopLatitude] decimal(15, 12) NULL, 
    [StopLongitude] decimal(15, 12) NULL, 
    [StopType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [BayNbr] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [PlatformNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopSpecName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LowFloorAccess] nvarchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TimetableCode] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [StopLandmark] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TrainNbrPlatforms] tinyint NULL, 
    [StationName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [TrainTimeTable] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [LoadEndDate] datetime2(7) NULL, 
    [IsCurrentFlag] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RecordSource] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [RunGroupId] int NULL, 
    [ProcessStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
    [UpdateStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX, DISTRIBUTION = ROUND_ROBIN);
