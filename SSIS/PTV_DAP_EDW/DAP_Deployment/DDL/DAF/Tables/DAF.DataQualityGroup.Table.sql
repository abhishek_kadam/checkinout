ALTER TABLE [DAF].[DataQualityGroup] DROP CONSTRAINT [DF__DataQuali__Subsc__3C69FB99]
GO
DROP TABLE [DAF].[DataQualityGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DAF].[DataQualityGroup](
	[DataQualityGroupID] [int] NOT NULL,
	[Description] [varchar](150) NULL,
	[ShortName] [varchar](50) NULL,
	[SubjectMatterExpertID] [varchar](50) NOT NULL,
	[SubscribedFlag] [char](1) NOT NULL,
	[DefaultEmailAddress] [varchar](256) NULL,
	[DefaultCCEmailAddress] [varchar](256) NULL,
	[DefaultEmailText] [varchar](1024) NOT NULL,
	[DefaultEmailSubject] [varchar](150) NOT NULL,
 CONSTRAINT [PK_DataQualityGroup] PRIMARY KEY CLUSTERED 
(
	[DataQualityGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [DAF].[DataQualityGroup] ADD  DEFAULT ('N') FOR [SubscribedFlag]
GO
